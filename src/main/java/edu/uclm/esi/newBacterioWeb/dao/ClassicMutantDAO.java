package edu.uclm.esi.newBacterioWeb.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import edu.uclm.esi.newBacterioWeb.model.ClassicMutant;
import edu.uclm.esi.newBacterioWeb.model.ProjectFile;

@Repository
public interface ClassicMutantDAO extends CrudRepository<ClassicMutant, String> {
	List<ClassicMutant> findByOriginalFile(ProjectFile originalFile);
	
	@Query(value = "SELECT max(mutant_index) from classic_mutant " +
			"inner join project_file on original_file_id=project_file.id " + 
			"inner join project on project_file.project_id=project.id " + 
			"where project_id=:projectId", nativeQuery = true)
	Integer findMaxIndexByProjectId(@Param("projectId") String projectId);
	
	@Query(value = "SELECT classic_mutant.id, classic_mutant.mutant_index, project_file.file_name FROM nbw.classic_mutant " + 
			"inner join project_file on original_file_id=project_file.id " + 
			"inner join project on project_file.project_id=project.id " + 
			"where project.id=:projectId order by classic_mutant.mutant_index", nativeQuery = true)
	List<Object[]> loadIds(@Param("projectId") String projectId);
	
	@Transactional
	void deleteByOriginalFile(ProjectFile originalFile);
	
	@Transactional
	@Modifying
	@Query(value = "delete from classic_mutant where original_file_id=:originalFileId", nativeQuery = true)
	void deleteByOriginalFileId(@Param("originalFileId") String originalFileId);

	@Query(value = "SELECT max(classic_mutant.mutant_index) FROM nbw.classic_mutant " + 
			"inner join project_file on original_file_id=project_file.id " + 
			"inner join project on project_file.project_id=project.id " + 
			"where project.id=:projectId", nativeQuery = true)
	Long selectLastIndex(@Param("projectId") String projectId);

	@Query(value = "SELECT code FROM nbw.classic_mutant " + 
			"inner join project_file on original_file_id=project_file.id " + 
			"inner join project on project_file.project_id=project.id " + 
			"where project.id=:projectId and mutant_index=:mutantIndex", nativeQuery = true)
	String getCode(@Param("projectId") String projectId, @Param("mutantIndex") int mutantIndex);

	@Query(value = "SELECT count(*) FROM nbw.classic_mutant " + 
			"where original_file_id=:fileId", nativeQuery = true)
	int getNumberOfMutants(@Param("fileId") String fileId);

}
