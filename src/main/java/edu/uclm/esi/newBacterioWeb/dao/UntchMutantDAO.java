package edu.uclm.esi.newBacterioWeb.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import edu.uclm.esi.newBacterioWeb.model.ProjectFile;
import edu.uclm.esi.newBacterioWeb.model.UntchMutant;

@Repository
public interface UntchMutantDAO extends CrudRepository<UntchMutant, String> {
	List<UntchMutant> findByOriginalFile(ProjectFile originalFile);
	
	@Transactional
	void deleteByOriginalFile(ProjectFile originalFile);
	
	@Transactional
	@Modifying
	@Query(value = "delete from untch_mutant where original_file_id=:originalFileId", nativeQuery = true)
	void deleteByOriginalFileId(@Param("originalFileId") String originalFileId);

	@Query(value = "SELECT untch_mutant.id FROM untch_mutant " + 
			"inner join project_file " + 
			"on untch_mutant.original_file_id=project_file.id " + 
			"where project_file.project_id=:projectId", nativeQuery = true)
	List<String> loadIds(@Param("projectId") String projectId);

	@Query(value = "SELECT untch_mutant.original_file_id FROM untch_mutant " + 
			"inner join project_file " + 
			"on untch_mutant.original_file_id=project_file.id " + 
			"where project_file.project_id=:projectId", nativeQuery = true)
	List<String> loadOriginalFileIds(@Param("projectId") String projectId);
}
