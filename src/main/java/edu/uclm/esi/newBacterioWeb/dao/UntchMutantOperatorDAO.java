package edu.uclm.esi.newBacterioWeb.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import edu.uclm.esi.newBacterioWeb.model.UntchMutantOperator;

@Repository
public interface UntchMutantOperatorDAO extends CrudRepository<UntchMutantOperator, String> {

	@Transactional
	void deleteByOriginalFileId(String originalFileId);

	@Query(value = "SELECT max(untch_mutant_operator.mutant_index) FROM nbw.untch_mutant_operator" + 
			"	inner join project on untch_mutant_operator.project_id=project.id" + 
			"    where project.id=:projectId", nativeQuery = true)
	Long selectLastIndex(@Param("projectId") String projectId);

	@Query(value = "SELECT untch_mutant_operator.id, untch_mutant_operator.mutant_index, project_file.file_name " + 
			"FROM untch_mutant_operator inner join untch_mutant on untch_mutant_operator.original_file_id=untch_mutant.original_file_id " + 
			"inner join project_file on untch_mutant_operator.original_file_id=project_file.id " + 
			"inner join project on project_file.project_id = project.id " + 
			"where project.id=:projectId order by untch_mutant_operator.mutant_index", nativeQuery = true)
	List<Object[]> loadIds(@Param("projectId") String projectId);
}
