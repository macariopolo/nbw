package edu.uclm.esi.newBacterioWeb.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import edu.uclm.esi.newBacterioWeb.model.TestClass;

@Repository
public interface TestClassDAO extends CrudRepository<TestClass, Long>{

	@Query(value = "SELECT * FROM nbw.test_class" + 
			"	inner join project_file on test_class.project_file_id = project_file.id" + 
			"   where project_file.project_id=:projectId and " +
			"   test_type='androidTest'", nativeQuery = true)
	List<TestClass> getTestClasses(@Param("projectId") String projectId);
	
	
}
