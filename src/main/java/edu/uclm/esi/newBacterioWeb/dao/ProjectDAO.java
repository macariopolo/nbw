package edu.uclm.esi.newBacterioWeb.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import edu.uclm.esi.newBacterioWeb.model.Project;
import edu.uclm.esi.newBacterioWeb.model.User;

@Repository
public interface ProjectDAO extends CrudRepository<Project, String>{

	List<Project> findByCreator(User creator);

	@Query(value = "select file_name, count(*), operator" + 
			"	from classic_mutant" + 
			"    inner join project_file" + 
			"    on project_file.id=classic_mutant.original_file_id" + 
			"    where project_id=:projectId" + 
			"    group by file_name, operator" + 
			"    order by file_name", nativeQuery = true)
	List<Object> getMutantsTable(@Param("projectId") String projectId);
	
	@Query(value = "SELECT file_name, test_type, name, checked" + 
			"	FROM project_file" + 
			"    inner join project_file_test_methods on project_file.id = project_file_test_methods.project_file_id" + 
			"    inner join test_method on project_file_test_methods.test_methods_id=test_method.id" + 
			"    where project_file.project_id=:projectId" + 
			"    order by file_name", nativeQuery = true)
	List<Object> getTestClasses(@Param("projectId") String projectId);

}
