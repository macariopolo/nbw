package edu.uclm.esi.newBacterioWeb.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import edu.uclm.esi.newBacterioWeb.model.User;

@Repository
public interface UserDAO extends CrudRepository<User, String> {

}
