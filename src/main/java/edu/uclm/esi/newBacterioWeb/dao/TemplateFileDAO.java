package edu.uclm.esi.newBacterioWeb.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import edu.uclm.esi.newBacterioWeb.model.TemplateFile;

@Repository
public interface TemplateFileDAO extends CrudRepository<TemplateFile, String> {

}
