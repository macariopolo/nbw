package edu.uclm.esi.newBacterioWeb.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import edu.uclm.esi.newBacterioWeb.model.TestMethod;

@Repository
public interface TestMethodDAO extends CrudRepository<TestMethod, Long>{

	@Query(value = "SELECT * FROM nbw.test_method where test_class_id=:testClassId", nativeQuery = true)
	List<TestMethod> getTestMethods(@Param("testClassId") Long testClassId);


}
