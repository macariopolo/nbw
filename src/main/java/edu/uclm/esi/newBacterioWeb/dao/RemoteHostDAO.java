package edu.uclm.esi.newBacterioWeb.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import edu.uclm.esi.newBacterioWeb.model.RemoteHost;

@Repository
public interface RemoteHostDAO extends JpaRepository<RemoteHost, String> {
}
