package edu.uclm.esi.newBacterioWeb.dao;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import edu.uclm.esi.newBacterioWeb.model.Project;
import edu.uclm.esi.newBacterioWeb.model.ProjectFile;

@Repository
public interface ProjectFileDAO extends CrudRepository<ProjectFile, String>{

	List<ProjectFile> findByProject(Project project);

	@Query(value = "SELECT id FROM nbw.project_file where project_id=:projectId", nativeQuery = true)
	List<String> findIdsByProject(@Param("projectId") String projectId);
	
	@Query(value = "SELECT id FROM nbw.project_file where project_id=:projectId and right(file_name, 5)='.java'", nativeQuery = true)
	List<String> findJavaIdsByProject(@Param("projectId") String projectId);

	@Query(value = "SELECT * FROM nbw.project_file where project_id=:projectId and file_name=:fileName", nativeQuery = true)
	ProjectFile findByFileName(@Param("projectId")  String projectId, @Param("fileName") String fileName);

	@Query(value = "SELECT id, file_name FROM nbw.project_file where project_id=:projectId and right(file_name, 5)='.java'", nativeQuery = true)
	List<Map<String, Object>> loadJavaFiles(@Param("projectId") String projectId);

	@Query(value = "select original_file_id from classic_mutant\n" + 
			"inner join project_file on classic_mutant.original_file_id = project_file.id\n" + 
			"inner join project on project_file.project_id = project.id\n" + 
			"where project.id=:projectId and classic_mutant.mutant_index=:mutantIndex", nativeQuery = true)
	String findByProjectIdAndMutantIndexId(@Param("projectId") String projectId, @Param("mutantIndex") int mutantIndex);
}
