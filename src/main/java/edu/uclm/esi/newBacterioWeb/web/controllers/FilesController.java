package edu.uclm.esi.newBacterioWeb.web.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import edu.uclm.esi.newBacterioWeb.dao.ProjectFileDAO;
import edu.uclm.esi.newBacterioWeb.model.ProjectFile;
import edu.uclm.esi.newBacterioWeb.model.User;

@RestController
public class FilesController {
	@Autowired
	private ProjectFileDAO projectFileDAO;
	
	@GetMapping(value = "/loadFile")
	public List<String> loadFile(HttpSession session, @RequestParam String id) throws Exception {
		User user = (User) session.getAttribute("user");
		session.setAttribute("user", user);
		Optional<ProjectFile> optProjectFile = projectFileDAO.findById(id);
		if (optProjectFile.isPresent()) {
			ProjectFile projectFile = optProjectFile.get();
			String content = projectFile.getCode();
			String[] splitted = content.split("\n");
			ArrayList<String> lines = new ArrayList<>();
			for (int i=0; i<splitted.length; i++)
				lines.add(splitted[i]);
			return lines;
		}
		return new ArrayList<>();
	}
	
	@GetMapping(value = "/loadFileFromMutantIndex")
	public String loadFileFromMutantIndex(HttpSession session, @RequestParam int mutantIndex, @RequestParam String projectId) throws Exception {
		User user = (User) session.getAttribute("user");
		session.setAttribute("user", user);
		String projectFileId = projectFileDAO.findByProjectIdAndMutantIndexId(projectId, mutantIndex);
		return projectFileId;
	}
}
