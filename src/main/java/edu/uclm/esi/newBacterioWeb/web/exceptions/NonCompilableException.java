package edu.uclm.esi.newBacterioWeb.web.exceptions;

import edu.uclm.esi.newBacterioWeb.model.ClassicMutantWrapper;

public class NonCompilableException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NonCompilableException(ClassicMutantWrapper mutantWrapper) {
		super(mutantWrapper ==null ? "Errors compiling the original version" : "The mutant " + mutantWrapper.getIndex() + " is not compilable" );
	}
}
