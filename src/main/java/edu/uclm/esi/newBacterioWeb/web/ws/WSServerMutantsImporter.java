package edu.uclm.esi.newBacterioWeb.web.ws;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.BinaryMessage;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.PackageDeclaration;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.printer.PrettyPrinterConfiguration;

import edu.uclm.esi.newBacterioWeb.model.ClassicMutant;
import edu.uclm.esi.newBacterioWeb.model.Project;
import edu.uclm.esi.newBacterioWeb.model.ProjectFile;
import edu.uclm.esi.newBacterioWeb.web.controllers.Manager;

@Component
public class WSServerMutantsImporter extends TextWebSocketHandler {

	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {
		System.out.println(session.getId());
	}
	
	@Override
	protected void handleBinaryMessage(WebSocketSession session, BinaryMessage message) {
		session.setBinaryMessageSizeLimit(50*1024*1024);
		Project project = getProject(session);
		if (project == null) {
			try {
				send(session, "type", "error", "message", "Project not found");
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			try {
				byte[] payload = message.getPayload().array();
				StaticJavaParser.getConfiguration().setSymbolResolver(project.getJavaSymbolSolver());
				String code = new String(payload);
				CompilationUnit cu=StaticJavaParser.parse(code);
				Optional<PackageDeclaration> optPackageDeclaration = cu.getPackageDeclaration();
				String fileName = "";
				if (optPackageDeclaration.isPresent()) {
					PackageDeclaration packageDeclaration = optPackageDeclaration.get();
					fileName = packageDeclaration.getNameAsString();
				}
				fileName=fileName + "/";
				List<ClassOrInterfaceDeclaration> classes = cu.findAll(ClassOrInterfaceDeclaration.class);
				if (!classes.isEmpty()) {
					ClassOrInterfaceDeclaration clazz = classes.get(0);
					fileName=fileName + clazz.getNameAsString();
					fileName = fileName.replace('.', '/') + ".java";
					fileName = project.getProjectConfiguration().getSrcFolder() + "main/java/" + fileName;
					fileName = fileName.replace('\\', '/');
					ProjectFile originalFile = Manager.get().getProjectFileDAO().findByFileName(project.getId(), fileName);
					if (originalFile!=null) {
						Integer nom = Manager.get().getClassicMutantDAO().findMaxIndexByProjectId(project.getId());
						ClassicMutant mutant = new ClassicMutant();
						mutant.setCode(cu.toString(new PrettyPrinterConfiguration()));
						if (nom==null)
							nom = 1;
						mutant.setIndex(nom+1);
						mutant.setOperator("Imported");
						mutant.setOriginalFile(originalFile);
						Manager.get().getClassicMutantDAO().save(mutant);
					}
				}
				send(session, "type", "GIVE_ME_OTHER", "fileName", fileName);
			} catch (Exception e) {
				try {
					send(session, "type", "GIVE_ME_OTHER", "fileName", "");
				} catch (IOException e1) {
					// TODO Auto-generated catch block
				}				
			}
		}
	}
	
	@Override
	public void handleTransportError(WebSocketSession session, Throwable exception) throws Exception {
		exception.printStackTrace();
	}
	
	private Project getProject(WebSocketSession session) {
		HttpHeaders headers = session.getHandshakeHeaders();
		List<String> cookies = headers.get("cookie");
		for (String cookie : cookies)
			if (cookie.startsWith("JSESSIONID=")) {
				String httpSessionId = cookie.substring("JSESSIONID=".length());
				HttpSession httpSession = Manager.get().findHttpSession(httpSessionId);
				String projectId = httpSession.getAttribute("projectId").toString();
				Optional<Project> optProject = Manager.get().getProjectDAO().findById(projectId);
				if (optProject.isPresent())
					return optProject.get();
			}
		return null;
	}
	
	private void send(WebSocketSession session, Object... typesAndValues) throws IOException {
		JSONObject jso = new JSONObject();
		int i=0;
		while (i<typesAndValues.length) {
			jso.put(typesAndValues[i].toString(), typesAndValues[i+1]);
			i+=2;
		}
		WebSocketMessage<?> wsMessage=new TextMessage(jso.toString());
		session.sendMessage(wsMessage);
	}
}
