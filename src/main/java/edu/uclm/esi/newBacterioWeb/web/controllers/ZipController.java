package edu.uclm.esi.newBacterioWeb.web.controllers;

import java.io.FileOutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import edu.uclm.esi.newBacterioWeb.model.Project;
import edu.uclm.esi.newBacterioWeb.model.User;

@RestController
public class ZipController {
	
	@PostMapping("/uploadZip")
    public String uploadZip(HttpSession session, @RequestParam("file") MultipartFile file) throws Exception {
		User user = (User) session.getAttribute("user");
		String name = file.getOriginalFilename();
		if (!name.endsWith(".zip"))
			throw new Exception("The file is not a zip file");
		name = name.substring(0, name.length()-4);
		Project project = Manager.get().createProject(user, name);
		session.setAttribute("projectId", project.getId());
		byte[] bytes = file.getBytes();
		String fileName = project.getProjectsFolder() + file.getOriginalFilename();
		try(FileOutputStream fos = new FileOutputStream(fileName)) {
			fos.write(bytes);
		}
		return file.getOriginalFilename();
	}
	
	@ExceptionHandler(Exception.class)
	public ModelAndView handleException(HttpServletRequest req, Exception ex) {
		ModelAndView result = new ModelAndView();
		result.setViewName("respuesta");
		result.addObject("exception", ex);
		result.setStatus(HttpStatus.BAD_REQUEST);
	    return result;
	} 
}
