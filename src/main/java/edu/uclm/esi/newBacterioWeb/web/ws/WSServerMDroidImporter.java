package edu.uclm.esi.newBacterioWeb.web.ws;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.Vector;

import org.json.JSONObject;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.PackageDeclaration;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.printer.PrettyPrinterConfiguration;

import edu.uclm.esi.newBacterioWeb.model.ClassicMutant;
import edu.uclm.esi.newBacterioWeb.model.Project;
import edu.uclm.esi.newBacterioWeb.model.ProjectFile;
import edu.uclm.esi.newBacterioWeb.web.controllers.Manager;

@Component
public class WSServerMDroidImporter extends TextWebSocketHandler {
	private HashMap<String, Info> sessions = new HashMap<>();
	private HashMap<String, Project> projects = new HashMap<>();
	
	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {
		session.setBinaryMessageSizeLimit(50*1024*1024);
		session.setTextMessageSizeLimit(50*1024*1024);
		String query = session.getUri().getQuery(); 
		String projectId = query.substring(query.indexOf('=')+1);
		Project project = Manager.get().getProjectDAO().findById(projectId).get();
		this.projects.put(session.getId(), project);
	}
	
	@Override
	protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
		JSONObject jso = new JSONObject(message.getPayload());
		if (jso.getString("type").equals("LOG")) {
			String logFileName = jso.getString("logFileName");
			String logFile = jso.getString("logFile");
			String mutantsFolder = jso.getString("webkitRelativePath");
			int posBarra = mutantsFolder.lastIndexOf('/');
			mutantsFolder = mutantsFolder.substring(0, posBarra);
			logFile = logFile.substring(logFile.indexOf("Mutant "));
			String[] lineas = logFile.split("\n");
			Info info = new Info(); 
			for (int i=0; i<lineas.length; i++) {
				String linea = lineas[i];
				int posDosPuntos = linea.indexOf(':');
				String mutantName = linea.substring(0, posDosPuntos);
				int posPyC = linea.indexOf(';');
				String fileName = linea.substring(posDosPuntos+2, posPyC);
				String operatorName = linea.substring(posPyC+2);
				String folder = logFileName.substring(0, logFileName.indexOf("s.log"));
				String numeroDeMutant = mutantName.substring(mutantName.indexOf(' ')).trim();
				folder = mutantsFolder + "/" + folder + numeroDeMutant;
				info.add(folder, fileName, operatorName);
			}
			this.sessions.put(session.getId(), info);
			askForMutant(session);
		} else if (jso.get("type").equals("JAVA")) {
			String fileName = jso.getString("fileName");
			String code = jso.getString("contents");
			String operatorName = jso.getString("operatorName");
			CompilationUnit cu=StaticJavaParser.parse(code);
			Optional<PackageDeclaration> optPackageDeclaration = cu.getPackageDeclaration();
			if (optPackageDeclaration.isPresent()) {
				PackageDeclaration packageDeclaration = optPackageDeclaration.get();
				fileName = packageDeclaration.getNameAsString();
			}
			fileName=fileName + "/";
			List<ClassOrInterfaceDeclaration> classes = cu.findAll(ClassOrInterfaceDeclaration.class);
			if (!classes.isEmpty()) {
				ClassOrInterfaceDeclaration clazz = classes.get(0);
				fileName = fileName + clazz.getNameAsString();
				fileName = fileName.replace('.', '/') + ".java";
				Project project = this.projects.get(session.getId());
				fileName = project.getProjectConfiguration().getSrcFolder() + "main/java/" + fileName;
				fileName = fileName.replace('\\', '/');
				ProjectFile originalFile = Manager.get().getProjectFileDAO().findByFileName(project.getId(), fileName);
				if (originalFile!=null) {
					ClassicMutant mutant = new ClassicMutant();
					mutant.setCode(cu.toString(new PrettyPrinterConfiguration()));
					Integer nom = Manager.get().getClassicMutantDAO().findMaxIndexByProjectId(project.getId());
					if (nom==null)
						nom = 1;
					mutant.setIndex(nom+1);
					mutant.setOperator(operatorName);
					mutant.setOriginalFile(originalFile);
					Manager.get().getClassicMutantDAO().save(mutant);
				}
			}
			askForMutant(session);
		}
	}
	
	private void askForMutant(WebSocketSession session) {
		Info info = sessions.get(session.getId());
		if (info!=null) {
			String folder = info.getFolder();
			String fileName = info.getFileName();
			String operatorName = info.getOperatorName();
			send(session, "type", "GIVE_ME", "folder", folder, "fileName", fileName, "operatorName", operatorName);
			if (info.isEmpty()) {
				sessions.remove(session.getId());	
				send(session, "type", "FINISHED");
			}
		}
	}

	private class Info {
		private Vector<String> folders;
		private Vector<String> fileNames;
		private Vector<String> operatorNames;
		
		public Info() {
			this.folders = new Vector<>();
			this.fileNames = new Vector<>();
			this.operatorNames = new Vector<>();
		}

		public boolean isEmpty() {
			return folders.isEmpty();
		}

		public void add(String folder, String fileName, String operatorName) {
			this.folders.add(folder);
			this.fileNames.add(fileName);
			this.operatorNames.add(operatorName);
		}
		
		String getFolder() {
			return this.folders.remove(0);
		}
		
		String getFileName() {
			return this.fileNames.remove(0);
		}
		
		String getOperatorName() {
			return this.operatorNames.remove(0);
		}
	}
	
	@Override
	public void handleTransportError(WebSocketSession session, Throwable exception) throws Exception {
		exception.printStackTrace();
	}
	
	public static synchronized void send(WebSocketSession session, Object... typesAndValues) {
		if (session.isOpen()) {
			JSONObject jso = new JSONObject();
			int i=0;
			while (i<typesAndValues.length) {
				jso.put(typesAndValues[i].toString(), typesAndValues[i+1]);
				i+=2;
			}
			WebSocketMessage<?> wsMessage=new TextMessage(jso.toString());
			try {
				session.sendMessage(wsMessage);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
