package edu.uclm.esi.newBacterioWeb.web.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.FORBIDDEN)
public class RegisterException extends Exception {
	private static final long serialVersionUID = 1L;

	public RegisterException() {
		super("Invalid credentials for registering");
	}

	public RegisterException(String msg) {
		super(msg);
	}
}
