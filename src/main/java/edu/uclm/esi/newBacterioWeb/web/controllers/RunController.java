package edu.uclm.esi.newBacterioWeb.web.controllers;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import edu.uclm.esi.newBacterioWeb.dao.TestClassDAO;
import edu.uclm.esi.newBacterioWeb.dao.TestMethodDAO;
import edu.uclm.esi.newBacterioWeb.model.Project;
import edu.uclm.esi.newBacterioWeb.model.ProjectConfiguration;
import edu.uclm.esi.newBacterioWeb.model.TestClass;
import edu.uclm.esi.newBacterioWeb.model.TestMethod;

@RestController
public class RunController {
	
	@GetMapping(value = "/loadTestClasses", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<TestClass> loadTestClasses(HttpSession session) throws Exception {
		String projectId = session.getAttribute("projectId").toString();
		Optional<Project> optProject = Manager.get().getProjectDAO().findById(projectId);
		if (optProject.isPresent()) {
			Project project = optProject.get();
			project.loadTestClasses();
			List<TestClass> result = project.getTestClasses();
			return result;
		}
		throw new Exception("Project not found");
	}
	
	@GetMapping(value = "/getCommands", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<String> getCommands(HttpSession session, @RequestParam String projectId) throws Exception {
		Optional<Project> optProject = Manager.get().getProjectDAO().findById(projectId);
		if (optProject.isPresent()) {
			Project project = optProject.get();
			ProjectConfiguration conf = project.getProjectConfiguration();
			List<String> result = new ArrayList<>();
			result.add(conf.getBuildAPKCommand());
			result.add(conf.getBuildGradle());
			result.add(conf.getBuildTestAPKCommand());
			result.add(conf.getDebugAPKPathAndName());
			result.add(conf.getDebugPackage());
			result.add(conf.getInstallAPKCommand());
			result.add(conf.getInstallTestAPKCommand());
			result.add(conf.getTestRunner());
			return result;
		}
		throw new Exception("Project not found");
	}
	
	@PostMapping(value = "/saveTestClasses", produces = MediaType.APPLICATION_JSON_VALUE)
	public void saveTestClasses(HttpSession session, @RequestBody List<TestClass> testClasses) throws Exception {
		String projectId = session.getAttribute("projectId").toString();
		Optional<Project> optProject = Manager.get().getProjectDAO().findById(projectId);
		if (optProject.isPresent()) {
			TestClassDAO testClassDAO = Manager.get().getTestClassDAO();
			TestMethodDAO testMethodDAO = Manager.get().getTestMethodDAO();
			for (int i=0; i<testClasses.size(); i++) {
				TestClass receivedTestClass = testClasses.get(i);
				Optional<TestClass> optTestClass = testClassDAO.findById(receivedTestClass.getId());
				if (optTestClass.isPresent()) {
					TestClass testClass = optTestClass.get();
					testClass.setChecked((boolean) receivedTestClass.isChecked());
					List<TestMethod> receivedTestMethods = receivedTestClass.getTestMethods();
					for (int j=0; j<receivedTestMethods.size(); j++) {
						TestMethod receivedTestMethod = receivedTestMethods.get(j);
						Optional<TestMethod> optTestMethod = testMethodDAO.findById(receivedTestMethod.getId());
						if (optTestMethod.isPresent()) {
							TestMethod testMethod = optTestMethod.get();
							testMethod.setChecked(receivedTestMethod.isChecked());
						}
					}
					testClassDAO.save(testClass);
				}
			}
		} else
			throw new Exception("Project not found");
	}
	
	@GetMapping(value = "/viewError", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<String> viewError(HttpSession session, @RequestParam("fileName") String fileName) throws Exception {
		ArrayList<String> lines = new ArrayList<>();
		try(FileReader fr=new FileReader(fileName)) {  
			try(BufferedReader br=new BufferedReader(fr)) {
				String line=null;
				while ((line=br.readLine())!=null)
					lines.add(line);
			}
		}
		return lines;
	}
}
