package edu.uclm.esi.newBacterioWeb.web.ws;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.BinaryMessage;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import edu.uclm.esi.newBacterioWeb.model.Project;
import edu.uclm.esi.newBacterioWeb.model.ProjectFile;
import edu.uclm.esi.newBacterioWeb.model.User;
import edu.uclm.esi.newBacterioWeb.web.controllers.Manager;

@Component
public class WSServerUpload extends TextWebSocketHandler {

	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {
		session.setBinaryMessageSizeLimit(1000*1024*1024);
		System.out.println(session.getId());
	}
	
	@Override
	protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
		String sM = message.getPayload().toString();
		System.out.println(sM);
		JSONObject jso = new JSONObject(sM);
		String type = jso.getString("type");
		if (type.equals("GIVE_ME_OTHER")) {
			send(session, "type", "GIVE_ME_OTHER", "fileName", jso.getString("fileName"));
		} else if (type.equals("CREATE_CONFIGURATION")) {
			String projectId = jso.getString("projectId");
			Optional<Project> optProject = Manager.get().getProjectDAO().findById(projectId );
			if (optProject.isPresent()) {
				Project project = optProject.get();
				this.send(session, "type", "MESSAGE", "message", "Calculating configuration");
				project.calculateConfiguration();
				this.send(session, "type", "MESSAGE", "message", "Analyzing source files");
				project.analyzeJavaFiles(session);
				this.send(session, "type", "ALL_SAVED", "projectId", project.getId());
			} else {
				this.send(session, "type", "ERROR", "message", "Project not found");
			}
		} else if (type.equals("ANALYZE_SOURCE_FILES")) {
			String projectId = jso.getString("projectId");
			Optional<Project> optProject = Manager.get().getProjectDAO().findById(projectId );
			if (optProject.isPresent()) {
				Project project = optProject.get();
				this.send(session, "type", "MESSAGE", "message", "Analyzing source files");
				project.analyzeJavaFiles(session);
				this.send(session, "type", "FILES_ANALYZED", "projectId", project.getId());
			}
		}
	}
	
	@Override
	protected void handleBinaryMessage(WebSocketSession session, BinaryMessage message) {
		session.setBinaryMessageSizeLimit(1000*1024*1024);
		
		byte[] payload = message.getPayload().array();
		int posSlash = 0;
		do {
			posSlash++;
		} while (payload[posSlash]!='#' || payload[posSlash+1]!='@' || payload[posSlash+2]!='0' || payload[posSlash+3]!='0' || payload[posSlash+4]!='@' || payload[posSlash+5]!='#');
		
		byte[] bFileName = new byte[posSlash];
		System.arraycopy(payload, 0, bFileName, 0, posSlash);
		String fileName = new String(bFileName);
		fileName=fileName.replace('\\', '/');
		
		Project project = getProject(session, fileName);
		if (project!=null) {
			String originalFileName = fileName;
			fileName = fileName.substring(project.getName().length() + 1);
			if (fileName.endsWith("AndroidManifest.xml") && !fileName.contains("intermediates")) {
				project.setAndroidManifest(fileName);
				Manager.get().getProjectDAO().save(project);
			}
			if (!fileName.endsWith(".DS_Store")) {
				int bodyStart = posSlash + 6;
				int bodyLength = payload.length - bodyStart;
				if (bodyLength>0) {
					byte[] bContents = new byte[bodyLength];
					System.arraycopy(payload, bodyStart, bContents, 0, bodyLength);
					ProjectFile projectFile = new ProjectFile(project, fileName, bContents);
					Manager.get().getProjectFileDAO().save(projectFile);
					
					int lastSeparator = fileName.lastIndexOf('/');
					String fileFolder = "";
					if (lastSeparator!=-1)
						fileFolder = fileName.substring(0, lastSeparator);
					
					new File(project.getProjectFolder() + fileFolder).mkdirs();
					fileName = project.getProjectFolder() + fileName;
					try(FileOutputStream fos = new FileOutputStream(fileName)) {
						fos.write(bContents);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			try {
				send(session, "type", "GIVE_ME_OTHER", "fileName", originalFileName, "projectId", project.getId());
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			try {
				send(session, "type", "ERROR", "message", "Project not found");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void handleTransportError(WebSocketSession session, Throwable exception) throws Exception {
		exception.printStackTrace();
	}
	
	private Project getProject(WebSocketSession session, String fileName) {
		HttpHeaders headers = session.getHandshakeHeaders();
		List<String> cookies = headers.get("cookie"); 
		for (String cookie : cookies) {
			if (cookie.startsWith("JSESSIONID=")) {
				String httpSessionId = cookie.substring("JSESSIONID=".length());
				HttpSession httpSession = Manager.get().findHttpSession(httpSessionId);
				if (httpSession.getAttribute("projectId")!=null) {
					String projectId = httpSession.getAttribute("projectId").toString();
					Optional<Project> optProject = Manager.get().getProjectDAO().findById(projectId);
					if (optProject.isPresent())
						return optProject.get();
				} else {
					String projectName = fileName.substring(0, fileName.indexOf('/'));
					if (projectName.startsWith("/"))
						projectName = projectName.substring(1);
					User user = (User) httpSession.getAttribute("user");
					Project project = Manager.get().createProject(user, projectName);
					httpSession.setAttribute("projectId", project.getId());
					return project;
				}
			}
		}
		return null;
	}
	
	private void send(WebSocketSession session, Object... typesAndValues) throws IOException {
		JSONObject jso = new JSONObject();
		int i=0;
		while (i<typesAndValues.length) {
			jso.put(typesAndValues[i].toString(), typesAndValues[i+1]);
			i+=2;
		}
		WebSocketMessage<?> wsMessage=new TextMessage(jso.toString());
		session.sendMessage(wsMessage);
	}
}
