package edu.uclm.esi.newBacterioWeb.web.ws;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import edu.uclm.esi.newBacterioWeb.model.Project;
import edu.uclm.esi.newBacterioWeb.web.controllers.Manager;

@Component
public class WSMutantsGeneration extends TextWebSocketHandler {	
	
	@Override
	protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
		session.setTextMessageSizeLimit(100*1024);
		JSONObject jso = new JSONObject(message.getPayload());
		boolean generateClassicMutants = jso.getBoolean("generateClassicMutants");
		boolean generateUntchMutants = jso.getBoolean("generateUntchMutants");
		String type = jso.optString("type");
		if (type.contentEquals("GENERATE_MUTANTS")) {
			if (jso.optJSONArray("operators")==null)
				send(session, "type", "ERROR", "message", "You haven't selected any mutation operator");
			else if (jso.optJSONArray("files")==null)
				send(session, "type", "ERROR", "message", "You haven't selected any file to mutate");
			else {
				Project project = getProject(session);
				if(generateClassicMutants)
					Manager.get().generateClassicMutants(project, session, jso.getJSONArray("operators"), jso.getJSONArray("files"));
				if(generateUntchMutants)
					Manager.get().generateUntchMutants(project, session, jso.getJSONArray("operators"), jso.getJSONArray("files"));
			}
		}
		session.close();
	}
	
	private Project getProject(WebSocketSession session) {
		HttpHeaders headers = session.getHandshakeHeaders();
		List<String> cookies = headers.get("cookie");
		for (String cookie : cookies)
			if (cookie.startsWith("JSESSIONID=")) {
				String httpSessionId = cookie.substring("JSESSIONID=".length());
				HttpSession httpSession = Manager.get().findHttpSession(httpSessionId);
				String projectId = httpSession.getAttribute("projectId").toString();
				Optional<Project> optionalProject = Manager.get().getProjectDAO().findById(projectId);
				if (optionalProject.isPresent())
					return optionalProject.get();
			}
		return null;
	}
	
	public static synchronized void send(WebSocketSession session, Object... typesAndValues) {
		JSONObject jso = new JSONObject();
		int i=0;
		while (i<typesAndValues.length) {
			jso.put(typesAndValues[i].toString(), typesAndValues[i+1]);
			i+=2;
		}
		WebSocketMessage<?> wsMessage=new TextMessage(jso.toString());
		try {
			session.sendMessage(wsMessage);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
