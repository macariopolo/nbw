package edu.uclm.esi.newBacterioWeb.web.controllers;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import edu.uclm.esi.newBacterioWeb.dao.ClassicMutantDAO;
import edu.uclm.esi.newBacterioWeb.dao.ProjectDAO;
import edu.uclm.esi.newBacterioWeb.model.Project;
import edu.uclm.esi.newBacterioWeb.model.ProjectConfiguration;
import edu.uclm.esi.newBacterioWeb.model.User;

@RestController
public class ProjectsController {
	@Autowired
	private ProjectDAO projectDAO;
	@Autowired
	private ClassicMutantDAO mutantDAO;
	
	@GetMapping(value = "/loadProjects", produces = MediaType.APPLICATION_JSON_VALUE)
	public Iterator<Project> loadProjects(HttpSession session) throws Exception {
		User user = (User) session.getAttribute("user");
		session.setAttribute("user", user);
		//List<Project> result = projectDAO.findByCreator(user);
		Iterator<Project> result = projectDAO.findAll().iterator();
		return result;
	}
	
	@GetMapping("/getNumberOfMutants")
	public int getNumberOfMutants(HttpSession session, @RequestParam String fileId) throws Exception {
		return mutantDAO.getNumberOfMutants(fileId);
	}
	
	@GetMapping(value = "/openProject", produces = MediaType.APPLICATION_JSON_VALUE)
	public Project openProject(HttpSession session, @RequestParam String id) throws Exception {
		User user = (User) session.getAttribute("user");
		session.setAttribute("user", user);
		Optional<Project> optProject= projectDAO.findById(id);
		if (!optProject.isPresent())
			throw new Exception("Project not found");
		Project project = optProject.get();
		project.loadSourceFiles();
		session.setAttribute("projectId", project.getId());
		return project;
	}
	
	@GetMapping(value = "/deleteProject", produces = MediaType.APPLICATION_JSON_VALUE)
	public String deleteProject(HttpSession session, @RequestParam String id) throws Exception {
		User user = (User) session.getAttribute("user");
		session.setAttribute("user", user);
		Optional<Project> optProject = Manager.get().getProjectDAO().findById(id);
		if (optProject.isPresent()) {
			Project project = optProject.get();
			File file = new File(project.getProjectFolder());
			try { FileUtils.forceDelete(file); } catch (Exception e) { }
			projectDAO.deleteById(id);
			return new JSONObject().put("type", "OK").toString();
		}
		throw new Exception("Project not found");
	}
	
	@PostMapping(value = "/createProject", produces = MediaType.APPLICATION_JSON_VALUE)
	public String createProject(HttpSession session, @RequestBody Map<String, Object> credenciales) throws Exception {
		JSONObject jso = new JSONObject(credenciales);
		String creator = jso.getString("userName");
		String name = jso.getString("name");
		
		User user = (User) session.getAttribute("user");
		
		if (!user.getUserName().equals(creator))
			throw new Exception("You cannot create this project with that user name");
		Manager.get().createProject(user, name);
		return new JSONObject().put("type", "OK").toString();
	}
	
	@PostMapping(value = "/saveConfiguration", produces = MediaType.APPLICATION_JSON_VALUE)
	public String saveConfiguration(HttpSession session, @RequestBody ProjectConfiguration configuration) throws Exception {
		String projectId = session.getAttribute("projectId").toString();
		Optional<Project> optProject = Manager.get().getProjectDAO().findById(projectId);
		if (optProject.isPresent()) {
			Project project = optProject.get();
			Long configurationId = project.getProjectConfiguration().getId();
			Manager.get().getProjectConfigurationDAO().deleteById(configurationId);			
			configuration.setProject(project);
			Manager.get().getProjectConfigurationDAO().save(configuration);
		}
		return new JSONObject().put("type", "OK").toString();
	}
	
	@GetMapping(value = "/loadProjectConfiguration", produces = MediaType.APPLICATION_JSON_VALUE)
	public ProjectConfiguration loadProjectConfiguration(HttpSession session) throws Exception {
		String projectId = session.getAttribute("projectId").toString();
		Optional<Project> optProject = Manager.get().getProjectDAO().findById(projectId);
		if (optProject.isPresent()) {
			ProjectConfiguration conf = optProject.get().getProjectConfiguration();
			return conf;
		}
		throw new Exception("Project  not found");
	}
	
	@GetMapping(value = "/loadAvailableLibraries", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<String> loadAvailableLibraries(HttpSession session) throws Exception {
		//HostConfiguration host = HostConfiguration.get();
		//return host.load().getLibraries();
		return new ArrayList<>();
	}
	
	@GetMapping(value = "/guessGradleW")
	public String guessGradleW(HttpSession session) throws Exception {
		String projectId = session.getAttribute("projectId").toString();
		Optional<Project> optProject = Manager.get().getProjectDAO().findById(projectId);
		if (optProject.isPresent()) {
			Project project = optProject.get();
			return project.guessGradleW();
		}
		throw new Exception("Project  not found");
	}
	
	@GetMapping(value = "/guessLocalProperties")
	public String guessLocalProperties(HttpSession session) throws Exception {
		String projectId = session.getAttribute("projectId").toString();
		Optional<Project> optProject = Manager.get().getProjectDAO().findById(projectId);
		if (optProject.isPresent()) {
			Project project = optProject.get();
			return project.guessLocalProperties();
		}
		throw new Exception("Project  not found");
	}
	
	@GetMapping(value = "/guessAppFolder")
	public String guessAppFolder(HttpSession session) throws Exception {
		String projectId = session.getAttribute("projectId").toString();
		Optional<Project> optProject = Manager.get().getProjectDAO().findById(projectId);
		if (optProject.isPresent()) {
			Project project = optProject.get();
			return project.guessAppFolder();
		}
		throw new Exception("Project  not found");
	}
	
	@GetMapping(value = "/guessBuildGradle")
	public String guessBuildGradle(HttpSession session) throws Exception {
		String projectId = session.getAttribute("projectId").toString();
		Optional<Project> optProject = Manager.get().getProjectDAO().findById(projectId);
		if (optProject.isPresent()) {
			Project project = optProject.get();
			return project.guessBuildGradle();
		}
		throw new Exception("Project  not found");
	}
	
	@GetMapping(value = "/guessSrcFolder")
	public String guessSrcFolder(HttpSession session) throws Exception {
		String projectId = session.getAttribute("projectId").toString();
		Optional<Project> optProject = Manager.get().getProjectDAO().findById(projectId);
		if (optProject.isPresent()) {
			Project project = optProject.get();
			return project.guessSrcFolder();
		}
		throw new Exception("Project  not found");
	}

	@GetMapping(value = "/guessManifest")
	public String guessManifest(HttpSession session) throws Exception {
		String projectId = session.getAttribute("projectId").toString();
		Optional<Project> optProject = Manager.get().getProjectDAO().findById(projectId);
		if (optProject.isPresent()) {
			Project project = optProject.get();
			return project.guessManifest();
		}
		throw new Exception("Project  not found");
	}
	
	@GetMapping(value = "/guessApplicationId")
	public String guessApplicationId(HttpSession session) throws Exception {
		String projectId = session.getAttribute("projectId").toString();
		Optional<Project> optProject = Manager.get().getProjectDAO().findById(projectId);
		if (optProject.isPresent()) {
			Project project = optProject.get();
			return project.guessApplicationId();
		}
		throw new Exception("Project  not found");
	}
	
	@GetMapping(value = "/guessTestRunner")
	public String guessTestRunner(HttpSession session) throws Exception {
		String projectId = session.getAttribute("projectId").toString();
		Optional<Project> optProject = Manager.get().getProjectDAO().findById(projectId);
		if (optProject.isPresent()) {
			Project project = optProject.get();
			return project.guessTestRunner();
		}
		throw new Exception("Project  not found");
	}
	
	@GetMapping(value = "/guessInstallAPKCommand")
	public String guessInstallAPKCommand(HttpSession session) throws Exception {
		String projectId = session.getAttribute("projectId").toString();
		Optional<Project> optProject = Manager.get().getProjectDAO().findById(projectId);
		if (optProject.isPresent()) {
			Project project = optProject.get();
			return project.guessInstallAPKCommand();
		}
		throw new Exception("Project  not found");
	}
	
	@GetMapping(value = "/guessDebugAPKPathAndName")
	public String guessDebugAPKPathAndName(HttpSession session) throws Exception {
		String projectId = session.getAttribute("projectId").toString();
		Optional<Project> optProject = Manager.get().getProjectDAO().findById(projectId);
		if (optProject.isPresent()) {
			Project project = optProject.get();
			return project.guessDebugAPKPathAndName();
		}
		throw new Exception("Project  not found");
	}
	
	@GetMapping(value = "/guessDebugPackage")
	public String guessDebugPackage(HttpSession session) throws Exception {
		String projectId = session.getAttribute("projectId").toString();
		Optional<Project> optProject = Manager.get().getProjectDAO().findById(projectId);
		if (optProject.isPresent()) {
			Project project = optProject.get();
			return project.guessDebugPackage();
		}
		throw new Exception("Project  not found");
	}
	
	@GetMapping(value = "/guessInstallTestAPKCommand")
	public String guessInstallTestAPKCommand(HttpSession session) throws Exception {
		String projectId = session.getAttribute("projectId").toString();
		Optional<Project> optProject = Manager.get().getProjectDAO().findById(projectId);
		if (optProject.isPresent()) {
			Project project = optProject.get();
			return project.guessInstallTestAPKCommand();
		}
		throw new Exception("Project  not found");
	}
	
	@GetMapping(value = "/guessTestAPKPathAndName")
	public String guessTestAPKPathAndName(HttpSession session) throws Exception {
		String projectId = session.getAttribute("projectId").toString();
		Optional<Project> optProject = Manager.get().getProjectDAO().findById(projectId);
		if (optProject.isPresent()) {
			Project project = optProject.get();
			return project.guessTestAPKPathAndName();
		}
		throw new Exception("Project  not found");
	}
	
	@GetMapping(value = "/guessTestPackage")
	public String guessTestPackage(HttpSession session) throws Exception {
		String projectId = session.getAttribute("projectId").toString();
		Optional<Project> optProject = Manager.get().getProjectDAO().findById(projectId);
		if (optProject.isPresent()) {
			Project project = optProject.get();
			return project.guessTestPackage();
		}
		throw new Exception("Project  not found");
	}
	
	@GetMapping(value = "/guessCompileSDKVersion")
	public String guessCompileSDKVersion(HttpSession session) throws Exception {
		String projectId = session.getAttribute("projectId").toString();
		Optional<Project> optProject = Manager.get().getProjectDAO().findById(projectId);
		if (optProject.isPresent()) {
			Project project = optProject.get();
			return project.guessCompileSdkVersion();
		}
		throw new Exception("Project  not found");
	}
}
