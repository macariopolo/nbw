package edu.uclm.esi.newBacterioWeb.web.controllers;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import edu.uclm.esi.newBacterioWeb.model.Task;
import edu.uclm.esi.newBacterioWeb.model.TaskQueue;
import edu.uclm.esi.newBacterioWeb.model.User;

@RestController
public class TaskController {
	
	@GetMapping(value = "/getTasks", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Task> getTasks(HttpSession session, HttpServletResponse response) throws Exception {
		User user = (User) session.getAttribute("user");
		List<Task> tasks = TaskQueue.get().getTasks(user);
		return tasks;
	}
	
	@GetMapping(value = "/viewTask", produces = MediaType.APPLICATION_JSON_VALUE)
	public Task viewTask(HttpSession session, @RequestParam String id) throws Exception {
		User user = (User) session.getAttribute("user");
		Task task = TaskQueue.get().findTask(user, id);
		return task;
	}
	
	@GetMapping(value = "/stopTask", produces = MediaType.APPLICATION_JSON_VALUE)
	public Task stopTask(HttpSession session, @RequestParam String id) throws Exception {
		User user = (User) session.getAttribute("user");
		Task task = TaskQueue.get().findTask(user, id);
		task.stop();
		return task;
	}
}
