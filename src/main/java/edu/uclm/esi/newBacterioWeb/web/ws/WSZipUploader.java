package edu.uclm.esi.newBacterioWeb.web.ws;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.json.JSONObject;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import com.mykong.zip.UnzipUtility;

import edu.uclm.esi.newBacterioWeb.model.Project;
import edu.uclm.esi.newBacterioWeb.model.ProjectFile;
import edu.uclm.esi.newBacterioWeb.web.controllers.Manager;

@Component
public class WSZipUploader extends TextWebSocketHandler {

	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {
		System.out.println(session.getId());
	}
	
	@Override
	protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
		String sM = message.getPayload().toString();
		System.out.println(sM);
		JSONObject jso = new JSONObject(sM);
		if (jso.getString("type").equals("SAVE_UNZIPPED_FILES")) {
			String fileName = jso.getString("fileName");
			Project project= this.getProject(session);
			
			UnzipUtility unzip = new UnzipUtility();
			String destDirectory = project.getProjectsFolder();
			this.send(session, "type", "MESSAGE", "message", "Unzipping file");
			unzip.unzip(destDirectory + fileName, destDirectory);
			new File(destDirectory + fileName).delete();
			saveFiles(session, project, destDirectory + project.getName() + '/');
			
			this.send(session, "type", "MESSAGE", "message", "Calculating configuration");
			project.calculateConfiguration();
			this.send(session, "type", "MESSAGE", "message", "Analyzing source files");
			project.analyzeJavaFiles(session);
	        try {
				this.send(session, "type", "ALL_SAVED", "projectId", project.getId());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	private void saveFiles(WebSocketSession session, Project project, String destDirectory) throws IOException {
		File dir = new File(destDirectory);
        File[] files = dir.listFiles();

        if (files != null && files.length > 0) {
            for (File file : files) {
                if (file.isDirectory()) {
                	if (file.getName().equals("__MACOSX"))
                		FileUtils.forceDelete(file);
                	else
                		saveFiles(session, project, file.getAbsolutePath());
                } else {
                	save(session, project, file);
                }
            }
        }
	}

	private void save(WebSocketSession session, Project project, File file) {
		String fileName = new String(file.getAbsolutePath());
		fileName = fileName.substring(project.getProjectFolder().length());
		if (fileName.endsWith("AndroidManifest.xml") && !fileName.contains("intermediates")) {
			project.setAndroidManifest(fileName);
			Manager.get().getProjectDAO().save(project);
		}
		if (!fileName.endsWith(".DS_Store")) {
			try(FileInputStream fis = new FileInputStream(file.getAbsolutePath())) {
				byte[] bContents = new byte[fis.available()];
				fis.read(bContents);
				ProjectFile projectFile = new ProjectFile(project, fileName, bContents);
				Manager.get().getProjectFileDAO().save(projectFile);
				this.send(session, "type", "FILE_NAME", "fileName", fileName);
			} catch (Exception e) {
		        try {
					this.send(session, "type", "ERROR", "message", e.getMessage());
				} catch (IOException e2) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}	
			}
		}
	}
	
	@Override
	public void handleTransportError(WebSocketSession session, Throwable exception) throws Exception {
		exception.printStackTrace();
	}
	
	private Project getProject(WebSocketSession session) {
		HttpHeaders headers = session.getHandshakeHeaders();
		List<String> cookies = headers.get("cookie");
		for (String cookie : cookies) {
			int start = cookie.indexOf("JSESSIONID=");
			if (start!=-1) {
				int end = cookie.indexOf(';', start+1);
				if (end==-1)
					end = cookie.length();
				String httpSessionId = cookie.substring(start + "JESSIONID=".length() + 1, end).trim();
				HttpSession httpSession = Manager.get().findHttpSession(httpSessionId);
				String projectId = httpSession.getAttribute("projectId").toString();
				Optional<Project> optProject = Manager.get().getProjectDAO().findById(projectId);
				if (optProject.isPresent())
					return optProject.get();
			}
		}
		return null;
	}
	
	private void send(WebSocketSession session, Object... typesAndValues) throws IOException {
		JSONObject jso = new JSONObject();
		int i=0;
		while (i<typesAndValues.length) {
			jso.put(typesAndValues[i].toString(), typesAndValues[i+1]);
			i+=2;
		}
		WebSocketMessage<?> wsMessage=new TextMessage(jso.toString());
		session.sendMessage(wsMessage);
	}
}
