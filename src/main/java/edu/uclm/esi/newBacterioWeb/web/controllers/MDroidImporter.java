package edu.uclm.esi.newBacterioWeb.web.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import edu.uclm.esi.newBacterioWeb.dao.ClassicMutantDAO;
import edu.uclm.esi.newBacterioWeb.dao.ProjectDAO;
import edu.uclm.esi.newBacterioWeb.dao.UntchMutantDAO;
import edu.uclm.esi.newBacterioWeb.model.ClassicMutant;
import edu.uclm.esi.newBacterioWeb.model.Table;
import edu.uclm.esi.newBacterioWeb.operators.classic.ClassicMutationOperator;

@RestController
public class MDroidImporter {
	@Autowired
	private ProjectDAO projectDAO;
	@Autowired
	private ClassicMutantDAO classicMutantDAO;
	
	@PostMapping(value = "/mdroidLog", produces = MediaType.APPLICATION_JSON_VALUE)
	public void loadOperators(HttpSession session, @RequestBody Map<String, Object> info) throws Exception {
		System.out.println(info);
	}
	
}
