package edu.uclm.esi.newBacterioWeb.web.exceptions;

@SuppressWarnings("serial")
public class HostConfigurationException extends Exception {

	public HostConfigurationException(String msg) {
		super(msg);
	}

}
