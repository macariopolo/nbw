package edu.uclm.esi.newBacterioWeb.web.exceptions;

import org.json.JSONArray;
import org.json.JSONObject;

public class ProcessException extends Exception {

	private static final long serialVersionUID = 1L;
	private JSONObject executionResult;


	public ProcessException(JSONObject executionResult) {
		this.executionResult = executionResult;
	}
	
	@Override
	public String getMessage() {
		StringBuilder sb = new StringBuilder();
		if (this.executionResult.optString("deviceName").length()>0) {
			sb.append("Error in device " + this.executionResult.getString("deviceName") + "\n\n");
		}
		JSONArray errorLines = executionResult.getJSONArray("errorLines");
		for (int i=0; i<errorLines.length(); i++)
			sb.append(errorLines.get(i) + "\n");
		return sb.toString();
	}
}
