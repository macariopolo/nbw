package edu.uclm.esi.newBacterioWeb.web.controllers;

import java.io.File;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketSession;

import edu.uclm.esi.newBacterioWeb.dao.ClassicMutantDAO;
import edu.uclm.esi.newBacterioWeb.dao.ExecutionLineDAO;
import edu.uclm.esi.newBacterioWeb.dao.ProjectConfigurationDAO;
import edu.uclm.esi.newBacterioWeb.dao.ProjectDAO;
import edu.uclm.esi.newBacterioWeb.dao.ProjectFileDAO;
import edu.uclm.esi.newBacterioWeb.dao.RemoteHostDAO;
import edu.uclm.esi.newBacterioWeb.dao.TemplateFileDAO;
import edu.uclm.esi.newBacterioWeb.dao.TestClassDAO;
import edu.uclm.esi.newBacterioWeb.dao.TestMethodDAO;
import edu.uclm.esi.newBacterioWeb.dao.UntchMutantDAO;
import edu.uclm.esi.newBacterioWeb.dao.UntchMutantOperatorDAO;
import edu.uclm.esi.newBacterioWeb.dao.UserDAO;
import edu.uclm.esi.newBacterioWeb.model.ClassicMutant;
import edu.uclm.esi.newBacterioWeb.model.Project;
import edu.uclm.esi.newBacterioWeb.model.ProjectFile;
import edu.uclm.esi.newBacterioWeb.model.User;
import edu.uclm.esi.newBacterioWeb.model.devices.DeviceProxy;
import edu.uclm.esi.newBacterioWeb.operators.classic.ClassicMutantGenerator;
import edu.uclm.esi.newBacterioWeb.operators.classic.ClassicMutationOperator;
import edu.uclm.esi.newBacterioWeb.operators.untch.UntchMutantGenerator;
import edu.uclm.esi.newBacterioWeb.web.exceptions.LoginException;
import edu.uclm.esi.newBacterioWeb.web.exceptions.RegisterException;

@Component
public class Manager {
	@Autowired
	private UserDAO userDAO;
	@Autowired
	private ProjectDAO projectDAO;
	@Autowired
	private ProjectConfigurationDAO projectConfigurationDAO;
	@Autowired
	private ProjectFileDAO projectFileDAO;
	@Autowired
	private TestClassDAO testClassDAO;
	@Autowired
	private ClassicMutantDAO classicMutantDAO;
	@Autowired
	private UntchMutantDAO untchMutantDAO;
	@Autowired
	private UntchMutantOperatorDAO untchMutantOperatorDAO;
	@Autowired
	private TemplateFileDAO templateFileDAO;
	@Autowired
	private TestMethodDAO testMethodDAO;
	@Autowired
	private ExecutionLineDAO executionLineDAO;
	@Autowired
	private RemoteHostDAO remoteHostDAO;
	
	private Vector<DeviceProxy> deviceProxies;
	
	private ConcurrentHashMap<String, HttpSession> sessions;
	
	private Manager() {
		this.sessions = new ConcurrentHashMap<String, HttpSession>();
		this.deviceProxies = new Vector<>();
		createFolders();
	}
	
	private void createFolders() {
		String homeFolder=System.getProperty("user.home");
		new File(homeFolder + File.separatorChar + "nbw").mkdir();
		new File(homeFolder + File.separatorChar + "nbw" + File.separatorChar + "io").mkdir();
		new File(homeFolder + File.separatorChar + "nbw" + File.separatorChar + "templates").mkdir();
		new File(homeFolder + File.separatorChar + "nbw" + File.separatorChar + "executionResults").mkdir();
		new File(homeFolder + File.separatorChar + "nbw" + File.separatorChar + "projects").mkdir();
		new File(homeFolder + File.separatorChar + "nbw" + File.separatorChar + "receivedAPKs").mkdir();
	}
	
	public String getIoPath() {
		return System.getProperty("user.home") + File.separatorChar + "nbw" + File.separatorChar + "io" + File.separatorChar;
	}
	
	public String getReceivedAPKs() {
		return System.getProperty("user.home") + File.separatorChar + "nbw" + File.separatorChar + "receivedAPKs" + File.separatorChar;
	}
	
	private static class ManagerHolder {
		static Manager singleton=new Manager();
	}
	
	@Bean
	public static Manager get() {
		return ManagerHolder.singleton;
	}

	public User login(String userName, String pwd) throws LoginException {
		Optional<User> optUser = userDAO.findById(userName);
		if (optUser.isPresent()) {
			User user = optUser.get();
			if (user.getPwd().equals(pwd))
				return user;
		}
		throw new LoginException();
	}

	public void register(String userName, String email, String pwd) throws RegisterException {
		User user = new User();
		user.setUserName(userName);
		user.setEmail(email);
		user.setPwd(pwd);
		userDAO.save(user);
	}

	public Project createProject(User creator, String name) {
		Project project = new Project();
		project.setCreator(creator);
		project.setName(name);
		projectDAO.save(project);
		new File(project.getProjectFolder()).mkdirs();
		return project;
	}
	
	public ProjectDAO getProjectDAO() {
		return projectDAO;
	}
	
	public RemoteHostDAO getRemoteHostDAO() {
		return remoteHostDAO;
	}
	
	public ProjectFileDAO getProjectFileDAO() {
		return projectFileDAO;
	}
	
	public TestClassDAO getTestClassDAO() {
		return testClassDAO;
	}
	
	public ProjectConfigurationDAO getProjectConfigurationDAO() {
		return projectConfigurationDAO;
	}
	
	public ClassicMutantDAO getClassicMutantDAO() {
		return classicMutantDAO;
	}
	
	public UntchMutantDAO getUntchMutantDAO() {
		return untchMutantDAO;
	}
	
	public UntchMutantOperatorDAO getUntchMutantOperatorDAO() {
		return untchMutantOperatorDAO;
	}
	
	public TemplateFileDAO getTemplateFileDAO() {
		return templateFileDAO;
	}
	
	public TestMethodDAO getTestMethodDAO() {
		return testMethodDAO;
	}
	
	public ExecutionLineDAO getExecutionLineDAO() {
		return executionLineDAO;
	}

	public void add(HttpSession session) {
		this.sessions.put(session.getId(), session);
	}
	
	public HttpSession findHttpSession(String id) {
		return this.sessions.get(id);
	}

	public Map<String, List<ClassicMutationOperator>> loadOperators() {
		String filePath=new File(".").getAbsolutePath();
		filePath=filePath.replace('\\', '/');
		filePath=filePath + "/target/classes/";
		int longitud=filePath.length();
		filePath=filePath + "edu/uclm/esi/newBacterioWeb/operators/classic";
		File folder=new File(filePath);
		HashMap<String, List<ClassicMutationOperator>> operatorsFamilies=loadOperators(folder, longitud);
		return operatorsFamilies;
	}
	
	@SuppressWarnings({ "rawtypes" })
	private HashMap<String, List<ClassicMutationOperator>> loadOperators(File folder, int longitudPrefijo) throws JSONException {
		List<String> classes=loadFiles(folder);
		HashMap<String, List<ClassicMutationOperator>> classification=new HashMap<>();
		
		for (int i=classes.size()-1; i>=0; i--) {
			String className=classes.get(i);
			className=className.substring(longitudPrefijo);
			className=className.replace(File.separator, ".");
			className=className.substring(0, className.length()-6);
			try {
				Class clazz=Class.forName(className);
				Class sc=clazz.getSuperclass();
				sc.toGenericString();
				if (Modifier.isAbstract(clazz.getModifiers()) || sc.equals(Enum.class) || sc.equals(Object.class) || !ClassicMutationOperator.class.isAssignableFrom(clazz))
					continue;
				ClassicMutationOperator op = (ClassicMutationOperator) clazz.newInstance();
				String familyName = op.getFamilyName();
				List<ClassicMutationOperator> jsa = classification.get(familyName);
				if (jsa==null) 
					jsa=new ArrayList<>();
				String operatorName=className;
				int last=operatorName.lastIndexOf('.');
				operatorName=operatorName.substring(last+1);
				jsa.add(op);
				classification.put(familyName, jsa);
			} catch (Exception e) { 
				System.out.println(e.toString());
			}
		}
		return classification;
	}
	
	private List<String> loadFiles(File folder) throws JSONException {
		ArrayList<String> result=new ArrayList<>();
		File[] listOfFiles = folder.listFiles();

		String pathName;
		for (int i = 0; i < listOfFiles.length; i++) {
			if (listOfFiles[i].isDirectory()) {
				pathName=listOfFiles[i].getAbsolutePath();
				List<String> auxi=loadFiles(new File(pathName));
				for (int j=0; j<auxi.size(); j++)
					result.add(auxi.get(j));
			} else if (!listOfFiles[i].getName().startsWith("."))
				result.add(listOfFiles[i].getAbsolutePath());
		}
		return result;
	}

	public void generateClassicMutants(Project project, WebSocketSession session, JSONArray operators, JSONArray files) {
		ClassicMutantGenerator generator = new ClassicMutantGenerator(session, project, operators, files);
		generator.generateMutants();
	}
	
	public void generateUntchMutants(Project project, WebSocketSession session, JSONArray operators, JSONArray files) {
		UntchMutantGenerator generator = new UntchMutantGenerator(session, project, operators, files);
		generator.generateMutants();
	}

	public List<ClassicMutant> loadMutantsOf(String id) {
		Optional<ProjectFile> optOriginal = this.projectFileDAO.findById(id);
		if (optOriginal.isPresent()) {
			ProjectFile original = optOriginal.get();
			List<ClassicMutant> mutants = this.classicMutantDAO.findByOriginalFile(original);
			mutants.sort(new Comparator<ClassicMutant>() {

				@Override
				public int compare(ClassicMutant o1, ClassicMutant o2) {
					return o1.getMutantIndex()-o2.getMutantIndex();
				}
			});
			return mutants;
		}
		return new ArrayList<>();
	}

	private DeviceProxy findDeviceProxy(String ip, int port, String name) {
		for (DeviceProxy device : this.deviceProxies)
			if (device.getIp().equals(ip) && device.getPort()==port && device.getName().equals(name))
				return device;
		return null;
	}

	public DeviceProxy addRemoteDevice(String ip, int port, String name) {
		DeviceProxy remoteDevice = findDeviceProxy(ip, port, name);
		if (remoteDevice==null) {
			remoteDevice = new DeviceProxy(ip, port, name);
			this.deviceProxies.add(remoteDevice);
		}
		remoteDevice.removeMutants();
		return remoteDevice;
	}
	
	public List<DeviceProxy> getDevices() {
		return this.deviceProxies;
	}

	public synchronized void reserveDevices() throws Exception {
		Exception e = null;
		int index = 0;
		for (int i=0; i<deviceProxies.size(); i++) {
			DeviceProxy device = deviceProxies.get(i);
			if (device.isBusy()) {
				index = i;
				e = new Exception(device.getDescription() + " is busy");
				break;
			}
			device.setBusy(true);
		}
		if (e!=null) {
			for (int i=0; i<index; i++) {
				DeviceProxy device = this.deviceProxies.get(i);
				device.setBusy(false);
			}
			throw e;
		}
	}
}
