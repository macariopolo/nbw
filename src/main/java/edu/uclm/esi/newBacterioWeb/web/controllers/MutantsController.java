package edu.uclm.esi.newBacterioWeb.web.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import edu.uclm.esi.newBacterioWeb.dao.ClassicMutantDAO;
import edu.uclm.esi.newBacterioWeb.dao.ProjectDAO;
import edu.uclm.esi.newBacterioWeb.dao.UntchMutantDAO;
import edu.uclm.esi.newBacterioWeb.model.ClassicMutant;
import edu.uclm.esi.newBacterioWeb.model.Table;
import edu.uclm.esi.newBacterioWeb.operators.classic.ClassicMutationOperator;

@RestController
public class MutantsController {
	@Autowired
	private ProjectDAO projectDAO;
	@Autowired
	private ClassicMutantDAO classicMutantDAO;
	
	@GetMapping(value = "/loadOperators", produces = MediaType.APPLICATION_JSON_VALUE)
	public Map<String, List<ClassicMutationOperator>> loadOperators(HttpSession session) throws Exception {
		return Manager.get().loadOperators();
	}
	
	@GetMapping(value = "/loadMutantsOf", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<ClassicMutant> loadMutantsOf(HttpSession session, @RequestParam String originalFileId) throws Exception {
		return Manager.get().loadMutantsOf(originalFileId);
	}
	
	@GetMapping(value = "/loadMutant", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<String> loadMutant(HttpSession session, @RequestParam String mutantId) throws Exception {
		Optional<ClassicMutant> optMutant = classicMutantDAO.findById(mutantId);
		if (optMutant.isPresent()) {
			ClassicMutant mutant = optMutant.get();
			String code = mutant.getCode();
			String[] splitted = code.split("\n");
			ArrayList<String> lines = new ArrayList<>();
			for (int i=0; i<splitted.length; i++)
				lines.add(splitted[i]);
			return lines;
		}
		return new ArrayList<>();
	}
	
	@GetMapping(value = "/loadMutantByIndex", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<String> loadMutantByIndex(HttpSession session, @RequestParam int mutantIndex) throws Exception {
		String projectId = session.getAttribute("projectId").toString();
		String code = classicMutantDAO.getCode(projectId, mutantIndex);
			String[] splitted = code.split("\n");
			ArrayList<String> lines = new ArrayList<>();
			for (int i=0; i<splitted.length; i++)
				lines.add(splitted[i]);
			return lines;
	}
	
	@GetMapping(value = "/loadMutantTables", produces = MediaType.APPLICATION_JSON_VALUE)
	public ArrayList<ArrayList<String>> loadMutantTables(HttpSession session) throws Exception {
		String projectId = session.getAttribute("projectId").toString();
		List<Object> resultSet = projectDAO.getMutantsTable(projectId);
		
		Table table = new Table();
		for (int i=0; i<resultSet.size(); i++) {
			Object[] row = (Object[]) resultSet.get(i);
			String fileName = row[0].toString();
			table.addRow(fileName);
			String operator = row[2].toString();
			table.addColumn(operator);
		}
		table.ready();
		for (int i=0; i<resultSet.size(); i++) {
			Object[] row = (Object[]) resultSet.get(i);
			String fileName = row[0].toString();
			String operator = row[2].toString();
			table.set(fileName, operator, row[1].toString());
		}
		ArrayList<ArrayList<String>> sTable = table.getTable();
		return sTable;
	}
	
	@PostMapping(value = "/deleteMutants", produces = MediaType.APPLICATION_JSON_VALUE)
	public String deleteMutants(HttpSession session, @RequestBody List<Map<String, Object>> classIds) throws Exception {
		ClassicMutantDAO classicMutantsDao = Manager.get().getClassicMutantDAO();
		UntchMutantDAO untchMutantsDao = Manager.get().getUntchMutantDAO();
		for (Map<String, Object> classIdAndName : classIds) {
			String classId = classIdAndName.get("id").toString();
			classicMutantsDao.deleteByOriginalFileId(classId);
			untchMutantsDao.deleteByOriginalFileId(classId);
		}
		return new JSONObject().put("type", "OK").toString();
	}
	
	
	@DeleteMapping(value = "/removeMutant")
	public void removeMutant(HttpSession session, @RequestParam String mutantId) throws Exception {
		classicMutantDAO.deleteById(mutantId);
	}
}
