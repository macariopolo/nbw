package edu.uclm.esi.newBacterioWeb.web.ws;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import edu.uclm.esi.newBacterioWeb.model.ExecutionAlgorithm;
import edu.uclm.esi.newBacterioWeb.model.ExecutionMode;
import edu.uclm.esi.newBacterioWeb.model.ExecutionOrder;
import edu.uclm.esi.newBacterioWeb.model.Project;
import edu.uclm.esi.newBacterioWeb.model.TaskQueue;
import edu.uclm.esi.newBacterioWeb.model.TestRunnerGroup;
import edu.uclm.esi.newBacterioWeb.model.User;
import edu.uclm.esi.newBacterioWeb.web.controllers.Manager;

@Component
public class WSTestsExecution extends TextWebSocketHandler {	
	
	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {
		System.out.println("afterConnectionEstablished: " + session.getId());
	}
	
	@Override
	protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
		session.setTextMessageSizeLimit(100*1024);
		JSONObject jso = new JSONObject(message.getPayload());
		String type = jso.getString("type");
		Project project = getProject(session);
		JSONArray remoteDevices = jso.getJSONArray("remoteDevices");
		if (remoteDevices.length()==0) {
			send(session, "type", "ERROR", "message", "No devices selected");
			return;
		} 
		if (project!=null) {
			User user = getUser(session);
			String sExecutionMode = jso.optString("executionMode");
			int mutantSampling = jso.optInt("mutantSampling");
			String selectedMutants = jso.optString("selectedMutants");
			selectedMutants = selectedMutants.trim();
			if (selectedMutants.length()>0)
				mutantSampling = 100;
			String sExecutionAgorithm = jso.optString("executionAlgorithm");
			String sExecutionOrder = jso.optString("executionOrder");
			
			ExecutionAlgorithm executionAgorithm = sExecutionAgorithm.equals("allAgainstAll") ? ExecutionAlgorithm.ALL_AGAINST_ALL  : ExecutionAlgorithm.ONLY_ALIVE;
			ExecutionOrder executionOrder = sExecutionOrder.equals("sequential") ? ExecutionOrder.SEQUENTIAL : ExecutionOrder.BEST_TEST_CASE_FIRST;
			ExecutionMode executionMode = null;
			String distributionAlgorithm = jso.optString("distributionAlgorithm"); 
			boolean skipOriginal = jso.optBoolean("skipOriginal");
			if (type.equals("runTests")) {
				if (sExecutionMode.equals("ORIGINAL")) {
					executionMode = ExecutionMode.ORIGINAL;
					executionAgorithm = ExecutionAlgorithm.ALL_AGAINST_ALL;
					executionOrder = ExecutionOrder.SEQUENTIAL;
				} else if (sExecutionMode.equals("CLASSIC_MUTANTS")) {
					executionMode = ExecutionMode.CLASSIC_MUTANTS;
				} else if (sExecutionMode.equals("UNTCH_MUTANTS")) {
					executionMode = ExecutionMode.UNTCH_MUTANTS;
				} else {
					WSTestsExecution.send(session, "type", "ERROR", "message", "Unrecognized execution mode");
					return;
				}
			}
			try {
				TestRunnerGroup testRunnerGroup = new TestRunnerGroup(user, project, session, remoteDevices, executionMode, type);
				testRunnerGroup.setMutantSampling(mutantSampling);
				testRunnerGroup.setSelectedMutants(selectedMutants);
				testRunnerGroup.setExecutionAlgorithm(executionAgorithm);
				testRunnerGroup.setExecutionOrder(executionOrder);
				testRunnerGroup.setDistributionAlgorithm(distributionAlgorithm);
				testRunnerGroup.setSkipOriginal(skipOriginal);
				TaskQueue.get().put(user, testRunnerGroup);
				Thread thread = new Thread(testRunnerGroup);
				thread.start();
			} catch (Exception e) {
				send(session, "type", "ERROR", "message", e.getMessage());
			}
		} 
	}
		
	private User getUser(WebSocketSession session) {
		HttpHeaders headers = session.getHandshakeHeaders();
		List<String> cookies = headers.get("cookie");
		for (String cookie : cookies)
			if (cookie.startsWith("JSESSIONID=")) {
				String httpSessionId = cookie.substring("JSESSIONID=".length());
				HttpSession httpSession = Manager.get().findHttpSession(httpSessionId);
				User user = (User) httpSession.getAttribute("user");
				return user;
			}
		return null;
	}
	
	private Project getProject(WebSocketSession session) {
		HttpHeaders headers = session.getHandshakeHeaders();
		List<String> cookies = headers.get("cookie");
		if (cookies!=null) {
			for (String cookie : cookies)
				if (cookie.startsWith("JSESSIONID=")) {
					String httpSessionId = cookie.substring("JSESSIONID=".length());
					HttpSession httpSession = Manager.get().findHttpSession(httpSessionId);
					String projectId = httpSession.getAttribute("projectId").toString();
					Optional<Project> optProject = Manager.get().getProjectDAO().findById(projectId);
					if (optProject.isPresent()) {
						Project project = optProject.get();
						return project;
					}
				}
		}
		return null;
	}
	
	public static synchronized void send(WebSocketSession session, JSONObject jso) {
		if (session.isOpen()) {
			WebSocketMessage<?> wsMessage=new TextMessage(jso.toString());
			try {
				session.sendMessage(wsMessage);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public static synchronized void send(WebSocketSession session, Object... typesAndValues) {
		if (session.isOpen()) {
			JSONObject jso = new JSONObject();
			int i=0;
			while (i<typesAndValues.length) {
				jso.put(typesAndValues[i].toString(), typesAndValues[i+1]);
				i+=2;
			}
			WebSocketMessage<?> wsMessage=new TextMessage(jso.toString());
			try {
				session.sendMessage(wsMessage);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
