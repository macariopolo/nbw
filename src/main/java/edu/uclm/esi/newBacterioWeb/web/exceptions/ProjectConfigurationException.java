package edu.uclm.esi.newBacterioWeb.web.exceptions;

public class ProjectConfigurationException extends Exception {

	private String key;

	public ProjectConfigurationException(String key, String message) {
		super(message);
		this.key = key;
	}

	public String getKey() {
		return key;
	}
}
