package edu.uclm.esi.newBacterioWeb.web.controllers;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import edu.uclm.esi.newBacterioWeb.model.Emulator;
import edu.uclm.esi.newBacterioWeb.model.RemoteHost;
import edu.uclm.esi.newBacterioWeb.model.devices.DeviceProxy;

@RestController
public class RemoteDevicesController {
	
	@GetMapping(value = "/loadEmulators", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Emulator> loadEmulators(HttpSession session) throws Exception {
		List<Emulator> emulators = new ArrayList<>();
		Iterable<RemoteHost> remoteHosts = Manager.get().getRemoteHostDAO().findAll();
		HttpClientBuilder builder = HttpClientBuilder.create();

		for (RemoteHost host : remoteHosts) {
			if (!host.isActive())
				continue;
			CloseableHttpClient client = builder.build();
			HttpGet httpGet = new HttpGet("http://" + host.getIp() + ":" + host.getPort() + "/loadEmulators");
			try {
				StringResponseHandler responseHandler = new StringResponseHandler();
				String responseBody = client.execute(httpGet, responseHandler);
				JSONArray jsa = new JSONArray(responseBody);
				String emulator;
				for (int i = 0; i < jsa.length(); i++) {
					emulator = jsa.getString(i);
					emulators.add(new Emulator(host.getIp(), host.getPort(), emulator));
				}
			} catch (HttpHostConnectException e) {
				
			} finally {
				client.close();
			}
		}
		return emulators;
	}
	
	@PostMapping(value = "/launchEmulators", produces = MediaType.APPLICATION_JSON_VALUE)
	public void launchEmulators(HttpSession session, @RequestBody List<Emulator> emulators) throws Exception {
		HttpClientBuilder builder = HttpClientBuilder.create();
		for (Emulator emulator : emulators) {
			CloseableHttpClient client = builder.build();
			HttpPost httpPost = new HttpPost("http://" + emulator.getIp() + ":" + emulator.getPort() + "/launchEmulator");
			StringEntity requestEntity = new StringEntity(emulator.getName(), ContentType.APPLICATION_JSON);
			httpPost.setEntity(requestEntity);
			try {
				StringResponseHandler responseHandler = new StringResponseHandler();
				client.execute(httpPost, responseHandler);
			} finally {
				client.close();
			}
		}
	}
	
	@GetMapping(value = "/loadRemoteDevices")
	public List<DeviceProxy> loadRemoteDevices(HttpSession session) throws Exception {
		Iterable<RemoteHost> remoteHosts = Manager.get().getRemoteHostDAO().findAll();
		HttpClientBuilder builder = HttpClientBuilder.create();

		for (RemoteHost host : remoteHosts) {
			CloseableHttpClient client = builder.build();
			HttpGet httpGet = new HttpGet("http://" + host.getIp() + ":" + host.getPort() + "/loadDevices");
			try {
				StringResponseHandler responseHandler = new StringResponseHandler();
				String responseBody = client.execute(httpGet, responseHandler);
				JSONArray jsa = new JSONArray(responseBody);
				JSONObject jsoDevice;
				for (int i = 0; i < jsa.length(); i++) {
					jsoDevice = jsa.getJSONObject(i);
					Manager.get().addRemoteDevice(host.getIp(), host.getPort(), jsoDevice.getString("name"));
				}
			} catch (HttpHostConnectException e) {
				
			} finally {
				client.close();
			}
		}
		return Manager.get().getDevices();
	}

	@PostMapping(value = "/postAPK", headers = "content-type=multipart/form-data")
	@ResponseBody
	public String postAPK(HttpServletRequest request, HttpServletResponse response, @RequestParam("deviceName") String deviceName, @RequestParam("file") MultipartFile apkFile) {
		try(InputStream is = apkFile.getInputStream()) {
			String fileName = apkFile.getOriginalFilename();
			fileName = trim(fileName);
			fileName = Manager.get().getReceivedAPKs() + fileName;
			try(FileOutputStream fos = new FileOutputStream(fileName)) {
				byte[] buffer = new byte[4096];
				int read = 0;
		        while ((read = is.read(buffer)) != -1) {
		            fos.write(buffer, 0, read);
		        }
			} 
		} catch (IOException e) {
			JSONArray errorLines = new JSONArray().put(e.getMessage());
			return new JSONObject().put("returnCode", -1).put("errorLines", errorLines).toString();
		}
		return new JSONObject().put("returnCode", 0).toString();
	}
	
	private String trim(String fileName) {
		if (fileName.indexOf('/')!=-1)
			fileName = fileName.substring(fileName.lastIndexOf('/')+1);
		else if (fileName.indexOf('\\')!=-1)
			fileName = fileName.substring(fileName.lastIndexOf('\\')+1);
		return fileName;
	}
}
