package edu.uclm.esi.newBacterioWeb.web.controllers;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import edu.uclm.esi.newBacterioWeb.model.RemoteHost;

@RestController
public class RemoteHostsController {
	
	@GetMapping(value = "/getRemoteHosts")
	public List<RemoteHost> getRemoteHosts() {
		List<RemoteHost> hosts = Manager.get().getRemoteHostDAO().findAll(Sort.by(Sort.DEFAULT_DIRECTION, "ip"));
		for (int i=hosts.size()-1; i>=0; i--) {
			RemoteHost host = hosts.get(i);
			if (!host.isActive())
				hosts.remove(i);
		}
		return hosts;
	}
	
	@DeleteMapping(value = "/deleteHost")
	public Iterable<RemoteHost> deleteHost(HttpSession session, @RequestBody RemoteHost host) throws Exception {
		Manager.get().getRemoteHostDAO().deleteById(host.getIp());
		return getRemoteHosts();
	}
	
	@PutMapping(value = "/addHost")
	public Iterable<RemoteHost> addHost(HttpSession session, @RequestBody RemoteHost host) throws Exception {
		Manager.get().getRemoteHostDAO().save(host);
		return getRemoteHosts();
	}
}
