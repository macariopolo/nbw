package edu.uclm.esi.newBacterioWeb.operators.classic;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.body.VariableDeclarator;
import com.github.javaparser.ast.expr.AssignExpr;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.expr.MethodCallExpr;
import com.github.javaparser.resolution.types.ResolvedType;

import edu.uclm.esi.newBacterioWeb.model.ProjectFile;

public class SIR extends ClassicMutationOperator{
	
	@Override
	public List<Node> loadMutableNodes() {
		List<MethodCallExpr>calls=this.compilationUnit.getCompilationUnit().findAll(MethodCallExpr.class);
		ArrayList<Node> mutableExpressions=filter(calls);
		return mutableExpressions;
	}

	private ArrayList<Node> filter(List<MethodCallExpr> calls) {
		ArrayList<Node> result=new ArrayList<>();
		for (int i=0; i<calls.size(); i++) {
			MethodCallExpr call = calls.get(i);
			if (call.getNameAsString().equals("getSystemService")){ 
				try {
					Node parentOpcion1 = getContainer(call, AssignExpr.class);
					Node parentOpcion2 = getContainer(call, VariableDeclarator.class);
					if(parentOpcion1!=null || parentOpcion2!=null){
					   String owner=getOwner(call);
						if (owner.contains("android.content.Context")|| owner.contains("android.app.Activity")){
						   Expression argument = call.getArguments().get(0); 
						   ResolvedType type=argument.calculateResolvedType();
						   if (!type.isArray() && getType(type).equals("java.lang.String"))
								   result.add(call);
						}
					} 
			   }catch (Exception e1) {}  
			}
		}
		return result;
	}
	
	@Override
	protected void generateMutants(ProjectFile originalFile, List<Node> mutableNodes, int[] projectCounter) {
		Iterator<Node> itMutableExpressions = mutableNodes.iterator();
		String stm="null";
		Node replaceStatement = StaticJavaParser.parseExpression(stm);
		String argumentNull="(String) null";
		Expression replaceArgument = StaticJavaParser.parseExpression(argumentNull);
		Node parent = null;
		while (itMutableExpressions.hasNext()) {
			MethodCallExpr call = (MethodCallExpr) itMutableExpressions.next();
			Expression originalArgument=call.getArgument(0);
			Optional<Node> optParent = call.getParentNode();
			parent = optParent.get();
			for(int i=0;i<2;i++){
				if(i==0){
					parent.replace(call, replaceStatement);
					projectCounter[0]++;
					saveClassicMutant(originalFile, projectCounter[0]);
					parent.replace(replaceStatement, call);
				}
				if(i==1){
					call.setArgument(0, replaceArgument);
					projectCounter[0]++;
					saveClassicMutant(originalFile, projectCounter[0]);
					call.setArgument(0, originalArgument);
				}	
			}
		}
	}

	@Override
	public String getDescription() {
		return "Service Identifier not Returned (replacing by null)";
	}

	@Override
	public String getFamilyName() {
		return "System-level service";
	}
}
