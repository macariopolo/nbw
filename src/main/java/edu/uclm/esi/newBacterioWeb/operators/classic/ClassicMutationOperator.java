package edu.uclm.esi.newBacterioWeb.operators.classic;

import java.util.List;

import org.springframework.web.socket.WebSocketSession;

import com.github.javaparser.ast.Node;

import edu.uclm.esi.newBacterioWeb.model.ClassicMutant;
import edu.uclm.esi.newBacterioWeb.model.ProjectFile;
import edu.uclm.esi.newBacterioWeb.operators.MutationOperator;
import edu.uclm.esi.newBacterioWeb.operators.WrapperCompilationUnit;
import edu.uclm.esi.newBacterioWeb.web.controllers.Manager;
import edu.uclm.esi.newBacterioWeb.web.ws.WSMutantsGeneration;

public abstract class ClassicMutationOperator extends MutationOperator {
	
	protected void saveClassicMutant(ProjectFile originalFile, int mutantIndex) {
		ClassicMutant mutant = new ClassicMutant();
		mutant.setOriginalFile(originalFile);
		mutant.setCode(this.compilationUnit.getCompilationUnit().toString());
		mutant.setIndex(mutantIndex);
		mutant.setOperator(this.getClass().getSimpleName());
		Manager.get().getClassicMutantDAO().save(mutant);
		WSMutantsGeneration.send(session, "type", "CLASSIC_MUTANT", "index", mutantIndex);
	}

	public void generateMutants(WebSocketSession session, ProjectFile originalFile, WrapperCompilationUnit wrapper, int[] projectCounter) {
		this.session = session;
		this.compilationUnit = wrapper;
		List<Node> mutableNodes=null;
		try{
			mutableNodes = this.loadMutableNodes();
		} catch (Exception e) {}
		if(mutableNodes!=null){
			if(!mutableNodes.isEmpty())
				generateMutants(originalFile, mutableNodes, projectCounter);
			else
				WSMutantsGeneration.send(session, "type", "CLASSIC_MUTANT", "index", projectCounter[0]);
	    }
		else 
			WSMutantsGeneration.send(session, "type", "CLASSIC_MUTANT", "index", projectCounter[0]);
	}

	
	protected abstract List<Node> loadMutableNodes();

	protected abstract void generateMutants(ProjectFile originalFile, List<Node> mutableNodes, int[] projectCounter);	
}
