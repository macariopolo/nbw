package edu.uclm.esi.newBacterioWeb.operators.untch;

import java.util.List;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.expr.MethodCallExpr;

import edu.uclm.esi.newBacterioWeb.operators.Filters;

public class IKIPE extends UntchMutationOperator{
	
	@Override
	protected Node nextMutableNode() {
		List<MethodCallExpr> calls = this.compilationUnit.getCompilationUnit().findAll(MethodCallExpr.class);
	    List<Node> mutableExpressions= Filters.filterByClassMethodNumberOfArguments(this.compilationUnit, calls, "android.content.Intent", "putExtra", 2);
	    for (int i=0; i<mutableExpressions.size(); i++) {
	    	return mutableExpressions.get(i);
	    }
		return null;
	}

	@Override  
	protected Expression generateMutants(Node node, int[] counter) {
		MethodCallExpr call = (MethodCallExpr) node;
		List<Node> elements = call.getChildNodes();
		String md="";
		if(elements.size()>3)
			md ="\t\tMutantDriver.IKIPE("+ elements.get(0);
		else
			md ="\t\tMutantDriver.IKIPE("+ "this";
		md = md + ", " + call.getArgument(0) +  ", " + call.getArgument(1) + ", " + ++counter[0] + ")";
		Expression replaceableExpr = StaticJavaParser.parseExpression(md);
		return replaceableExpr;
	}

	@Override
	public String getFamilyName() {
		return "Android intents";
	}

	@Override
	public String getDescription() {
		return "Invalid key intent put extra";
	}
	
	
}
