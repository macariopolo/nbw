package edu.uclm.esi.newBacterioWeb.operators.classic;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.expr.BinaryExpr;
import com.github.javaparser.ast.expr.BinaryExpr.Operator;
import com.github.javaparser.ast.expr.Expression;

import edu.uclm.esi.newBacterioWeb.model.ProjectFile;

public abstract class BinaryMutationOperator extends ClassicMutationOperator {

	@Override
	public String getFamilyName() {
		return "Traditional";
	}
	
	public List<Node> loadMutableNodes() {
		List<BinaryExpr> binaryExpressions = this.compilationUnit.getCompilationUnit().findAll(BinaryExpr.class);
		return filter(binaryExpressions);
	}

	protected void generateMutants(ProjectFile originalFile, List<Node> mutableNodes, int[] projectCounter) {
		BinaryExpr bExpr;
		Expression expr;
		for (int i=0; i<mutableNodes.size(); i++) {
			bExpr = (BinaryExpr) mutableNodes.get(i);
			expr = bExpr.getLeft();
			if (expr instanceof BinaryExpr) {
				BinaryExpr beLeft = (BinaryExpr) expr;
				if (operatorIsApplicable(beLeft.getOperator()))
					generateMutants(originalFile, beLeft, projectCounter);
			}
			expr = bExpr.getRight();
			if (expr instanceof BinaryExpr) {
				BinaryExpr beRight = (BinaryExpr) expr;
				if (operatorIsApplicable(beRight.getOperator()))
					generateMutants(originalFile, beRight, projectCounter);
			}
			generateMutants(originalFile, bExpr, projectCounter);
		}
	}
	
	protected void generateUntchMutants(ProjectFile originalFile, List<Node> mutableNodes, int[] counter) {
		for (int i=0; i<mutableNodes.size(); i++) {
			BinaryExpr originalExpr = (BinaryExpr) mutableNodes.get(i);
			Node parent = originalExpr.getParentNode().get();
			Node mutatedExpression = generateUntchMutants(originalExpr, counter);
			parent.replace(originalExpr, mutatedExpression);
			this.compilationUnit.setCode(this.compilationUnit.getCompilationUnit().toString());
		}
	}

	private Expression generateUntchMutants(BinaryExpr originalExpr, int[] counter) {
		Expression left = originalExpr.getLeft();
		if (left instanceof BinaryExpr) {
			BinaryExpr beLeft = (BinaryExpr) left;
			if (operatorIsApplicable(beLeft.getOperator())) {
				Expression mutatedLeft = generateUntchMutants(beLeft, counter);
				originalExpr.setLeft(mutatedLeft);
			}	
		}
		Expression right = originalExpr.getRight();
		if (right instanceof BinaryExpr) {
			BinaryExpr beRight = (BinaryExpr) right;
			if (operatorIsApplicable(beRight.getOperator())) {
				Expression mutatedRight = generateUntchMutants(beRight, counter);
				originalExpr.setRight(mutatedRight);
			}
		}
		Operator op=originalExpr.getOperator();
		Operator[] otherOperators = this.getOtherOperators(op);
		
		StringBuilder sbTransformation=new StringBuilder("MutantDriver." + op.name() + "(" + originalExpr.getLeft().toString() + ", " + originalExpr.getRight().toString() + ", ");
		for (int i=0; i<otherOperators.length; i++) {
			sbTransformation.append(++counter[0]);
			if (i<otherOperators.length-1)
				sbTransformation.append(", ");
		}
		sbTransformation.append(")");
		Expression mutatedExpression = StaticJavaParser.parseExpression(sbTransformation.toString());
		return mutatedExpression;
	}

	private void generateMutants(ProjectFile originalFile, BinaryExpr expr, int[] counter) {
		Operator op = expr.getOperator();
		Operator[] otherOperators = this.getOtherOperators(op);
		for (int j=0; j<otherOperators.length; j++) {
			Operator otherOperator = otherOperators[j];
			expr.setOperator(otherOperator);
			counter[0]++;
			saveClassicMutant(originalFile, counter[0]);
			expr.setOperator(op);
		}
	}

	private ArrayList<Node> filter(List<BinaryExpr> binaryExpressions) {
		ArrayList<Node> mutableExpressions = new ArrayList<>();
		for (int i=0; i<binaryExpressions.size(); i++) {
			BinaryExpr originalExpr = binaryExpressions.get(i);
			Operator operator = originalExpr.getOperator();
			if (operatorIsApplicable(operator) && typesAreCompatible(originalExpr)) 
				mutableExpressions.add(originalExpr);
		}
		
		int size = mutableExpressions.size();
		for (int i=size-1; i>=0; i--) {
			BinaryExpr originalExpr = (BinaryExpr) mutableExpressions.get(i);
			Optional<Node> optValue = originalExpr.getParentNode();
			if (optValue.isPresent()) {
				Node parent = optValue.get();
				if (parent instanceof BinaryExpr && includes(mutableExpressions, parent))
					mutableExpressions.remove(i);
			}
		}
		return mutableExpressions;
	}
	
	private boolean includes(ArrayList<Node> mutableExpressions, Node parent) {
		for (int i=0; i<mutableExpressions.size(); i++)
			if (mutableExpressions.get(i)==parent)
				return true;
		return false;
	}
	
	protected abstract boolean typesAreCompatible(BinaryExpr originalExpr);

	protected final boolean operatorIsApplicable(Operator operator) {
		Operator[] applicableOperators = this.getApplicableOperators();
		for (int i=0; i<applicableOperators.length; i++) 
			if (applicableOperators[i]==operator)
				return true;
		return false;
	}
	
	protected Operator[] getOtherOperators(Operator op) {
		Operator[] applicableOperators = getApplicableOperators();
		Operator[] result = new Operator[applicableOperators.length-1];
		int cont=0;
		for (int i=0; i<applicableOperators.length; i++)
			if (applicableOperators[i]!=op)
				result[cont++]=applicableOperators[i];
		return result;
	}

	protected abstract Operator[] getApplicableOperators();

}
