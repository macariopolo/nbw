package edu.uclm.esi.newBacterioWeb.operators;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.expr.MethodCallExpr;
import com.github.javaparser.ast.expr.ObjectCreationExpr;
import com.github.javaparser.ast.type.ClassOrInterfaceType;
import com.github.javaparser.resolution.types.ResolvedArrayType;
import com.github.javaparser.resolution.types.ResolvedType;

public class Filters {
	public static ArrayList<Node> filterByClassMethodNames(WrapperCompilationUnit compilationUnit, List<MethodDeclaration> calls, String className, String... methodNames) {
		ArrayList<Node> result=new ArrayList<>();
		MethodDeclaration call;
		ClassOrInterfaceDeclaration declaringClass;
		ClassOrInterfaceType superclass;
		NodeList<ClassOrInterfaceType> superclasses;
		for (int i=0; i<calls.size(); i++) {
			call = calls.get(i);
			declaringClass = getDeclaringClass(compilationUnit, call);
			if (declaringClass!=null) {
				superclasses= declaringClass.getExtendedTypes();
				if (superclasses.isNonEmpty()) {
					superclass = superclasses.get(0);
					if (superclass.toString().contains(className)) {
						for (int j=0; j<methodNames.length; j++) {
							if (call.getNameAsString().equals(methodNames[j])) {
								result.add(call);
								break;
							}
						}
					}
				}
			}
		}
		return result;
	}
	
	private static ClassOrInterfaceDeclaration getDeclaringClass(WrapperCompilationUnit compilationUnit, MethodDeclaration call) {
		Optional<Node> optParent = call.getParentNode();
		Node parent;
		while (optParent.isPresent()) {
			parent = optParent.get();
			if (parent instanceof ClassOrInterfaceDeclaration)
				return (ClassOrInterfaceDeclaration) parent;
			optParent = parent.getParentNode();
		}
		return null;
	}

	public static ArrayList<MethodDeclaration> filterByMethodNames(WrapperCompilationUnit compilationUnit, List<MethodDeclaration> calls, String... methodNames) {
		ArrayList<MethodDeclaration> result=new ArrayList<>();
		MethodDeclaration call;
		for (int i=0; i<calls.size(); i++) {
			call = calls.get(i);
			for (int j=0; j<methodNames.length; j++) {
				if (call.getNameAsString().equals(methodNames[j])) {
					result.add(call);
					break;
				}
			}
		}
		return result;
	}
	
	public static ArrayList<Node> filterByAParameterType(WrapperCompilationUnit compilationUnit, List<MethodDeclaration> methods, String parameterName) {
		ArrayList<Node> result=new ArrayList<>();
		for (int i=0; i<methods.size(); i++)
			if (methods.get(i).getParameterByType(parameterName).isPresent() && !methods.get(i).getClass().isInterface())
				result.add(methods.get(i));
		return result;
	}
	
	public static ArrayList<Node> filterByClassMethodNumberOfArguments(WrapperCompilationUnit compilationUnit, List<MethodCallExpr> calls, String className, String methodName, int arguments) {
		ArrayList<Node> result=new ArrayList<>();
		ResolvedType instanceType;
		Optional<Expression> optInstanceScope;
		String type;
		for (int i=0; i<calls.size(); i++) {
			MethodCallExpr call = calls.get(i);
			if (call.getNameAsString().equals(methodName) && call.getArguments().size()==arguments) { 
				try {
					optInstanceScope = call.getScope();
					if (optInstanceScope.isPresent()) {
						Expression scope = optInstanceScope.get();
						instanceType = scope.calculateResolvedType();
						type = getType(compilationUnit, instanceType);
						if (type.equals(className))
							result.add(call);
					}
				} catch (Exception e) {}
			}
		}
		return result;
	}
	
	public static ArrayList<Node> filterByClassMethod(WrapperCompilationUnit compilationUnit, List<MethodCallExpr> calls, String className, String methodName) {
		ArrayList<Node> result=new ArrayList<>();
		ResolvedType instanceType;
		Optional<Expression> optInstanceScope;
		String type;
		MethodCallExpr call;
		for (int i=0; i<calls.size(); i++) {
			call = calls.get(i);
			if (call.getNameAsString().equals(methodName)) { 
				try {
					optInstanceScope = call.getScope();
					if (optInstanceScope.isPresent()) {
						Expression scope = optInstanceScope.get();
						instanceType = scope.calculateResolvedType();
						type = getType(compilationUnit, instanceType);
						if (type.equals(className))
							result.add(call);
					}
				} catch (Exception e) {
				}
			}
		}
		return result;
	}
	
	public static ArrayList<Node> filterByClassMethodSecondArgument(WrapperCompilationUnit compilationUnit, List<MethodCallExpr> calls, String className, String methodName, String parameterType) {
		ArrayList<Node> result=new ArrayList<>();
		Expression argument;
		ResolvedType instanceType, argumentType;
		Optional<Expression> optInstanceScope;
		String type;
		for (int i=0; i<calls.size(); i++) {
			MethodCallExpr call = calls.get(i);
			if (call.getNameAsString().equals(methodName) && call.getArguments().isNonEmpty()) { 
				try {
					optInstanceScope = call.getScope();
					argument = call.getArgument(1);//
					argumentType = argument.calculateResolvedType();
					if (argumentType instanceof ResolvedArrayType)
						continue;
					type = getType(compilationUnit, argumentType);
					if (optInstanceScope.isPresent() && type.equals(parameterType)) {
						Expression scope = optInstanceScope.get();
						instanceType = scope.calculateResolvedType();
						type = getType(compilationUnit, instanceType);
						if (type.equals(className))
							result.add(call);
					}
				} catch (Exception e) {}
			}
		}
		return result;
	}
	
	public static ArrayList<Node> filterByClassMethodFirstArgument(WrapperCompilationUnit compilationUnit, List<MethodCallExpr> calls, String className, String methodName, String parameterType) {
		ArrayList<Node> result=new ArrayList<>();
		Expression argument;
		ResolvedType instanceType, argumentType;
		Optional<Expression> optInstanceScope;
		String type;
		for (int i=0; i<calls.size(); i++) {
			MethodCallExpr call = calls.get(i);
			if (call.getNameAsString().equals(methodName) && call.getArguments().isNonEmpty()) { 
				try {
					optInstanceScope = call.getScope();
					argument = call.getArgument(0);
					argumentType = argument.calculateResolvedType();
					if (argumentType instanceof ResolvedArrayType)
						continue;
					type = getType(compilationUnit, argumentType);
					if (optInstanceScope.isPresent() && type.equals(parameterType)) {
						Expression scope = optInstanceScope.get();
						instanceType = scope.calculateResolvedType();
						type = getType(compilationUnit, instanceType);
						if (type.equals(className))
							result.add(call);
					}
				} catch (Exception e) {}
			}
		}
		return result;
	}
	
	public static ArrayList<Node> filterByClassConstructorNumberOfArguments(WrapperCompilationUnit compilationUnit, List<ObjectCreationExpr> calls, String constructorName, int numberOfArguments) {
		ArrayList<Node> result=new ArrayList<>();
		ResolvedType objectType;
		String type;
		for (int i=0; i<calls.size(); i++) {
			try {
				ObjectCreationExpr call = calls.get(i);
				objectType = call.calculateResolvedType();
				type = getType(compilationUnit, objectType);
				if (type.equals(constructorName) && call.getArguments().size()==numberOfArguments)
					result.add(call);
			} catch (Exception e) {}
		}
		return result;
	}
	
	public static ArrayList<Node> filterByNameCallAndMethodNames(WrapperCompilationUnit compilationUnit, List<MethodCallExpr> calls, String className, String... methodNames) {
		ArrayList<Node> result=new ArrayList<>();
		ResolvedType instanceType;
		Optional<Expression> optInstanceScope;
		String type;
		for (int i=0; i<calls.size(); i++) {
			MethodCallExpr call = calls.get(i);
			for (int j=0; j<methodNames.length; j++) {
				if (call.getNameAsString().equals(methodNames[j])) {
					try {
						optInstanceScope = call.getScope();
						if (optInstanceScope.isPresent()) {
							Expression scope = optInstanceScope.get();
							instanceType = scope.calculateResolvedType();
							type = getType(compilationUnit, instanceType);
							if (type.equals(className))
								result.add(call);
						}
					} catch (Exception e) {}
				}
			}
		}
		return result;
	}
	
	public static ArrayList<Node> filterByNameCallAndFirstParameter(WrapperCompilationUnit compilationUnit, List<MethodCallExpr> calls, String nameCall, String parameterType, int arguments) {
		ArrayList<Node> result=new ArrayList<>();
		ResolvedType argumentType;
		MethodCallExpr call;
		String type;
		for (int i=0; i<calls.size(); i++) {
			call = calls.get(i);
			if (call.getNameAsString().equals(nameCall) && call.getArguments().size()==arguments) {
			    argumentType=call.getArgument(0).calculateResolvedType();
				 type=getType(compilationUnit, argumentType);
				if ( type.equals(parameterType)) 
						result.add(call);
			}
		}
		return result;
	}
	
	public static ArrayList<Node> filterByClassNamesMethod(WrapperCompilationUnit compilationUnit, List<MethodDeclaration> methodDecla, String methodName, String... classNames) {
		ArrayList<Node> result=new ArrayList<>();
		MethodDeclaration method;
		ClassOrInterfaceDeclaration declaringClass;
		ClassOrInterfaceType superclass = null;
		NodeList<ClassOrInterfaceType> superclasses;
		for (int i=0; i<methodDecla.size(); i++) {
			method = methodDecla.get(i);
			declaringClass = getDeclaringClass(compilationUnit, method);
			if (declaringClass!=null) {
				superclasses= declaringClass.getExtendedTypes();
				if (superclasses.isNonEmpty()) {
					superclass = superclasses.get(0);
						for(int k=0; k<classNames.length; k++){
							if (superclass.toString().contains(classNames[k])) {
								if (method.getNameAsString().equals(methodName)) {
								result.add(method);
								break;
								}
							}
						}
					}
				}
			}
		return result;
	}
	
	private static String getType(WrapperCompilationUnit wcu, ResolvedType type){
		return wcu.getResolvedType(type);
	}
	
	public static String getPrimitiveTypeName(String type) { 
		if (type.equals("double"))
			return "Double";
		else if (type.equals("float"))
			return "Float";
		else if (type.equals("char"))
			return "char";
		else if (type.equals("long"))
			return "Long";
		else if (type.equals("int"))
			return "Integer";
		else if (type.equals("boolean"))
			return "Boolean";
		else if (type.equals("short"))
			return "Short";
		else if (type.equals("byte"))
			return "Byte";
		return type;
	}
}
