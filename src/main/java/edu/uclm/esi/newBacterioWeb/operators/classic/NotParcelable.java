package edu.uclm.esi.newBacterioWeb.operators.classic;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.expr.AnnotationExpr;
import com.github.javaparser.ast.type.ClassOrInterfaceType;

import edu.uclm.esi.newBacterioWeb.model.ProjectFile;

public class NotParcelable extends ClassicMutationOperator {

	public List<Node> loadMutableNodes() {
		List<Node> expressions = new ArrayList<>();
		ClassOrInterfaceDeclaration clazz = this.compilationUnit.getClass0();
		NodeList<ClassOrInterfaceType> interfaces = clazz.getImplementedTypes();
		ClassOrInterfaceType interfaz;
		for (int i=0; i<interfaces.size(); i++) {
			interfaz = interfaces.get(i);
			if (interfaz.toString().equals("Parcelable")) {
				expressions.add(clazz);
				break;
			}				
		}
		return expressions;
	}
	
	@Override
	public String getFamilyName() {
		return "Android programming";
	}

	@Override
	protected void generateMutants(ProjectFile originalFile, List<Node> mutableNodes, int[] projectCounter) {
		ClassOrInterfaceDeclaration clazz = (ClassOrInterfaceDeclaration) mutableNodes.get(0);
		NodeList<ClassOrInterfaceType> interfaces = clazz.getImplementedTypes();
		ClassOrInterfaceType interfaz;
		for (int i=0; i<interfaces.size(); i++) {
			interfaz = interfaces.get(i);
			if (interfaz.toString().equals("Parcelable")) {
				clazz.remove(interfaz);
				break;
			}				
		}
		
		String[] methodNames = { "describeContents", "writeToParcel" };
		for (int i=0; i<methodNames.length; i++) {
			List<MethodDeclaration> methods = clazz.getMethodsByName(methodNames[i]);
			for (int j=0; j<methods.size(); j++) {
				MethodDeclaration method = methods.get(j);
				Optional<AnnotationExpr> optAnnotation = method.getAnnotationByName("Override");
				if (optAnnotation.isPresent())
					method.remove(optAnnotation.get());
			}
		}
		projectCounter[0]++;
		saveClassicMutant(originalFile, projectCounter[0]);
	}
	
	@Override
	public String getDescription() {
		return "Removes 'implements Parcelable' and '@Override'";
	}
}
