package edu.uclm.esi.newBacterioWeb.operators.untch;

import java.util.List;

import org.springframework.web.socket.WebSocketSession;

import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.expr.Expression;

import edu.uclm.esi.newBacterioWeb.model.ProjectFile;
import edu.uclm.esi.newBacterioWeb.model.UntchMutantOperator;
import edu.uclm.esi.newBacterioWeb.operators.MutationOperator;
import edu.uclm.esi.newBacterioWeb.operators.WrapperCompilationUnit;
import edu.uclm.esi.newBacterioWeb.web.controllers.Manager;
import edu.uclm.esi.newBacterioWeb.web.ws.WSMutantsGeneration;

public abstract class UntchMutationOperator_Insertion extends MutationOperator {
	
	public void generateMutants(WebSocketSession session, ProjectFile originalFile, WrapperCompilationUnit wrapper, int[] projectCounter, String projectId, String originalFileId, String mutantId) {
		this.session = session;
		this.compilationUnit = wrapper;
		List<Node> mutableNodes=this.loadMutableNodes();
		if(!mutableNodes.isEmpty())
			generateMutants(originalFile, mutableNodes, projectCounter);
	}
	
	protected abstract List<Node> loadMutableNodes();

	protected abstract void generateMutants(ProjectFile originalFile, List<Node> mutableNodes, int[] projectCounter);
	
	protected String getMutationType(){
		return "Replacement";
	}

}
