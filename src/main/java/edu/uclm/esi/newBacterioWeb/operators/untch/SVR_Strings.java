package edu.uclm.esi.newBacterioWeb.operators.untch;

import java.util.List;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.expr.MethodCallExpr;
import com.github.javaparser.resolution.types.ResolvedType;

public class SVR_Strings extends UntchMutationOperator {
	
	@Override
	protected Node nextMutableNode() {
		List<MethodCallExpr> calls = this.compilationUnit.getCompilationUnit().findAll(MethodCallExpr.class);
		MethodCallExpr call;
		for (int i=0; i<calls.size(); i++) {
			call = calls.get(i);
			if(call.toString().contains("MutantDriver"))
				continue;
			for (int j=0; j<call.getArguments().size(); j++) {
				Expression argument = call.getArgument(j);
				try {
					ResolvedType argumentType = argument.calculateResolvedType();
					if (argumentType.describe().equals("java.lang.String")) {
						return call;
					}
				} catch(Exception e) {}
			}
		}
		return null;
	}
	
	@Override
	protected Expression generateMutants(Node node, int[] projectCounter) {		
			MethodCallExpr call = (MethodCallExpr) node;
			for (int j=0; j<call.getArguments().size(); j++) {
				Expression argument = call.getArgument(j);
				try {
					ResolvedType argumentType = argument.calculateResolvedType();
					if (argumentType.describe().equals("java.lang.String")) {
						String md = "MutantDriver.SVR_STRINGS(" + argument.toString() + ", " + ++projectCounter[0] + ")";
						Expression newExpression = StaticJavaParser.parseExpression(md);
						call.setArgument(j, newExpression);
						return call;
					}
				} catch(Exception e) {}
			}
			return null;
	}

	@Override
	public String getFamilyName() {
		return "Traditional";
	}

	@Override
	public String getDescription() {
		return "Replace a String par by \"\"";
	}
	
}
