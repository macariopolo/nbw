package edu.uclm.esi.newBacterioWeb.operators.untch;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import com.github.javaparser.JavaToken;
import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.expr.FieldAccessExpr;

import edu.uclm.esi.newBacterioWeb.model.ProjectFile;

public class ETR extends UntchMutationOperator_Insertion{
	
	@Override
	protected List<Node> loadMutableNodes() {
		List<FieldAccessExpr> accesExp = this.compilationUnit.getCompilationUnit().findAll(FieldAccessExpr.class);
		return filter(accesExp); 
	}

	@Override
	protected void generateMutants(ProjectFile originalFile, List<Node> mutableNodes, int[] projectCounter) {
		Iterator<Node> itMutableExpressions = mutableNodes.iterator();
		while (itMutableExpressions.hasNext()) {
			FieldAccessExpr field = (FieldAccessExpr) itMutableExpressions.next();
			String md = "MutantDriver.ETR(android.view.MotionEvent." + field.getNameAsString() + ", ";
			for (int i=0; i<3; i++)
				md = md +  ++projectCounter[0] + ", ";
			md = md.substring(0, md.length()-2);
			md = md + ")";
			Optional<Node> optParent = field.getParentNode();
			Node replacementNode = StaticJavaParser.parseExpression(md);
			Node parent = optParent.get();
			parent.replace(field, replacementNode);
		}
	}

	private ArrayList<Node> filter(List<FieldAccessExpr> accesExps) {
		ArrayList<Node> result=new ArrayList<>();
		FieldAccessExpr field;
		for (int i=0; i<accesExps.size(); i++){
			field= accesExps.get(i);
			Node parent = field.getParentNode().get();
			String optParentString=field.toString();
			if ((optParentString.contains("ACTION_UP") ||optParentString.contains("ACTION_DOWN")
					||optParentString.contains("ACTION_CANCEL")||optParentString.contains("ACTION_MOVE"))&& optParentString.contains("MotionEvent")){
				 JavaToken beginCase = parent.getTokenRange().get().getBegin();
				if(!beginCase.asString().equals("case"))
					result.add(field);
			}
		}
		return result;
	}

	@Override
	public String getFamilyName() {
		return "Event handler";
	}

	@Override
	public String getDescription() {
		return "OnTouch Event Replacement";
	}
	
}
