package edu.uclm.esi.newBacterioWeb.operators.classic;

import com.github.javaparser.ast.expr.BinaryExpr;
import com.github.javaparser.ast.expr.BinaryExpr.Operator;
import com.github.javaparser.ast.expr.Expression;

public class AOR extends BinaryMutationOperator {
	private static Operator[] applicableOperators = { Operator.PLUS, Operator.MINUS, Operator.MULTIPLY, Operator.DIVIDE, Operator.REMAINDER };

	@Override
	protected boolean typesAreCompatible(BinaryExpr expr) {
		Expression left = expr.getLeft();
		Expression right = expr.getRight();
		try {
			return true; // left.calculateResolvedType().isPrimitive() && right.calculateResolvedType().isPrimitive();
		} catch (RuntimeException e) {
			return left.toString().startsWith("MutantDriver.") && right.toString().startsWith("MutantDriver.");
		}
	}
	
	@Override
	protected Operator[] getApplicableOperators() {
		return applicableOperators;
	}

	@Override
	public String getDescription() {
		return "Arithmetic operator replacement (+ by -, *, etc.)";
	}
}
