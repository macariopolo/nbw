package edu.uclm.esi.newBacterioWeb.operators.untch;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.expr.BinaryExpr;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.expr.BinaryExpr.Operator;

public abstract class BinaryMutationOperator extends UntchMutationOperator {

	@Override
	public String getFamilyName() {
		return "Traditional";
	}
	
	@Override
	protected Expression generateMutants(Node node, int[] projectCounter) {
		BinaryExpr originalExpr = (BinaryExpr) node;
		Operator op=originalExpr.getOperator();
		Operator[] otherOperators = this.getOtherOperators(op);
		
		StringBuilder sbTransformation=new StringBuilder("MutantDriver." + op.name() + "(" + originalExpr.getLeft().toString() + ", " + originalExpr.getRight().toString() + ", ");
		for (int i=0; i<otherOperators.length; i++) {
			sbTransformation.append(++projectCounter[0]);
			if (i<otherOperators.length-1)
				sbTransformation.append(", ");
		}
		sbTransformation.append(")");
		Expression mutatedExpression = StaticJavaParser.parseExpression(sbTransformation.toString());
		return mutatedExpression;
	}
	//modifique esto,
	@Override
	protected Node nextMutableNode() {
		List<BinaryExpr> binaryExpressions = this.compilationUnit.getCompilationUnit().findAll(BinaryExpr.class);
		ArrayList<Node> expressions = filter(binaryExpressions);// pienso que hay q filtrar igual
		for (int i=0; i<expressions.size(); i++) {
			return expressions.get(i);
		}
	/*	BinaryExpr be;
		for (int i=0; i<binaryExpressions.size(); i++) {
			be = binaryExpressions.get(i);
			if (operatorIsApplicable(be.getOperator()))//
				return be;
		}*/
		return null;
	}
	
	private ArrayList<Node> filter(List<BinaryExpr> binaryExpressions) {
		ArrayList<Node> mutableExpressions = new ArrayList<>();
		for (int i=0; i<binaryExpressions.size(); i++) {
			BinaryExpr originalExpr = binaryExpressions.get(i);
			Operator operator = originalExpr.getOperator();
			if (operatorIsApplicable(operator) && typesAreCompatible(originalExpr)) 
				mutableExpressions.add(originalExpr);
		}
		
		int size = mutableExpressions.size();
		for (int i=size-1; i>=0; i--) {
			BinaryExpr originalExpr = (BinaryExpr) mutableExpressions.get(i);
			Optional<Node> optValue = originalExpr.getParentNode();
			if (optValue.isPresent()) {
				Node parent = optValue.get();
				if (parent instanceof BinaryExpr && includes(mutableExpressions, parent))
					mutableExpressions.remove(i);
			}
		}
		return mutableExpressions;
	}
	
	private boolean includes(ArrayList<Node> mutableExpressions, Node parent) {
		for (int i=0; i<mutableExpressions.size(); i++)
			if (mutableExpressions.get(i)==parent)
				return true;
		return false;
	}

	protected Operator[] getOtherOperators(Operator op) {
		Operator[] applicableOperators = getApplicableOperators();
		Operator[] result = new Operator[applicableOperators.length-1];
		int cont=0;
		for (int i=0; i<applicableOperators.length; i++)
			if (applicableOperators[i]!=op)
				result[cont++]=applicableOperators[i];
		return result;
	}
	
	protected abstract Operator[] getApplicableOperators();
	
	protected final boolean operatorIsApplicable(Operator operator) {
		Operator[] applicableOperators = this.getApplicableOperators();
		for (int i=0; i<applicableOperators.length; i++) 
			if (applicableOperators[i]==operator)
				return true;
		return false;
	}


}
