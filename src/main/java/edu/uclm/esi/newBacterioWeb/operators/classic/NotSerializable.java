package edu.uclm.esi.newBacterioWeb.operators.classic;

import java.util.ArrayList;
import java.util.List;

import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.type.ClassOrInterfaceType;

import edu.uclm.esi.newBacterioWeb.model.ProjectFile;

public class NotSerializable extends ClassicMutationOperator {

	public List<Node> loadMutableNodes() {
		List<Node> expressions = new ArrayList<>();
		ClassOrInterfaceDeclaration clazz = this.compilationUnit.getClass0();
		NodeList<ClassOrInterfaceType> interfaces = clazz.getImplementedTypes();
		ClassOrInterfaceType interfaz;
		for (int i=0; i<interfaces.size(); i++) {
			interfaz = interfaces.get(i);
			if (interfaz.toString().equals("Serializable")) {
				expressions.add(clazz);
				break;
			}				
		}
		return expressions;
	}
	
	@Override
	public String getFamilyName() {
		return "General programming";
	}

	@Override
	protected void generateMutants(ProjectFile originalFile, List<Node> mutableNodes, int[] projectCounter) {
		ClassOrInterfaceDeclaration clazz = (ClassOrInterfaceDeclaration) mutableNodes.get(0);
		NodeList<ClassOrInterfaceType> interfaces = clazz.getImplementedTypes();
		ClassOrInterfaceType interfaz;
		for (int i=0; i<interfaces.size(); i++) {
			interfaz = interfaces.get(i);
			if (interfaz.toString().equals("Serializable")) {
				clazz.remove(interfaz);
				break;
			}				
		}
		projectCounter[0]++;
		saveClassicMutant(originalFile, projectCounter[0]);
	}
	
	@Override
	public String getDescription() {
		return "Removes 'implements Serializable'";
	}
}
