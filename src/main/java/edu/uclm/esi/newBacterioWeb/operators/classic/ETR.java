package edu.uclm.esi.newBacterioWeb.operators.classic;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.github.javaparser.JavaToken;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.expr.FieldAccessExpr;

import edu.uclm.esi.newBacterioWeb.model.ProjectFile;

public class ETR extends ClassicMutationOperator {
	
	@Override
	public List<Node> loadMutableNodes() {
		List<FieldAccessExpr> accesExp = this.compilationUnit.getCompilationUnit().findAll(FieldAccessExpr.class);
		ArrayList<Node> mutableExpressions=filter(accesExp); 
		return mutableExpressions;
	}

	@Override
	protected void generateMutants(ProjectFile originalFile, List<Node> mutableNodes, int[] projectCounter) {
		Iterator<Node> itMutableExpressions = mutableNodes.iterator();
		while (itMutableExpressions.hasNext()) {
			FieldAccessExpr field = (FieldAccessExpr) itMutableExpressions.next();
			String name=field.getNameAsString();
			ArrayList<String> changes= getChangesList(field); 
			if(!changes.isEmpty()){
				for(int i=0;i<changes.size();i++){
					field.setName(changes.get(i));
					projectCounter[0]++;
					saveClassicMutant(originalFile, projectCounter[0]);
					field.setName(name);
				}
			}
		}
	}
	
	private ArrayList<Node> filter(List<FieldAccessExpr> accesExps) {
		ArrayList<Node> result=new ArrayList<>();
		FieldAccessExpr field;
		for (int i=0; i<accesExps.size(); i++){
			field= accesExps.get(i);
			Node parent = field.getParentNode().get();
			String optParentString=field.toString();
			if ((optParentString.contains("ACTION_UP") ||optParentString.contains("ACTION_DOWN")
					||optParentString.contains("ACTION_CANCEL")||optParentString.contains("ACTION_MOVE"))&& optParentString.contains("MotionEvent")){
				 JavaToken beginCase = parent.getTokenRange().get().getBegin();
				if(!beginCase.asString().equals("case"))
					result.add(field);
			}
		}
		return result;
	}
	
	private ArrayList<String> getChangesList(Node expression){
		String changes[]={"ACTION_DOWN", "ACTION_UP", "ACTION_CANCEL","ACTION_MOVE"};
		ArrayList<String> result=getMutants(changes, expression);
			if (!result.isEmpty())
				return result;
		return null;
	}
	
	private ArrayList<String> getMutants(String[] changes, Node expression) {
		 ArrayList<String> result=new ArrayList<>();
		int posEncontrado=-1;
		for (int i=0; i<changes.length; i++) {
			if (expression.toString().contains(changes[i])) {
				posEncontrado=i;
				break;
			}
		}
		if (posEncontrado!=-1) {
			for (int i=0; i<changes.length; i++)
				if (i!=posEncontrado) 
					result.add(changes[i]);
		}
		  return result;
		}


	@Override
	public String getFamilyName() {
		return "Event handler";
	}

	@Override
	public String getDescription() {
		return "OnTouch Event Replacement";
	}
	
}
