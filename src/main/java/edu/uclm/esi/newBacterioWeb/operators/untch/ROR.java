package edu.uclm.esi.newBacterioWeb.operators.untch;

import com.github.javaparser.ast.expr.BinaryExpr;
import com.github.javaparser.ast.expr.BinaryExpr.Operator;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.resolution.types.ResolvedType;

public class ROR extends BinaryMutationOperator {
	private static Operator[] applicableOperators = { Operator.EQUALS, Operator.NOT_EQUALS, Operator.LESS, Operator.GREATER, Operator.LESS_EQUALS, Operator.GREATER_EQUALS };

	@Override
	protected boolean typesAreCompatible(BinaryExpr expr) {
		Expression left = expr.getLeft();
		Expression right = expr.getRight();
		try {
			ResolvedType resolvedType = left.calculateResolvedType();
			if (!resolvedType.isPrimitive())
				return false;
			resolvedType = right.calculateResolvedType();
			if (!resolvedType.isPrimitive())
				return false;
			String sType = resolvedType.asPrimitive().describe();
			if (sType.equals("boolean"))
				return false;
			sType = resolvedType.asPrimitive().describe();
			if (sType.equals("boolean"))
				return false;
			return true;
		} catch (Throwable e) {
			return false;
		}
	}
	
	@Override
	protected Operator[] getApplicableOperators() {
		return applicableOperators;
	}

	@Override
	public String getDescription() {
		return "Relational operator replacement (== by !, <, etc.)";
	}
}
