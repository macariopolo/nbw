package edu.uclm.esi.newBacterioWeb.operators.untch;

import java.util.List;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.expr.MethodCallExpr;
import com.github.javaparser.resolution.types.ResolvedPrimitiveType;
import com.github.javaparser.resolution.types.ResolvedType;

import edu.uclm.esi.newBacterioWeb.operators.Filters;

public class RSPE extends UntchMutationOperator{
	
	
	@Override
	protected Node nextMutableNode() {
		List<MethodCallExpr> calls = this.compilationUnit.getCompilationUnit().findAll(MethodCallExpr.class);
	    List<Node> mutableExpressions= Filters.filterByNameCallAndMethodNames(this.compilationUnit, calls, "android.content.SharedPreferences.Editor", "putString", "putBoolean", "putFloat", "putInt", "putLong");
	    for (int i=0; i<mutableExpressions.size(); i++) 
	    	return mutableExpressions.get(i);
		return null;
	}


	@Override 
	protected Expression generateMutants(Node node, int[] counter) {
		MethodCallExpr call = (MethodCallExpr) node;
		ResolvedType argumentType = null;
		List<Node> elements = call.getChildNodes();
		String md="";
		String type="";
		if(elements.size()>3)
			md ="\t\tMutantDriver.RSPE" + "("+ elements.get(0);
		else
			md ="\t\tMutantDriver.RSPE" + "("+ "this";
		md = md + ", " + call.getArgument(0) +  ", " + call.getArgument(1);
		
		Expression secondArg = call.getArgument(1);
		if(secondArg instanceof MethodCallExpr){
			if (((MethodCallExpr) secondArg).getScope().get().toString().equals("MutantDriver") && secondArg.getChildNodes().get(1).getTokenRange().get().toString().equals("IMCA"))
				type="Primitive";
		} else { 
			argumentType=call.getArgument(1).calculateResolvedType();
			type = getType(argumentType);
		}
		if (!type.equals("Unsolved")){ 
			if(type.equals("java.lang.String")){
			for (int i=0; i<4; i++)
				md = md + ", " + ++counter[0];
			}
			else
				md = md + ", " + ++counter[0];
			md = md + ")";
			Expression replaceableExpr = StaticJavaParser.parseExpression(md);
			return replaceableExpr;
		}
		return null;
	}

	@Override
	public String getFamilyName() {
		return "Data SharedPreferences";
	}

	@Override
	public String getDescription() {
		return "Replace SharedPreferences Editor";
	}
	
	
}
