package edu.uclm.esi.newBacterioWeb.operators.classic;

import java.util.List;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.expr.ObjectCreationExpr;

import edu.uclm.esi.newBacterioWeb.model.ProjectFile;
import edu.uclm.esi.newBacterioWeb.operators.Filters;

public class DAID_ITR extends ClassicMutationOperator {

	public List<Node> loadMutableNodes() {
		List<ObjectCreationExpr> expressions = this.compilationUnit.getCompilationUnit().findAll(ObjectCreationExpr.class);
		return Filters.filterByClassConstructorNumberOfArguments(this.compilationUnit, expressions, "android.content.Intent", 2);
	}
	
	@Override
	public String getFamilyName() {
		return "Android intents";
	}

	@Override
	protected void generateMutants(ProjectFile originalFile, List<Node> mutableNodes, int[] projectCounter) {
		ObjectCreationExpr expr;
		for (int i=0; i<mutableNodes.size(); i++) {
			expr = (ObjectCreationExpr) mutableNodes.get(i);
			Expression arg1 = expr.getArgument(1);
			Expression newArg1 = StaticJavaParser.parseExpression("android.app.Activity.class");
			expr.setArgument(1, newArg1);
			projectCounter[0]++;
			saveClassicMutant(originalFile, projectCounter[0]);
			expr.setArgument(1, arg1);
		}
	}
	
	@Override
	public String getDescription() {
		return "Different activity intent definition";
	}
}
