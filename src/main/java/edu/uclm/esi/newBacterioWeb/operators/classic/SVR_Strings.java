package edu.uclm.esi.newBacterioWeb.operators.classic;

import java.util.ArrayList;
import java.util.List;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.expr.MethodCallExpr;
import com.github.javaparser.resolution.types.ResolvedType;

import edu.uclm.esi.newBacterioWeb.model.ProjectFile;

public class SVR_Strings extends ClassicMutationOperator {
	

	public List<Node> loadMutableNodes() {
		List<MethodCallExpr> calls = this.compilationUnit.getCompilationUnit().findAll(MethodCallExpr.class);
		List<Node> selectedCalls = new ArrayList<>();
		MethodCallExpr call;
		for (int i=0; i<calls.size(); i++) {
			call = calls.get(i);
			for (int j=0; j<call.getArguments().size(); j++) {
				Expression argument = call.getArgument(j);
				try {
					ResolvedType argumentType= argument.calculateResolvedType();
					if (argumentType.describe().equals("java.lang.String")) {
						selectedCalls.add(call);
						break;
					}
				} catch(Exception e) {}
			}
		}
		return selectedCalls;
	}
	
	@Override
	protected void generateMutants(ProjectFile originalFile, List<Node> mutableNodes, int[] projectCounter) {
		for (int i=0; i<mutableNodes.size(); i++) {
			MethodCallExpr call = (MethodCallExpr) mutableNodes.get(i);
			for (int j=0; j<call.getArguments().size(); j++) {
				Expression argument = call.getArgument(j);
				try {
					ResolvedType argumentType = argument.calculateResolvedType();
					if (argumentType.describe().equals("java.lang.String")) {
						Expression newExpression = StaticJavaParser.parseExpression("\"\"");
						call.setArgument(j, newExpression);
						projectCounter[0]++;
						saveClassicMutant(originalFile, projectCounter[0]);
						call.setArgument(j, argument);
						break;
					}
				} catch(Exception e) {}
			}
		}
	}

	@Override
	public String getFamilyName() {
		return "Traditional";
	}

	@Override
	public String getDescription() {
		return "Replace a String par by \"\"";
	}

}
