package edu.uclm.esi.newBacterioWeb.operators.classic;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.expr.MethodCallExpr;

import edu.uclm.esi.newBacterioWeb.model.ProjectFile;


public class RAQ extends ClassicMutationOperator{
	
	private String changes[]={"moveToFirst", "moveToLast","moveToNext","moveToPrevious"};
	
	@Override
	public List<Node> loadMutableNodes() {
		List<MethodCallExpr>calls=this.compilationUnit.getCompilationUnit().findAll(MethodCallExpr.class);
		ArrayList<Node> mutableExpressions=filter(calls);
		return mutableExpressions;
	}
  
	private ArrayList<Node> filter(List<MethodCallExpr> calls) {
		ArrayList<Node> result=new ArrayList<>();
		for (int i=0; i<calls.size(); i++) {
			MethodCallExpr call = calls.get(i);
			for(int k=0;k<changes.length;k++){
				if (call.getNameAsString().equals(changes[k])){
					String owner=getOwner(call); 
					if( owner.contains("android.database.Cursor"))
							result.add(call);
				}
			}
		}
		return result;
	}
	
	private ArrayList<String> getChangesList(Node expression){
		ArrayList<String> result=getMutants(changes, expression);
			if (!result.isEmpty())
				return result;
		return null;
	}
	
	private ArrayList<String> getMutants(String[] changes, Node expression) {
		 ArrayList<String> result=new ArrayList<>();
		int posEncontrado=-1;
		for (int i=0; i<changes.length; i++) {
			if (expression.toString().contains(changes[i])) {
				posEncontrado=i;
				break;
			}
		}
		if (posEncontrado!=-1) {
			for (int i=0; i<changes.length; i++)
				if (i!=posEncontrado) 
					result.add(changes[i]);
		}
		  return result;
		}

	@Override
	protected void generateMutants(ProjectFile originalFile, List<Node> mutableNodes, int[] projectCounter) {
		Iterator<Node> itMutableExpressions = mutableNodes.iterator();
		while (itMutableExpressions.hasNext()) {
			MethodCallExpr call = (MethodCallExpr) itMutableExpressions.next();
			String callName=call.getNameAsString();
			ArrayList<String> changes= getChangesList(call);
			if(!changes.isEmpty()){
				for(int i=0;i<changes.size();i++){
					call.setName(changes.get(i));
					projectCounter[0]++;
					saveClassicMutant(originalFile, projectCounter[0]);
					call.setName(callName);
				}
			}
		}
		
	}

	@Override
	public String getDescription() {
		return "Replace read-write Access to a database Query ";
	}

	@Override
	public String getFamilyName() {
		return "Database";
	}

}
