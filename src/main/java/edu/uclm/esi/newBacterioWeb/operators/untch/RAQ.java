package edu.uclm.esi.newBacterioWeb.operators.untch;

import java.util.ArrayList;
import java.util.List;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.expr.MethodCallExpr;


public class RAQ extends UntchMutationOperator{
	
	private String changes[]={"moveToFirst", "moveToLast","moveToNext","moveToPrevious"};

	@Override
	protected Node nextMutableNode() {
		List<MethodCallExpr>calls=this.compilationUnit.getCompilationUnit().findAll(MethodCallExpr.class);
		ArrayList<Node> mutableExpressions=filter(calls);
		for (int i = 0; i < mutableExpressions.size(); i++) {
			return mutableExpressions.get(i);
		}
		return null;
	}
	
	private ArrayList<Node> filter(List<MethodCallExpr> calls) {
		ArrayList<Node> result=new ArrayList<>();
		for (int i=0; i<calls.size(); i++) {
			MethodCallExpr call = calls.get(i);
			for(int k=0;k<changes.length;k++){
				if (call.getNameAsString().equals(changes[k])){
					String owner=getOwner(call); 
					if( owner.contains("android.database.Cursor"))
							result.add(call);
				}
			}
		}
		return result;
	}
	
	private ArrayList<String> getChangesList(Node expression){
		ArrayList<String> result=getMutants(changes, expression);
			if (!result.isEmpty())
				return result;
		return null;
	}
	
	private ArrayList<String> getMutants(String[] changes, Node expression) {
		 ArrayList<String> result=new ArrayList<>();
		int posEncontrado=-1;
		for (int i=0; i<changes.length; i++) {
			if (expression.toString().contains(changes[i])) {
				posEncontrado=i;
				break;
			}
		}
		if (posEncontrado!=-1) {
			for (int i=0; i<changes.length; i++)
				if (i!=posEncontrado) 
					result.add(changes[i]);
		}
		  return result;
		}

	@Override
	protected Expression generateMutants(Node node, int[] projectCounter) {
		MethodCallExpr call = (MethodCallExpr) node;
		ArrayList<String> changes= getChangesList(call); 
		if(!changes.isEmpty()){
			String md = "MutantDriver.RAQ("+ call.getChildNodes().get(0)+ ", " + "\"" + call.getNameAsExpression().toString() + "\", ";
			for (int i=0; i<changes.size(); i++) {
				String action = changes.get(i);
				md = md + "\""+action+"\"";
				if (i<changes.size()-1)
					md = md + ", ";
			}
			for (int i=0; i<3; i++)
				md = md + ", " + ++projectCounter[0];
			md = md + ")";			
			Expression replaceableExpr = StaticJavaParser.parseExpression(md);
			return replaceableExpr;
		}
		return null;
	}

	@Override
	public String getDescription() {
		return "Replace read-write Access to a database Query ";
	}

	@Override
	public String getFamilyName() {
		return "SQlite Database";
	}

}
