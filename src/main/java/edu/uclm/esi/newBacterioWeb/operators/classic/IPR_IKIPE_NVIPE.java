package edu.uclm.esi.newBacterioWeb.operators.classic;

import java.util.Iterator;
import java.util.List;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.expr.MethodCallExpr;
import com.github.javaparser.ast.expr.StringLiteralExpr;
import com.github.javaparser.resolution.types.ResolvedPrimitiveType;
import com.github.javaparser.resolution.types.ResolvedType;

import edu.uclm.esi.newBacterioWeb.model.ProjectFile;
import edu.uclm.esi.newBacterioWeb.operators.Filters;

public class IPR_IKIPE_NVIPE extends ClassicMutationOperator {

	@Override
	public List<Node> loadMutableNodes() {
		List<MethodCallExpr> calls = this.compilationUnit.getCompilationUnit().findAll(MethodCallExpr.class);
	    return Filters.filterByClassMethodNumberOfArguments(this.compilationUnit, calls, "android.content.Intent", "putExtra", 2);
	}

	@Override
	protected void generateMutants(ProjectFile originalFile, List<Node> mutableNodes, int[] projectCounter) {
		
		Iterator<Node> itMutableExpressions = mutableNodes.iterator();
		String newArgValue = "new Parcelable[0]";
		Expression newArgNVIPE = StaticJavaParser.parseExpression(newArgValue);
		while (itMutableExpressions.hasNext()) { 
			MethodCallExpr call = (MethodCallExpr) itMutableExpressions.next();
			Expression argValue=call.getArgument(1);
			Expression argName= call.getArgument(0);
			String newArgName = "_" + argName;
			StringLiteralExpr newArgIKIPE = new StringLiteralExpr(newArgName);
			
			ResolvedType argumentType=argValue.calculateResolvedType();
			String type = getType(argumentType);
			
			//Offut's change
			if (type.equals("Unsolved") || (argumentType.isReferenceType() && !type.equals("java.lang.String")))
				continue;
			if(argumentType.isArray() && !type.contains("Parcelable"))
				ipreArreglo(originalFile, projectCounter, call, argValue, type);
			else if(!argumentType.isArray() && type.equals("java.lang.String"))
				ipreString(originalFile, projectCounter, call, argValue);
			if (argumentType instanceof ResolvedPrimitiveType)
				iprePrimitive(originalFile, projectCounter, call, argValue, type);
			
			// IKIPE change
			call.getArguments().set(0, newArgIKIPE);
			projectCounter[0]++;
			saveClassicMutant(originalFile, projectCounter[0]);
			call.getArguments().set(0, argName);
			
			//NVIPE change
			call.getArguments().set(1, newArgNVIPE);
			projectCounter[0]++;
			saveClassicMutant(originalFile, projectCounter[0]);
			call.getArguments().set(1, argValue);
		}	
	}

	private void ipreArreglo(ProjectFile originalFile, int[] projectCounter, MethodCallExpr call, Expression argValue, String type){
		Expression replace;
		String typeName= Filters.getPrimitiveTypeName(type);
		String aux= "("+ typeName +"[])" + "null";
		replace = StaticJavaParser.parseExpression(aux);
		//change 
		call.getArguments().set(1, replace);
		projectCounter[0]++;
		saveClassicMutant(originalFile, projectCounter[0]);
		call.getArguments().set(1, argValue);
	}

	private void ipreString(ProjectFile originalFile, int[] projectCounter, MethodCallExpr call, Expression argValue) {
		String empty="\"\"";
		Expression replaceCadVacia=StaticJavaParser.parseExpression(empty);
		String aux= "(String) null";
		Expression replace = StaticJavaParser.parseExpression(aux);
		for(int i=0;i<2;i++){
			if(i==0)
				call.getArguments().set(1, replace);
			if(i==1)
				call.getArguments().set(1, replaceCadVacia);
			projectCounter[0]++;
			saveClassicMutant(originalFile, projectCounter[0]);
			call.getArguments().set(1, argValue);
		}
	}

	private void iprePrimitive(ProjectFile originalFile, int[] projectCounter, MethodCallExpr call, Expression argValue,
			String type) {
		String zero = "0";
		Expression replaceZero = StaticJavaParser.parseExpression(zero);
		if(type.equals("boolean")){ 
			Expression replace = StaticJavaParser.parseExpression("!(" + argValue.toString() + ")");
			call.getArguments().set(1, replace);
		}
		else call.getArguments().set(1, replaceZero);
		projectCounter[0]++;
		saveClassicMutant(originalFile, projectCounter[0]);
		call.getArguments().set(1, argValue);
	}

	
	@Override
	public String getDescription() {
		return "putExtra method replacement";
	}

	@Override
	public String getFamilyName() {
		return "Android intents";
	}

}
