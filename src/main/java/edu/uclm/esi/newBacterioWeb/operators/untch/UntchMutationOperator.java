package edu.uclm.esi.newBacterioWeb.operators.untch;

import org.springframework.web.socket.WebSocketSession;

import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.expr.Expression;

import edu.uclm.esi.newBacterioWeb.model.ProjectFile;
import edu.uclm.esi.newBacterioWeb.model.UntchMutantOperator;
import edu.uclm.esi.newBacterioWeb.operators.MutationOperator;
import edu.uclm.esi.newBacterioWeb.operators.WrapperCompilationUnit;
import edu.uclm.esi.newBacterioWeb.web.controllers.Manager;

public abstract class UntchMutationOperator extends MutationOperator {
	
	public void generateMutants(WebSocketSession session, ProjectFile originalFile, WrapperCompilationUnit wrapper, int[] projectCounter, String projectId, String originalFileId, String mutantId) {
		this.session = session;
		this.compilationUnit = wrapper;
		Node node = this.nextMutableNode();
		Node parent;
		Node newNode;
		while (node!=null) { // replacement mutants only
			int start = projectCounter[0];
			newNode = generateMutants(node, projectCounter);
			int end = projectCounter[0];
			parent = node.getParentNode().get();
			parent.replace(node, newNode);
			this.compilationUnit.setCode(wrapper.getCompilationUnit().toString());
			for (int i=start+1; i<=end; i++) {
				UntchMutantOperator umo = new UntchMutantOperator(projectId, originalFileId, mutantId, i, this.getClass().getSimpleName());
				Manager.get().getUntchMutantOperatorDAO().save(umo);
			}
			node = this.nextMutableNode();//lo malo de esto que obliga a filtrar en cada iteracion
		}
	}

	protected abstract Expression generateMutants(Node node, int[] projectCounter);

	protected abstract Node nextMutableNode();
	
	protected String getMutationType(){
		return "Replacement";
	}

}
