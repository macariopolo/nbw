package edu.uclm.esi.newBacterioWeb.operators.untch;

import java.util.List;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.expr.MethodCallExpr;
import com.github.javaparser.resolution.types.ResolvedType;

public class IMCA extends UntchMutationOperator {
	

	@Override
	protected Node nextMutableNode() {
		List<MethodCallExpr> calls = this.compilationUnit.getCompilationUnit().findAll(MethodCallExpr.class);
		MethodCallExpr call;
		for (int i=0; i<calls.size(); i++) {
			call = calls.get(i);
			if(call.toString().contains("MutantDriver"))
				continue;
			for (int j=0; j<call.getArguments().size(); j++) {
				Expression argument = call.getArgument(j);
				try {
					ResolvedType argumentType = argument.calculateResolvedType();
					String desc = argumentType.describe();
					if (desc.equals("int") || desc.equals("java.lang.Integer") || desc.equals("long") || desc.equals("java.lang.Long") ||
							desc.equals("float") || desc.equals("java.lang.Flaot") || desc.equals("double") || desc.equals("java.lang.Double") ||
							desc.equals("boolean") || desc.equals("java.lang.Boolean")) {
						return call;
					}
				} catch(Exception e) {}
			}
		}
		return null;
	}
	
	@Override
	protected Expression generateMutants(Node node, int[] projectCounter) {
		MethodCallExpr call = (MethodCallExpr) node;
		for (int j=0; j<call.getArguments().size(); j++) {
			Expression argument = call.getArgument(j);
				ResolvedType argumentType = argument.calculateResolvedType();
				if (argumentType.isPrimitive()) {
					String md = "MutantDriver.IMCA(" + argument.toString() + ", " + ++projectCounter[0] + ")";
					Expression newExpression = StaticJavaParser.parseExpression(md);
					call.setArgument(j, newExpression);
					return call;
				}
		}
		return null;
}

	@Override
	public String getFamilyName() {
		return "Traditional";
	}

	@Override
	public String getDescription() {
		return "Invalid Method Call Argument";
	}

}
