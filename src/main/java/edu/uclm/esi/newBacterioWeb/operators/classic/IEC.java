package edu.uclm.esi.newBacterioWeb.operators.classic;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.body.Parameter;
import com.github.javaparser.ast.stmt.BlockStmt;
import com.github.javaparser.ast.stmt.Statement;

import edu.uclm.esi.newBacterioWeb.model.ProjectFile;
import edu.uclm.esi.newBacterioWeb.operators.Filters;

public class IEC extends ClassicMutationOperator {
	
	@Override
	public List<Node> loadMutableNodes() {
		List<MethodDeclaration> methods = this.compilationUnit.getCompilationUnit().findAll(MethodDeclaration.class);
		return Filters.filterByAParameterType(this.compilationUnit, methods, "MotionEvent");
	}

	@Override
	protected void generateMutants(ProjectFile originalFile, List<Node> mutableNodes, int[] projectCounter) {
		Iterator<Node> itMutableExpressions = mutableNodes.iterator();
		while (itMutableExpressions.hasNext()) {
			MethodDeclaration method = (MethodDeclaration) itMutableExpressions.next();
			NodeList<Parameter> parameters = method.getParameters();
			for (int j=0; j<parameters.size(); j++) {
				Parameter parameter=parameters.get(j);
				if (parameter.getTypeAsString().equals("MotionEvent")) {
					String name=parameter.getNameAsString();
					for(int i=0; i<3;i++){
						String md="";
						if(i==0)
							md="\t\t" + name + ".setLocation(" + name + ".getY(), " + name + ".getX());";
						else if(i==1)
							md="\t\t" + name + ".setLocation(" + name + ".getX(), " + 34 + ");";
						else if(i==2)
							md="\t\t" + name + ".setLocation(" + 34 + ", " + name + ".getY());";
						String[] codes= {md};
						Statement[] statements=new Statement[codes.length];
						for (int k=0; k<codes.length; k++)
							statements[k]=StaticJavaParser.parseStatement(codes[k]);
						Optional<BlockStmt> opFirstBlock=method.getBody();
						BlockStmt blockStmt;
						if (opFirstBlock.isPresent()) {
							blockStmt=opFirstBlock.get();
						} else {
							blockStmt=new BlockStmt();
						}
						for (int k=statements.length-1; k>=0; k--)
							blockStmt.addStatement(0, statements[k]);
						projectCounter[0]++;
						saveClassicMutant(originalFile, projectCounter[0]);
						for (int k=0; k<statements.length; k++)
							blockStmt.remove(statements[k]);
					}
				}
			}
		}
	}
	
	@Override
	public String getFamilyName() {
		return "Event handler";
	}

	@Override
	public String getDescription() {
		return "Interchanges the Event's Coordinates";
	}
}
