package edu.uclm.esi.newBacterioWeb.operators.untch;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.body.Parameter;
import com.github.javaparser.ast.stmt.BlockStmt;
import com.github.javaparser.ast.stmt.Statement;

import edu.uclm.esi.newBacterioWeb.model.ProjectFile;
import edu.uclm.esi.newBacterioWeb.operators.Filters;

public class IEC extends UntchMutationOperator_Insertion {
	
	@Override
	protected List<Node> loadMutableNodes() {
		List<MethodDeclaration> methods = this.compilationUnit.getCompilationUnit().findAll(MethodDeclaration.class);
		return Filters.filterByAParameterType(this.compilationUnit, methods, "MotionEvent");
	}

	@Override
	protected void generateMutants(ProjectFile originalFile, List<Node> mutableNodes, int[] projectCounter) {
		Iterator<Node> itMutableExpressions = mutableNodes.iterator();
		while (itMutableExpressions.hasNext()) {
			MethodDeclaration method = (MethodDeclaration) itMutableExpressions.next();
			NodeList<Parameter> parameters = method.getParameters();
			String md;
			for (int j=0; j<parameters.size(); j++) {
				Parameter parameter=parameters.get(j);
				if (parameter.getTypeAsString().equals("MotionEvent")) {
					String name=parameter.getNameAsString();
					md="\t\tMutantDriver.IEC(" + name ;
					for (int i=0; i<3; i++)
						md = md + ", " + ++projectCounter[0];
					md = md + ");";
					String[] codes= {md};
					Statement[] statements=new Statement[codes.length];
					for (int k=0; k<codes.length; k++)
						statements[k]=StaticJavaParser.parseStatement(codes[k]);
					Optional<BlockStmt> opFirstBlock=method.getBody();
					BlockStmt blockStmt;
					if (opFirstBlock.isPresent()) {
						blockStmt=opFirstBlock.get();
					} else {
						blockStmt=new BlockStmt();
					}
					for (int k=statements.length-1; k>=0; k--)
						blockStmt.addStatement(0, statements[k]);
				}
			}
		}
	}

	@Override
	public String getFamilyName() {
		return "Event handler";
	}

	@Override
	public String getDescription() {
		return "Interchanges the Event's Coordinates";
	}

}
