package edu.uclm.esi.newBacterioWeb.operators.untch;

import com.github.javaparser.ast.expr.BinaryExpr;
import com.github.javaparser.ast.expr.BinaryExpr.Operator;

public class LOR extends BinaryMutationOperator {
	private static Operator[] applicableOperators = { Operator.OR, Operator.AND }; //, Operator.BINARY_OR, Operator.BINARY_AND, Operator.XOR };

	@Override
	protected boolean typesAreCompatible(BinaryExpr originalExpr) {
		return true;
	}

	@Override
	protected Operator[] getApplicableOperators() {
		return applicableOperators;
	}
	
	@Override
	public String getDescription() {
		return "Logical operator replacement (|| by &&, && by ||)";
	}
}
