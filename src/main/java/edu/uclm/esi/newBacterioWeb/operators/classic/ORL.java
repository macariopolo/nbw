package edu.uclm.esi.newBacterioWeb.operators.classic;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.stmt.BlockStmt;
import com.github.javaparser.ast.stmt.Statement;

import edu.uclm.esi.newBacterioWeb.model.ProjectFile;
import edu.uclm.esi.newBacterioWeb.operators.Filters;

public class ORL extends ClassicMutationOperator {

	@Override
	public List<Node> loadMutableNodes() {
		List<MethodDeclaration> onCreateMethod = this.compilationUnit.getCompilationUnit().findAll(MethodDeclaration.class);
		ArrayList<Node> mutableExpressions=Filters.filterByClassNamesMethod(this.compilationUnit, onCreateMethod, "onCreate", "Activity", "WebViewActivity", "ConnectingActivity", "ListActivity", "AppCompatActivity", "FragmentActivity", "SupportActivity");
		return mutableExpressions;
	}
	
	@Override
	protected void generateMutants(ProjectFile originalFile, List<Node> mutableNodes, int[] projectCounter) {
		Iterator<Node> itMutableExpressions = mutableNodes.iterator();
		while (itMutableExpressions.hasNext()) {
			MethodDeclaration method = (MethodDeclaration) itMutableExpressions.next();
			String md="\t\t" + "setRequestedOrientation(" + "android.content.pm.ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE" + ");";
			String[] codes= {md};
			Statement[] statements=new Statement[codes.length];
			for (int k=0; k<codes.length; k++)
				statements[k]=StaticJavaParser.parseStatement(codes[k]);
			Optional<BlockStmt> opFirstBlock=method.getBody();
			BlockStmt blockStmt;
			if (opFirstBlock.isPresent()) {
				blockStmt=opFirstBlock.get();
			} else {
				blockStmt=new BlockStmt();
			}
			for (int k=statements.length-1; k>=0; k--)
				blockStmt.addStatement(0, statements[k]);
			projectCounter[0]++;
			saveClassicMutant(originalFile, projectCounter[0]);
			for (int k=0; k<statements.length; k++)
				blockStmt.remove(statements[k]);
		}
	}

	@Override
	public String getDescription() {
		return "Orientation locked";
	}

	@Override
	public String getFamilyName() {
		return "Screen";
	}
}
