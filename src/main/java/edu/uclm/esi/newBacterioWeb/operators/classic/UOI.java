package edu.uclm.esi.newBacterioWeb.operators.classic;

import java.util.ArrayList;
import java.util.List;

import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.body.CallableDeclaration;
import com.github.javaparser.ast.expr.AssignExpr;
import com.github.javaparser.ast.expr.BinaryExpr;
import com.github.javaparser.ast.expr.NameExpr;
import com.github.javaparser.ast.expr.UnaryExpr;
import com.github.javaparser.ast.expr.UnaryExpr.Operator;
import com.github.javaparser.ast.stmt.SwitchEntry;

import edu.uclm.esi.newBacterioWeb.model.ProjectFile;

public class UOI extends ClassicMutationOperator {
	private static Operator[] applicableOperators = { 
			Operator.PLUS, Operator.MINUS, 
			Operator.PREFIX_INCREMENT, Operator.PREFIX_DECREMENT, 
			Operator.POSTFIX_INCREMENT, Operator.POSTFIX_DECREMENT };

	public List<Node> loadMutableNodes() {
		List<NameExpr> nameExpressions = this.compilationUnit.getCompilationUnit().findAll(NameExpr.class);
		return filter(nameExpressions);
	}
	
	private ArrayList<Node> filter(List<NameExpr> nameExpressions) {
		ArrayList<Node> mutableExpressions=new ArrayList<>();
		Node parent;
		NameExpr expr;
		for (int i=0; i<nameExpressions.size(); i++) {
			expr = nameExpressions.get(i);
			CallableDeclaration<?> parentMethod = findParentMethod(expr);
			if (parentMethod==null)
				continue;
			parent=expr.getParentNode().get();
			if (parent instanceof UnaryExpr || parent instanceof SwitchEntry)
				continue;
			if (parent instanceof AssignExpr) {
				AssignExpr ae = (AssignExpr) parent;
				if (expr != ae.getTarget() && isANumber(expr)) 
					mutableExpressions.add(expr);
			} else if (parent instanceof BinaryExpr && isANumber(expr)) {
				mutableExpressions.add(expr);
			} else if (parent instanceof CallableDeclaration && isANumber(expr)) {
				mutableExpressions.add(expr);
			}
		}
		return mutableExpressions;
	}
	
	@Override
	public String getFamilyName() {
		return "Traditional";
	}
	
	private Operator[] getOtherOperators(Operator op) {
		Operator[] result = new Operator[applicableOperators.length-1];
		int cont=0;
		for (int i=0; i<applicableOperators.length; i++) {
			if (applicableOperators[i]==Operator.LOGICAL_COMPLEMENT)
				continue;
			if ((op==null && applicableOperators[i]!=Operator.PLUS) || (op!=null && applicableOperators[i]!=op))
				result[cont++]=applicableOperators[i];
		}
		return result;
	}

	@Override
	protected void generateMutants(ProjectFile originalFile, List<Node> mutableNodes, int[] projectCounter) {
		NameExpr expr;
		for (int i=0; i<mutableNodes.size(); i++) {
			expr = (NameExpr) mutableNodes.get(i);
			Node parentNode = expr.getParentNode().get();
			Operator[] otherOperators = this.getOtherOperators(null);
			for (int j=0; j<otherOperators.length; j++) {
				Operator otherOperator = otherOperators[j];
				UnaryExpr ue = new UnaryExpr(expr, otherOperator);
				parentNode.replace(expr, ue);
				projectCounter[0]++;
				saveClassicMutant(originalFile, projectCounter[0]);
				parentNode.replace(ue, expr);
			}
		}
	}

	@Override
	public String getDescription() {
		return "Unary operator insertion";
	}
}
