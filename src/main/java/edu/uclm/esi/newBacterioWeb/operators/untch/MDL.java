package edu.uclm.esi.newBacterioWeb.operators.untch;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.body.Parameter;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.stmt.BlockStmt;
import com.github.javaparser.ast.stmt.IfStmt;
import com.github.javaparser.ast.stmt.Statement;

import edu.uclm.esi.newBacterioWeb.model.ProjectFile;
import edu.uclm.esi.newBacterioWeb.operators.Filters;

public class MDL extends UntchMutationOperator_Insertion {
	
	@Override
	protected List<Node> loadMutableNodes() {
		List<MethodDeclaration> declarations = this.compilationUnit.getCompilationUnit().findAll(MethodDeclaration.class);
		declarations = Filters.filterByMethodNames(this.compilationUnit, declarations, "onResume",  "onStop", "onStart", "onRestart", "onPause", "onDestroy", "onCreate");
		ArrayList<Node> selectedDeclarations = new ArrayList<>();
		MethodDeclaration declaration;
		for (int i=0; i<declarations.size(); i++) {
			declaration = declarations.get(i);
			if (declaration.getParameters().isNonEmpty()) {
				Parameter par0 = declaration.getParameter(0);
				if (!par0.getType().toString().contains("SQLiteDatabase"))
					selectedDeclarations.add(declaration);
			}
		}
		return selectedDeclarations;
	}

	@Override
	protected void generateMutants(ProjectFile originalFile, List<Node> mutableNodes, int[] projectCounter) {
		Iterator<Node> itMutableExpressions = mutableNodes.iterator();
		while (itMutableExpressions.hasNext()) {
			MethodDeclaration method = (MethodDeclaration) itMutableExpressions.next();
			Optional<BlockStmt> optBody = method.getBody();
			if (optBody.isPresent()) {
				BlockStmt body = optBody.get();
				IfStmt newCode = new IfStmt();
				Expression condition = StaticJavaParser.parseExpression("MutantDriver.MDL(" + ++projectCounter[0] + ")");
				String trueBranchCode = "super." + method.getNameAsString() + "(";
				NodeList<Parameter> parameters = method.getParameters();
				for(int i=0; i<parameters.size(); i++) 
					trueBranchCode+=parameters.get(i).getNameAsString() + ", ";
				if (trueBranchCode.endsWith(", ")) {
					trueBranchCode=trueBranchCode.substring(0, trueBranchCode.length()-2);
				}
				trueBranchCode +=");\n";
				Statement trueBranch = StaticJavaParser.parseStatement(trueBranchCode);
				newCode.setCondition(condition);
				newCode.setThenStmt(trueBranch);
				newCode.setElseStmt(body);
				BlockStmt newBody = new BlockStmt();
				newBody.addStatement(newCode);
				method.setBody(newBody);
			}
		}
	}

	@Override
	public String getDescription() {
		return "Lifecycle method deletion";
	}

	@Override
	public String getFamilyName() {
		return "Acivity lifeclycle";
	}

}
