package edu.uclm.esi.newBacterioWeb.operators.classic;

import java.util.Iterator;
import java.util.List;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.expr.MethodCallExpr;

import edu.uclm.esi.newBacterioWeb.model.ProjectFile;
import edu.uclm.esi.newBacterioWeb.operators.Filters;

public class ISQLQ extends ClassicMutationOperator{

	public List<Node> loadMutableNodes() {
		List<MethodCallExpr> calls = this.compilationUnit.getCompilationUnit().findAll(MethodCallExpr.class);
		return Filters.filterByClassMethodNumberOfArguments(this.compilationUnit, calls, "android.database.sqlite.SQLiteDatabase", "rawQuery", 2);
	}

	@Override
	protected void generateMutants(ProjectFile originalFile, List<Node> mutableNodes, int[] projectCounter) {
		Iterator<Node> itMutableExpressions = mutableNodes.iterator();
		Expression nuller=StaticJavaParser.parseExpression("null");
		while (itMutableExpressions.hasNext()) {
			MethodCallExpr call = (MethodCallExpr) itMutableExpressions.next();
			Expression query = call.getArguments().get(0);
			call.getArguments().set(0, nuller);
			projectCounter[0]++;
			saveClassicMutant(originalFile, projectCounter[0]);
			call.getArguments().set(0, query);
		}
	}

	@Override
	public String getDescription() {
		return "Invalid SQ LQuery";
	}

	@Override
	public String getFamilyName() {
		return "Database";
	}

}
