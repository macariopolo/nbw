package edu.uclm.esi.newBacterioWeb.operators.classic;

import java.util.List;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.expr.MethodCallExpr;

import edu.uclm.esi.newBacterioWeb.model.ProjectFile;
import edu.uclm.esi.newBacterioWeb.operators.Filters;

public class NVIPE extends ClassicMutationOperator {

	public List<Node> loadMutableNodes() {
		List<MethodCallExpr> expressions = this.compilationUnit.getCompilationUnit().findAll(MethodCallExpr.class);
		return Filters.filterByClassMethodNumberOfArguments(this.compilationUnit, expressions, "android.content.Intent", "putExtra", 2);
	}
	
	@Override
	public String getFamilyName() {
		return "Android intents";
	}

	@Override
	protected void generateMutants(ProjectFile originalFile, List<Node> mutableNodes, int[] projectCounter) {
		MethodCallExpr expr;
		for (int i=0; i<mutableNodes.size(); i++) {
			expr = (MethodCallExpr) mutableNodes.get(i);
			Expression arg = expr.getArgument(1);
			String newArgValue = "new Parcelable[0]";
			Expression newArg = StaticJavaParser.parseExpression(newArgValue);
			expr.setArgument(1, newArg);
			projectCounter[0]++;
			saveClassicMutant(originalFile, projectCounter[0]);
			expr.setArgument(1, arg);
		}
	}

	@Override
	public String getDescription() {
		return "Null value intent put extra";
	}
}
