package edu.uclm.esi.newBacterioWeb.operators.classic;

import java.util.ArrayList;
import java.util.List;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.body.Parameter;

import edu.uclm.esi.newBacterioWeb.model.ProjectFile;
import edu.uclm.esi.newBacterioWeb.operators.Filters;

public class MDL extends ClassicMutationOperator {
	
	@Override
	public List<Node> loadMutableNodes() {
		List<MethodDeclaration> declarations = this.compilationUnit.getCompilationUnit().findAll(MethodDeclaration.class);
		declarations = Filters.filterByMethodNames(this.compilationUnit, declarations, "onResume",  "onStop", "onStart", "onRestart", "onPause", "onDestroy", "onCreate");
		ArrayList<Node> selectedDeclarations = new ArrayList<>();
		MethodDeclaration declaration;
		for (int i=0; i<declarations.size(); i++) {
			declaration = declarations.get(i);
			if (declaration.getParameters().isNonEmpty()) {
				Parameter par0 = declaration.getParameter(0);
				if (!par0.getType().toString().contains("SQLiteDatabase"))
					selectedDeclarations.add(declaration);
			}
		}
		return selectedDeclarations;
	}

	@Override
	protected void generateMutants(ProjectFile originalFile, List<Node> mutableNodes, int[] projectCounter) {
		ClassOrInterfaceDeclaration[] cid = new ClassOrInterfaceDeclaration[1];
		for (int i=0; i<mutableNodes.size(); i++) {
			MethodDeclaration method = (MethodDeclaration) mutableNodes.get(i);
			CompilationUnit copy = this.compilationUnit.getCompilationUnit().clone();
			MethodDeclaration methodCopy = findMethod(method.toString(), copy, cid);
			if (methodCopy!=null) {
				cid[0].remove(methodCopy);
				projectCounter[0]++;
				saveClassicMutant(originalFile, projectCounter[0]);
			}			
		 }
	}

	private MethodDeclaration findMethod(String methodCode, CompilationUnit cu, ClassOrInterfaceDeclaration[] cid) {
		List<MethodDeclaration> methods = cu.findAll(MethodDeclaration.class);
		for (MethodDeclaration method : methods)
			if (method.toString().equals(methodCode)) {
				cid[0] = (ClassOrInterfaceDeclaration) method.getParentNode().get();
				return method;
			}
		return null;
	}

	@Override
	public String getDescription() {
		return "MDL: Lifecycle method deletion";
	}

	@Override
	public String getFamilyName() {
		return "Acivity lifeclycle";
	}

}
