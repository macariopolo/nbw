package edu.uclm.esi.newBacterioWeb.operators.classic;

import java.util.Iterator;
import java.util.List;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.expr.MethodCallExpr;
import com.github.javaparser.resolution.types.ResolvedPrimitiveType;
import com.github.javaparser.resolution.types.ResolvedType;

import edu.uclm.esi.newBacterioWeb.model.ProjectFile;
import edu.uclm.esi.newBacterioWeb.operators.Filters;


public class IPR extends ClassicMutationOperator {

	public List<Node> loadMutableNodes() {
		List<MethodCallExpr> expressions = this.compilationUnit.getCompilationUnit().findAll(MethodCallExpr.class);
		return Filters.filterByClassMethodNumberOfArguments(this.compilationUnit, expressions, "android.content.Intent", "putExtra", 2);
	}
	
	@Override
	protected void generateMutants(ProjectFile originalFile, List<Node> mutableNodes, int[] projectCounter) {
		Iterator<Node> itMutableExpressions = mutableNodes.iterator();
		String empty="\"\"";
		Expression replaceCadVacia=StaticJavaParser.parseExpression(empty);
		String zero = "0";
		Expression replaceZero = StaticJavaParser.parseExpression(zero);
		while (itMutableExpressions.hasNext()) { 
			MethodCallExpr call = (MethodCallExpr) itMutableExpressions.next();
			Expression argValue=call.getArgument(1);
			ResolvedType argumentType=argValue.calculateResolvedType();
			String type = getType(argumentType);
			//Evitamos de momento, Parcelable, Serializable, Bundle
			if (type.equals("Unsolved") || (argumentType.isReferenceType() && !type.equals("java.lang.String")))
				continue;
			if(argumentType.isArray() && !type.contains("Parcelable"))
				ipreArreglo(originalFile, projectCounter, call, argValue, type);
			else if(!argumentType.isArray() && type.equals("java.lang.String"))
				ipreString(originalFile, projectCounter, replaceCadVacia, call, argValue);
			if (argumentType instanceof ResolvedPrimitiveType)
				iprePrimitive(originalFile, projectCounter, replaceZero, call, argValue, type);
		}	
	}

	private void ipreArreglo(ProjectFile originalFile, int[] counter, MethodCallExpr call, Expression argValue, String type){
		Expression replace;
		String typeName= Filters.getPrimitiveTypeName(type);
		String aux= "("+ typeName +"[])" + "null";
		replace = StaticJavaParser.parseExpression(aux);
		//change 
		call.getArguments().set(1, replace);
		counter[0]++;
		saveClassicMutant(originalFile, counter[0]);
		call.getArguments().set(1, argValue);
	}

	private void ipreString(ProjectFile originalFile, int[] counter, Expression replaceCadVacia, MethodCallExpr call, Expression argValue) {
		String aux= "(String) null";
		Expression replace = StaticJavaParser.parseExpression(aux);
		for(int i=0;i<2;i++){
			if(i==0)
				call.getArguments().set(1, replace);
			if(i==1)
				call.getArguments().set(1, replaceCadVacia);
			counter[0]++;
			saveClassicMutant(originalFile, counter[0]);
			call.getArguments().set(1, argValue);
		}
	}

	private void iprePrimitive(ProjectFile originalFile, int[] counter, Expression replaceZero, MethodCallExpr call, Expression argValue,
			String type) {//puedo decir que no se mute el booleano
		if(type.equals("boolean")){ 
			Expression replace = StaticJavaParser.parseExpression("!(" + argValue.toString() + ")");
			call.getArguments().set(1, replace);
		}
		else call.getArguments().set(1, replaceZero);
		counter[0]++;
		saveClassicMutant(originalFile, counter[0]);
		call.getArguments().set(1, argValue);
	}

	

	@Override
	public String getFamilyName() {
		return "Android intents";
	}

	@Override
	public String getDescription() {
		return "Intent payload replacement";
	}

	
}
