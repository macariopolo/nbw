package edu.uclm.esi.newBacterioWeb.operators.classic;

import java.util.ArrayList;
import java.util.List;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.expr.MethodCallExpr;
import com.github.javaparser.resolution.types.ResolvedPrimitiveType;
import com.github.javaparser.resolution.types.ResolvedType;

import edu.uclm.esi.newBacterioWeb.model.ProjectFile;

public class IMCA extends ClassicMutationOperator {
	
	public List<Node> loadMutableNodes() {
		List<MethodCallExpr> calls = this.compilationUnit.getCompilationUnit().findAll(MethodCallExpr.class);
		ArrayList<Node> selectedCalls = new ArrayList<>();
		MethodCallExpr call;
		for (int i=0; i<calls.size(); i++) {
			call = calls.get(i);
			for (int j=0; j<call.getArguments().size(); j++) {
				Expression argument = call.getArgument(j);
				try {
					ResolvedType argumentType = argument.calculateResolvedType();
					String desc = argumentType.describe();
					if (desc.equals("int") || desc.equals("java.lang.Integer") || desc.equals("long") || desc.equals("java.lang.Long") ||
							desc.equals("float") || desc.equals("java.lang.Flaot") || desc.equals("double") || desc.equals("java.lang.Double") ||
							desc.equals("boolean") || desc.equals("java.lang.Boolean")) {
						selectedCalls.add(call);
						break;
					}
				} catch(Exception e) {}
			}
		}
		return selectedCalls;
	}
	
	@Override
	protected void generateMutants(ProjectFile originalFile, List<Node> mutableNodes, int[] projectCounter) {
		for (int i=0; i<mutableNodes.size(); i++) {
			MethodCallExpr call = (MethodCallExpr) mutableNodes.get(i);
			for (int j=0; j<call.getArguments().size(); j++) {
				Expression argument = call.getArgument(j);
				try {
					ResolvedType argumentType = argument.calculateResolvedType();
					if (argumentType.isPrimitive()) {
						ResolvedPrimitiveType primitiveType = argumentType.asPrimitive();
						Expression newExpression = null;
						if (primitiveType.isNumeric()) 
							newExpression = StaticJavaParser.parseExpression("-(" + argument.toString() + ")");
						else {
							newExpression = StaticJavaParser.parseExpression("!(" + argument.toString() + ")");
						}
						call.setArgument(j, newExpression);
						projectCounter[0]++;
						saveClassicMutant(originalFile, projectCounter[0]);
						call.setArgument(j, argument);
						break;
					}
				} catch(Exception e) {}
			}
		}
	}

	@Override
	public String getFamilyName() {
		return "Traditional";
	}

	@Override
	public String getDescription() {
		return "Invalid Method Call Argument";
	}

}
