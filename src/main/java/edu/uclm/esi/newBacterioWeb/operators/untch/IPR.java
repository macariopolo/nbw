package edu.uclm.esi.newBacterioWeb.operators.untch;

import java.util.List;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.expr.MethodCallExpr;
import com.github.javaparser.resolution.types.ResolvedType;

import edu.uclm.esi.newBacterioWeb.operators.Filters;

public class IPR extends UntchMutationOperator {
	
	@Override
	protected Node nextMutableNode() {
		List<MethodCallExpr> calls = this.compilationUnit.getCompilationUnit().findAll(MethodCallExpr.class);
	    List<Node> mutableExpressions= Filters.filterByClassMethodNumberOfArguments(this.compilationUnit, calls, "android.content.Intent", "putExtra", 2);
	    for (int i=0; i<mutableExpressions.size(); i++) {
	    	return mutableExpressions.get(i);
	    }
		return null;
	}
	
	@Override
	protected Expression generateMutants(Node node, int[] projectCounter) {
		MethodCallExpr call = (MethodCallExpr) node;
		List<Node> elements = call.getChildNodes();
		String md="";
		String type = null;
		Expression replaceableExpr= null;
		ResolvedType argumentType = null;
		Expression secondArg = call.getArgument(1);
		if(secondArg instanceof MethodCallExpr){
			if (((MethodCallExpr) secondArg).getScope().get().toString().equals("MutantDriver") && secondArg.getChildNodes().get(1).getTokenRange().get().toString().equals("IMCA"))
				type="Primitive";
		} else {
			argumentType=call.getArgument(1).calculateResolvedType();
			type = getType(argumentType);
		}
		if(elements.size()>3)
			md ="\t\tMutantDriver.IPR("+ elements.get(0);
		else
			md ="\t\tMutantDriver.IPR("+ "this";
		md = md + ", " + call.getArgument(0) +  ", " + call.getArgument(1);
		
		if(argumentType==null && type.equals("Primitive")){
			md = md + ", " + ++projectCounter[0] + ")";
		    replaceableExpr = StaticJavaParser.parseExpression(md);
		} 
		else if ((!type.equals("Unsolved") && !argumentType.isReferenceType()) || type.equals("java.lang.String")){
			if (!argumentType.isArray() && type.equals("java.lang.String")){
				for (int i=0; i<2; i++)
					md = md + ", " + ++projectCounter[0];
			} 
			else if (!type.contains("Parcelable"))
				md = md + ", " + ++projectCounter[0];
			md = md + ")";
			replaceableExpr = StaticJavaParser.parseExpression(md);
		}
			return replaceableExpr;
	}

	@Override
	public String getFamilyName() {
		return "Android intents";
	}

	@Override
	public String getDescription() {
		return "Intent payload replacement";
	}
	
}
