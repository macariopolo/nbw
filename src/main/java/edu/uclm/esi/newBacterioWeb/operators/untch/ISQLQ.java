package edu.uclm.esi.newBacterioWeb.operators.untch;

import java.util.ArrayList;
import java.util.List;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.expr.MethodCallExpr;

import edu.uclm.esi.newBacterioWeb.operators.Filters;

public class ISQLQ extends UntchMutationOperator {

	@Override
	protected Expression generateMutants(Node node, int[] projectCounter) {
		MethodCallExpr call = (MethodCallExpr) node;
		List<Node> elements = call.getChildNodes();
		String md="";
		if(elements.size()>3)
			md = "MutantDriver.ISQLQ(" + elements.get(0);
		else
			md ="\t\tMutantDriver.ISQLQ("+ "this";
		md = md + ", " + call.getArgument(0) +  ", " + call.getArgument(1) + ", " + ++projectCounter[0] + ")";
		Expression replaceableExpr = StaticJavaParser.parseExpression(md);
		return replaceableExpr;
	}

	@Override
	protected Node nextMutableNode() {
		List<MethodCallExpr> calls = this.compilationUnit.getCompilationUnit().findAll(MethodCallExpr.class);
		ArrayList<Node> mutableExpressions = Filters.filterByClassMethodNumberOfArguments(this.compilationUnit, calls, "android.database.sqlite.SQLiteDatabase", "rawQuery", 2);
	    for (int i=0; i<mutableExpressions.size(); i++) 
	    	return mutableExpressions.get(i);
	    return null;
	}

	@Override
	public String getDescription() {
		return "Invalid SQ LQuery";
	}

	@Override
	public String getFamilyName() {
		return "Database";
	}

}
