package edu.uclm.esi.newBacterioWeb.operators.classic;

import java.util.Iterator;
import java.util.List;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.expr.MethodCallExpr;
import com.github.javaparser.resolution.types.ResolvedPrimitiveType;
import com.github.javaparser.resolution.types.ResolvedType;

import edu.uclm.esi.newBacterioWeb.model.ProjectFile;
import edu.uclm.esi.newBacterioWeb.operators.Filters;

public class RSPE extends ClassicMutationOperator{
	
	@Override
	public List<Node> loadMutableNodes() {
		List<MethodCallExpr> calls = this.compilationUnit.getCompilationUnit().findAll(MethodCallExpr.class);
		return Filters.filterByNameCallAndMethodNames(this.compilationUnit, calls, "android.content.SharedPreferences.Editor", "putString", "putBoolean", "putFloat", "putInt", "putLong");
	}

	@Override 
	protected void generateMutants(ProjectFile originalFile, List<Node> mutableNodes, int[] counter) {
		Iterator<Node> itMutableExpressions = mutableNodes.iterator();
		String empty="\"\"";
		Expression replaceCadVacia=StaticJavaParser.parseExpression(empty);
		String zero = "0";
		Expression replaceZero = StaticJavaParser.parseExpression(zero);
		String cadenaNull= "(String) null";
		Expression replaceCadenaNull = StaticJavaParser.parseExpression(cadenaNull);
		while (itMutableExpressions.hasNext()) {
			MethodCallExpr call = (MethodCallExpr) itMutableExpressions.next();
			Expression argName=call.getArgument(0);
			Expression argValue=call.getArgument(1);
			ResolvedType argumentType=argValue.calculateResolvedType();
			String type = getType(argumentType);
			if(type.equals("Unsolved"))
				continue;
			if(type.equals("java.lang.String")){
				rspeString(originalFile, counter,replaceCadVacia,replaceCadenaNull, call, argName, argValue); 
			} else if (argumentType instanceof ResolvedPrimitiveType){
				rspePrimitive(originalFile, counter,replaceZero, call,argValue, type);
			}	
		}
	}

	private void rspePrimitive(ProjectFile originalFile, int[] counter, Expression replaceZero, MethodCallExpr call, Expression argValue,
			String type) {
		if(type.equals("boolean")){ //puedo quitar el booleano pq esa combinacion esta en IMCA
				Expression replace = StaticJavaParser.parseExpression("!(" + argValue.toString() + ")");
				call.getArguments().set(1, replace);
		}
		else call.getArguments().set(1, replaceZero);
		counter[0]++;
		saveClassicMutant(originalFile, counter[0]);
		call.getArguments().set(1, argValue);
	}

	private void rspeString(ProjectFile originalFile, int[] counter, Expression replaceCadVacia, Expression replaceCadenaNull,
			MethodCallExpr call, Expression argName, Expression argValue) {
		String auxName= argName.toString();
		String auxValue=argValue.toString();
		Expression replaceName = StaticJavaParser.parseExpression(auxName);
		Expression replaceValue = StaticJavaParser.parseExpression(auxValue);
		for(int i=0;i<4;i++){
			if(i==0){
				call.getArguments().set(1, replaceCadenaNull);
			} else if(i==1){
				call.getArguments().set(0, replaceValue);
				call.getArguments().set(1, replaceName);
			} else if(i==2)
				call.getArguments().set(1, replaceName);
			  else if(i==3)
				call.getArguments().set(1, replaceCadVacia);
			counter[0]++;
			saveClassicMutant(originalFile, counter[0]);
			call.getArguments().set(0, argName);
			call.getArguments().set(1, argValue);
		}
	}

	@Override
	public String getFamilyName() {
		return "Data SharedPreferences";
	}

	@Override
	public String getDescription() {
		return "Replace SharedPreferences Editor";
	}

}
