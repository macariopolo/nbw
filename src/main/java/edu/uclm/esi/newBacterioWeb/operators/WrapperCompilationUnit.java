package edu.uclm.esi.newBacterioWeb.operators;

import java.io.File;
import java.util.List;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.Modifier.Keyword;
import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.ConstructorDeclaration;
import com.github.javaparser.ast.body.FieldDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.expr.MethodCallExpr;
import com.github.javaparser.ast.expr.NameExpr;
import com.github.javaparser.ast.stmt.BlockStmt;
import com.github.javaparser.resolution.declarations.ResolvedReferenceTypeDeclaration;
import com.github.javaparser.resolution.types.ResolvedArrayType;
import com.github.javaparser.resolution.types.ResolvedReferenceType;
import com.github.javaparser.resolution.types.ResolvedType;
import com.github.javaparser.symbolsolver.JavaSymbolSolver;
import com.github.javaparser.symbolsolver.model.resolution.TypeSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.CombinedTypeSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.JarTypeSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.JavaParserTypeSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.ReflectionTypeSolver;

public class WrapperCompilationUnit{
	private CompilationUnit compilationUnit;

	private  CombinedTypeSolver combinedSolver;

	public WrapperCompilationUnit(String code, String src_path) throws Exception {
		TypeSolver reflectionTypeSolver = new ReflectionTypeSolver();
		//segun el usuario y la pc los jar
		TypeSolver jarTypeSolver= new JarTypeSolver("/Users/macariopolousaola/Library/Android/sdk/platforms/android-28/android.jar");
		TypeSolver jarTypeGoogleServices= new JarTypeSolver("/Users/macariopolousaola/.gradle/caches/modules-2/files-2.1/com.google.gms/google-services/3.2.0/202fd26a10eb8426e49bd1bdbb1ab093ea30bd76/google-services-3.2.0.jar");
		TypeSolver javaParserTypeSolver = new JavaParserTypeSolver(new File(src_path));
		
		this.combinedSolver= new CombinedTypeSolver();
		combinedSolver.add(reflectionTypeSolver);
		combinedSolver.add(javaParserTypeSolver);
		combinedSolver.add(jarTypeSolver);
		//combinedSolver.add(jarTypeGoogleServices);
		
		JavaSymbolSolver symbolSolver = new JavaSymbolSolver(combinedSolver);
		StaticJavaParser.getConfiguration().setSymbolResolver(symbolSolver);	
		
		this.compilationUnit=StaticJavaParser.parse(code); 
	}
	
	public void setCode(String code) {
		this.compilationUnit=StaticJavaParser.parse(code); 
	}
	
	public WrapperCompilationUnit(CompilationUnit cu) {
		this.compilationUnit = cu;
	}

	public ResolvedReferenceTypeDeclaration solveType(String name) {
		return this.combinedSolver.solveType(name);
	}
	
	public String getResolvedType(ResolvedType type) {
		try {
			if (type.isArray()) {
				ResolvedArrayType arrayType = type.asArrayType();
				return resolvedArray(arrayType);
			} else if (type.isReferenceType()) {
				ResolvedReferenceType rt = type.asReferenceType(); 
				return rt.describe();
			} 
			return type.describe();
		} catch (Exception e) {
			return "Unsolved";
		}
	}
	
	private String resolvedArray(ResolvedArrayType array) {
		ResolvedType type = array.getComponentType();
		if (type.isReferenceType()) {
			ResolvedReferenceType rt = type.asReferenceType(); 
			return rt.describe();
		} 
		return type.describe();
	}
	
	public CompilationUnit getCompilationUnit(){
		return this.compilationUnit;
	}
	
	public CompilationUnit getCopy() {
		return this.compilationUnit.clone();
	}

	public String getClassName() {
		ClassOrInterfaceDeclaration coid = getClass0();
		if (coid==null)
			return null;
		return coid.getNameAsString();
	}
	
	public ClassOrInterfaceDeclaration getClass0() {
		List<ClassOrInterfaceDeclaration> classes = this.compilationUnit.findAll(ClassOrInterfaceDeclaration.class);
		if (classes.isEmpty())
			return null;
		return classes.get(0);
	}

	public ClassOrInterfaceDeclaration setClassName(CompilationUnit cu, String className) {
		ClassOrInterfaceDeclaration clazz=cu.findAll(ClassOrInterfaceDeclaration.class).get(0);
		clazz.setName(className);
		List<ConstructorDeclaration> constructors = cu.findAll(ConstructorDeclaration.class);
		ConstructorDeclaration constructor;
		for (int i=0; i<constructors.size(); i++) {
			constructor=constructors.get(i);
			constructor.setName(className);
		}
		return clazz;
	}
	
	public void makeAllMembersPublic(CompilationUnit cu) {
		List<FieldDeclaration> fields = cu.findAll(FieldDeclaration.class);
		FieldDeclaration field;
		for (int i=0; i<fields.size(); i++) {
			field=fields.get(i);
			field.setModifier(Keyword.PUBLIC, true);
			field.setModifier(Keyword.PRIVATE, false);
			field.setModifier(Keyword.PROTECTED, false);
		}
		List<ConstructorDeclaration> constructors = cu.findAll(ConstructorDeclaration.class);
		ConstructorDeclaration constructor;
		for (int i=0; i<constructors.size(); i++) {
			constructor=constructors.get(i);
			constructor.setModifier(Keyword.PUBLIC, true);
			constructor.setModifier(Keyword.PRIVATE, false);
			constructor.setModifier(Keyword.PROTECTED, false);
		}
		List<MethodDeclaration> methods = cu.findAll(MethodDeclaration.class);
		MethodDeclaration method;
		for (int i=0; i<methods.size(); i++) {
			method=methods.get(i);
			method.setModifier(Keyword.PUBLIC, true);
			method.setModifier(Keyword.PRIVATE, false);
			method.setModifier(Keyword.PROTECTED, false);
		}
	}
	
	public static void replaceThisParameter(String originalClassName, MethodDeclaration method, String newClass) {
		List<MethodCallExpr> calls = method.findAll(MethodCallExpr.class);
		MethodCallExpr call;
		NodeList<Expression> arguments;
		Expression argument;
		for (int j=0; j<calls.size(); j++) {
			call = calls.get(j);
			arguments = call.getArguments();
			for (int k=0; k<arguments.size(); k++) {
				argument=arguments.get(k);
				if (argument.toString().equals("this"))
					call.replace(argument, new NameExpr("this.wrapper"));
				else if (argument.toString().equals(originalClassName + ".this"))
					call.replace(argument, new NameExpr(newClass + ".this.wrapper"));
			}
		}

	}
	
	public static void replaceThisParameter(String originalClassName, ClassOrInterfaceDeclaration clazz, String newClass) {
		List<MethodDeclaration> methods = clazz.getMethods();
		MethodDeclaration method;
		for (int i=0; i<methods.size(); i++) {
			method=methods.get(i);
			replaceThisParameter(originalClassName, method, newClass);
		}
	}
	
	public void addGetWrapper(ClassOrInterfaceDeclaration clazz, String originalClassName) {
		MethodDeclaration method = clazz.addMethod("getWrapper", Keyword.PUBLIC);
		method.setType(originalClassName);
		BlockStmt body = method.createBody();
		body.addStatement("return this.wrapper;");
	}

	public void addSetWrapper(ClassOrInterfaceDeclaration clazz, String originalClassName) {
		MethodDeclaration method = clazz.addMethod("setWrapper", Keyword.PUBLIC);
		method.setType("void");
		method.addParameter(originalClassName, "wrapper");
		BlockStmt body = method.createBody();
		body.addStatement("this.wrapper=wrapper;");
	}

	public boolean isInterface() {
		List<ClassOrInterfaceDeclaration> classes = this.compilationUnit.findAll(ClassOrInterfaceDeclaration.class);
		if (classes.isEmpty())
			return true;
		ClassOrInterfaceDeclaration clazz=classes.get(0);
		return clazz.isInterface();
	}

}
