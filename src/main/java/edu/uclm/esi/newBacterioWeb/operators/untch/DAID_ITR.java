package edu.uclm.esi.newBacterioWeb.operators.untch;

import java.util.List;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.expr.ObjectCreationExpr;
import edu.uclm.esi.newBacterioWeb.operators.Filters;

public class DAID_ITR extends UntchMutationOperator{
	
	@Override
	protected Node nextMutableNode() {
		List<ObjectCreationExpr> expressions = this.compilationUnit.getCompilationUnit().findAll(ObjectCreationExpr.class);
	    List<Node> mutableExpressions= Filters.filterByClassConstructorNumberOfArguments(this.compilationUnit, expressions, "android.content.Intent", 2);
	    for (int i=0; i<mutableExpressions.size(); i++) 
	    	return mutableExpressions.get(i);
		return null;
	}


	@Override  
	protected Expression generateMutants(Node node, int[] counter) {
		ObjectCreationExpr call = (ObjectCreationExpr) node;
		Expression arg0 = call.getArgument(0);
		Expression arg1 = call.getArgument(1);
		String md = "\t\tMutantDriver.DAID(" + arg0 + ", " + arg1 + ", " + ++counter[0] + ")";
		Expression replaceableExpr = StaticJavaParser.parseExpression(md);
		return replaceableExpr;
	}

	@Override
	public String getFamilyName() {
		return "Android intents";
	}

	@Override
	public String getDescription() {
		return "Different activity intent definition";
	}
	
	
}
