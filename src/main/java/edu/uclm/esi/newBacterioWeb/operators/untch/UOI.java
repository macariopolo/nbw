package edu.uclm.esi.newBacterioWeb.operators.untch;

import java.util.List;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.body.CallableDeclaration;
import com.github.javaparser.ast.expr.AssignExpr;
import com.github.javaparser.ast.expr.BinaryExpr;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.expr.NameExpr;
import com.github.javaparser.ast.expr.UnaryExpr.Operator;

public class UOI extends UntchMutationOperator {
	private static Operator[] applicableOperators = { 
			Operator.PLUS, Operator.MINUS, 
			Operator.PREFIX_INCREMENT, Operator.PREFIX_DECREMENT, 
			Operator.POSTFIX_INCREMENT, Operator.POSTFIX_DECREMENT };

	@Override
	public String getFamilyName() {
		return "Traditional";
	}
	
	@Override
	protected Expression generateMutants(Node node, int[] projectCounter) {
		Expression originalExpression = (Expression) node;
		Operator[] otherOperators = getOtherOperators(null);
		StringBuilder sbTransformation=
				new StringBuilder("MutantDriver.UOI(" + originalExpression.toString() + ", ");
		for (int i=0; i<otherOperators.length; i++)
			sbTransformation.append((++projectCounter[0]) + ", ");
		String newExpr=sbTransformation.substring(0,  sbTransformation.length()-2) + ")";
		
		Expression mutatedExpression = StaticJavaParser.parseExpression(newExpr);
		return mutatedExpression;
	}
	
	@Override
	protected Node nextMutableNode() {
		List<NameExpr> nameExpressions = this.compilationUnit.getCompilationUnit().findAll(NameExpr.class);
		NameExpr expr;
		Node parent;
		for (int i=0; i<nameExpressions.size(); i++) {
			expr = nameExpressions.get(i);
			parent = expr.getParentNode().get();
			if (parent instanceof AssignExpr) {
				AssignExpr ae = (AssignExpr) parent;
				if (expr != ae.getTarget() && isANumber(expr))
					return expr;
			} else if (parent instanceof BinaryExpr && isANumber(expr))
				return expr;
			else if (parent instanceof CallableDeclaration && isANumber(expr))
				return expr;
		}
		return null;
	}
	
	private Operator[] getOtherOperators(Operator op) {
		Operator[] result = new Operator[applicableOperators.length-1];
		int cont=0;
		for (int i=0; i<applicableOperators.length; i++) {
			if (applicableOperators[i]==Operator.LOGICAL_COMPLEMENT)
				continue;
			if ((op==null && applicableOperators[i]!=Operator.PLUS) || (op!=null && applicableOperators[i]!=op))
				result[cont++]=applicableOperators[i];
		}
		return result;
	}

	@Override
	public String getDescription() {
		return "Unary operator insertion";
	}
}
