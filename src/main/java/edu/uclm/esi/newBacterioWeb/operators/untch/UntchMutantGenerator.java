package edu.uclm.esi.newBacterioWeb.operators.untch;

import java.io.StringReader;
import java.util.Optional;

import javax.json.Json;

import org.json.JSONArray;
import org.springframework.web.socket.WebSocketSession;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.serialization.JavaParserJsonDeserializer;

import edu.uclm.esi.newBacterioWeb.model.Project;
import edu.uclm.esi.newBacterioWeb.model.ProjectFile;
import edu.uclm.esi.newBacterioWeb.model.UntchMutant;
import edu.uclm.esi.newBacterioWeb.operators.WrapperCompilationUnit;
import edu.uclm.esi.newBacterioWeb.web.controllers.Manager;
import edu.uclm.esi.newBacterioWeb.web.ws.WSMutantsGeneration;

public class UntchMutantGenerator {
	private WebSocketSession session;
	private Project project;
	private JSONArray operators;
	private JSONArray files;
	private JavaParserJsonDeserializer deserializer;

	public UntchMutantGenerator(WebSocketSession session, Project project, JSONArray operators, JSONArray files) {
		this.session = session;
		this.project = project;
		this.operators = operators;
		this.files = files;
		this.deserializer = new JavaParserJsonDeserializer();
		//deleteMutants();
		deleteMutantsAll();
	}

	public void generateMutants() {
		StaticJavaParser.getConfiguration().setSymbolResolver(project.getJavaSymbolSolver());
		String prefix = "edu.uclm.esi.newBacterioWeb.operators.untch.";
		String operatorName, fileId, code;
		Long n = Manager.get().getUntchMutantOperatorDAO().selectLastIndex(project.getId());
		int[] projectCounter = { n==null ? 0 : n.intValue() };
		
		for (int i=0; i<this.files.length(); i++) {
			fileId = files.getString(i);
			Optional<ProjectFile> optOriginalFile = Manager.get().getProjectFileDAO().findById(fileId);
			if (optOriginalFile.isPresent()) {
				ProjectFile originalFile = optOriginalFile.get();
				code = originalFile.getCompilationUnit();
				CompilationUnit cu = (CompilationUnit) deserializer.deserializeObject(Json.createReader(new StringReader(code)));
				WrapperCompilationUnit wrapper = new WrapperCompilationUnit(cu);
			
				UntchMutant mutant = new UntchMutant();
				mutant.setIndexFrom(projectCounter[0]+1);
				for (int j=0; j<this.operators.length(); j++) {
					operatorName = prefix + this.operators.getString(j);
					UntchMutationOperator operator;
					UntchMutationOperator_Insertion operatorI;
					try {
						Class<?> clazz = Class.forName(operatorName);
						String name= this.operators.getString(j);
						if(name.equals("ETR")|| name.equals("IEC") || name.equals("ORL") || name.equals("MDL")) {
							operatorI= (UntchMutationOperator_Insertion) clazz.newInstance();
							operatorI.generateMutants(session, originalFile, wrapper, projectCounter, project.getId(), originalFile.getId(), mutant.getId());
						}
						else {
							operator = (UntchMutationOperator) clazz.newInstance();
							operator.generateMutants(session, originalFile, wrapper, projectCounter, project.getId(), originalFile.getId(), mutant.getId());
						}
					} catch (Exception e) {
						System.err.println("Error with operator " + operatorName + ": " + e.getMessage());
						continue;
					}
				}
				wrapper.getCompilationUnit().addImport("mutantSchema.MutantDriver");
				mutant.setCode(wrapper.getCompilationUnit().toString());
				mutant.setOriginalFile(originalFile);
				mutant.setIndexTo(projectCounter[0]);
				if (mutant.getMutantIndexTo() - mutant.getMutantIndexFrom() +1 != 0)
					Manager.get().getUntchMutantDAO().save(mutant);
				WSMutantsGeneration.send(session, "type", "UNTCH_MUTANT", "amount", projectCounter[0]);
			}
		}
	}

	private void deleteMutantsAll(){
		Manager.get().getUntchMutantDAO().deleteAll();
		Manager.get().getUntchMutantOperatorDAO().deleteAll();
	}
	
	/*private void deleteMutants() {
		String fileId;
		for (int j=0; j<files.length(); j++) {
			fileId = files.getString(j);
			Optional<ProjectFile> optOriginalFile = Manager.get().getProjectFileDAO().findById(fileId);
			if (optOriginalFile.isPresent()) {
				ProjectFile originalFile = optOriginalFile.get();
				Manager.get().getUntchMutantDAO().deleteByOriginalFile(originalFile);
				Manager.get().getUntchMutantOperatorDAO().deleteByOriginalFileId(originalFile.getId());
			}
		}
	}*/
}
