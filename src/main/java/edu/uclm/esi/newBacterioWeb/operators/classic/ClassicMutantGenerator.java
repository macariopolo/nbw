package edu.uclm.esi.newBacterioWeb.operators.classic;

import java.io.StringReader;
import java.util.Optional;

import javax.json.Json;

import org.json.JSONArray;
import org.springframework.web.socket.WebSocketSession;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.serialization.JavaParserJsonDeserializer;

import edu.uclm.esi.newBacterioWeb.model.Project;
import edu.uclm.esi.newBacterioWeb.model.ProjectFile;
import edu.uclm.esi.newBacterioWeb.operators.WrapperCompilationUnit;
import edu.uclm.esi.newBacterioWeb.web.controllers.Manager;

public class ClassicMutantGenerator {
	private WebSocketSession session;
	private Project project;
	private JSONArray operators;
	private JSONArray files;
	private JavaParserJsonDeserializer deserializer;

	public ClassicMutantGenerator(WebSocketSession session, Project project, JSONArray operators, JSONArray files) {
		this.session = session;
		this.project = project;
		this.operators = operators;
		this.files = files;
		this.deserializer = new JavaParserJsonDeserializer();
		//deleteMutants();
		deleteMutantsAll();
	}

	public void generateMutants() {
		StaticJavaParser.getConfiguration().setSymbolResolver(project.getJavaSymbolSolver());
		String prefix = "edu.uclm.esi.newBacterioWeb.operators.classic.";
		String operatorName, fileId, code;
		Long n = Manager.get().getClassicMutantDAO().selectLastIndex(project.getId());
		int[] projectCounter = { n==null ? 0 : n.intValue() };
		for (int i=0; i<this.operators.length(); i++) {
			operatorName = prefix + this.operators.getString(i);
			ClassicMutationOperator operator;
			try {
				Class<?> clazz = Class.forName(operatorName);
				operator = (ClassicMutationOperator) clazz.newInstance();
			} catch (Exception e) {
				System.err.println("Error with operator " + operatorName + ": " + e.getMessage());
				continue;
			}
			for (int j=0; j<files.length(); j++) {
				fileId = files.getString(j);
				Optional<ProjectFile> optOriginalFile = Manager.get().getProjectFileDAO().findById(fileId);
				if (optOriginalFile.isPresent()) {
					ProjectFile originalFile = optOriginalFile.get();
					code = originalFile.getCompilationUnit();
					CompilationUnit cu = (CompilationUnit) deserializer.deserializeObject(Json.createReader(new StringReader(code)));
					WrapperCompilationUnit wrapper = new WrapperCompilationUnit(cu);
					operator.generateMutants(session, originalFile, wrapper, projectCounter);
				}
			}
		}
	}

	private void deleteMutantsAll(){
		Manager.get().getClassicMutantDAO().deleteAll();
	}
	
	/*private void deleteMutants() {
		String fileId;
		for (int j=0; j<files.length(); j++) {
			fileId = files.getString(j);
			Optional<ProjectFile> optOriginalFile = Manager.get().getProjectFileDAO().findById(fileId);
			if (optOriginalFile.isPresent()) {
				ProjectFile originalFile = optOriginalFile.get();
				Manager.get().getClassicMutantDAO().deleteByOriginalFile(originalFile);
			}
		}
	}*/
}
