package edu.uclm.esi.newBacterioWeb.operators.untch;

import java.util.ArrayList;
import java.util.List;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.body.VariableDeclarator;
import com.github.javaparser.ast.expr.AssignExpr;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.expr.MethodCallExpr;
import com.github.javaparser.resolution.types.ResolvedType;

public class SIR extends UntchMutationOperator{
	
	@Override
	protected Node nextMutableNode() {
		List<MethodCallExpr>calls=this.compilationUnit.getCompilationUnit().findAll(MethodCallExpr.class);
		ArrayList<Node> mutableExpressions=filter(calls);
		for (int i=0; i<mutableExpressions.size(); i++) 
	    	return mutableExpressions.get(i);
		return null;
	}

	private ArrayList<Node> filter(List<MethodCallExpr> calls) {
		ArrayList<Node> result=new ArrayList<>();
		for (int i=0; i<calls.size(); i++) {
			MethodCallExpr call = calls.get(i);
			if (call.getNameAsString().equals("getSystemService")){ 
				try {
					Node parentOpcion1 = getContainer(call, AssignExpr.class);
					Node parentOpcion2 = getContainer(call, VariableDeclarator.class);
					if(parentOpcion1!=null || parentOpcion2!=null){
					   String owner=getOwner(call);
						if (owner.contains("android.content.Context")|| owner.contains("android.app.Activity")){
						   Expression argument = call.getArguments().get(0); 
						   ResolvedType type=argument.calculateResolvedType();
						   if (!type.isArray() && getType(type).equals("java.lang.String"))
								   result.add(call);
						}
					} 
			   }catch (Exception e1) {}  
			}
		}
		return result;
	}
	
	
	@Override
	protected Expression generateMutants(Node node, int[] projectCounter) {
		MethodCallExpr call = (MethodCallExpr) node;
		List<Node> elements = call.getChildNodes();
		String md="";
		if(elements.size()>2)
			md="\t\tMutantDriver.SIR("+ elements.get(0)+  ", ";
		else
			md="\t\tMutantDriver.SIR("+ "this" + ", ";   
		md = md + call.getArgument(0);
		for (int i=0; i<2; i++)
			md = md + ", " + ++projectCounter[0];
		md = md + ")";
		Expression replaceableExpr = StaticJavaParser.parseExpression(md);
		return replaceableExpr;
			
	}

	@Override
	public String getDescription() {
		return "Service Identifier not Returned (replacing by null)";
	}

	@Override
	public String getFamilyName() {
		return "System-level service";
	}

}
