package edu.uclm.esi.newBacterioWeb.operators;

import java.util.Optional;

import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketSession;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.body.CallableDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.expr.BinaryExpr;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.expr.MethodCallExpr;
import com.github.javaparser.ast.expr.NameExpr;
import com.github.javaparser.resolution.UnsolvedSymbolException;
import com.github.javaparser.resolution.declarations.ResolvedMethodDeclaration;
import com.github.javaparser.resolution.declarations.ResolvedReferenceTypeDeclaration;
import com.github.javaparser.resolution.types.ResolvedType;


@Component
public abstract class MutationOperator {
	protected WrapperCompilationUnit compilationUnit;
	protected WebSocketSession session;
	
	public void setCompilationUnit(WrapperCompilationUnit compilationUnit) {
		this.compilationUnit = compilationUnit;
	}
	
	public String getName() {
		return this.getClass().getSimpleName();
	}
	
	public abstract String getDescription();
	
	@JsonIgnore
	public abstract String getFamilyName();
	
	protected Node getContainer(Node child, Class<?> container){
		if(child.getParentNode().isPresent()) {
			Node parent =child.getParentNode().get();
			if(parent.getClass()==container)
				return parent;
			return getContainer(parent, container);
		}
		return null;
	}
	
	protected String getType(ResolvedType type){
		return compilationUnit.getResolvedType(type);
	}
	
	protected boolean typesAreCompatible(BinaryExpr expr) {
		Expression left = expr.getLeft();
		Expression right = expr.getRight();
		try {
			return true; // left.calculateResolvedType().isPrimitive() && right.calculateResolvedType().isPrimitive();
		} catch (RuntimeException e) {
			return left.toString().startsWith("MutantDriver.") && right.toString().startsWith("MutantDriver.");
		}
	}
	
	protected String getOwner(MethodCallExpr call) {
		try {
			ResolvedMethodDeclaration methodDeclaration = call.resolve();
			ResolvedReferenceTypeDeclaration declaringType = methodDeclaration.declaringType();
			return declaringType.getQualifiedName();
		} catch (UnsolvedSymbolException e){
			try {
				NameExpr objeto = (NameExpr) call.getChildNodes().get(0);
				ResolvedType type = objeto.calculateResolvedType();
				return getType(type);
			} catch (Exception e1) {
				return "Unsolved";
			}
		}
	}
	
	protected CallableDeclaration<?> findParentMethod(Node childNode) {
		boolean found=false;
		Optional<Node> optionalParent;
		Node parent=null;
		do {
			optionalParent=childNode.getParentNode();
			if (optionalParent.isPresent()) {
				parent=optionalParent.get();
				if (parent instanceof MethodDeclaration || parent instanceof CallableDeclaration)
					found=true;
				else
					childNode=parent;
			} else {
				break;
			}
		} while (!found);
		if (parent instanceof CallableDeclaration)
			return (CallableDeclaration<?>) parent;
		return null;
	}
	
	protected boolean isANumber(NameExpr expr) {
		try {
			ResolvedType resolvedType = expr.calculateResolvedType();
			if (resolvedType.isPrimitive()) {
				String type = resolvedType.describe();
				if (type.equals("int") || type.equals("float") || type.equals("byte") || type.equals("long") || type.equals("double"))
					return true;
			}
		} catch (RuntimeException e) {
		}
		return false;
	}
	
}
