package edu.uclm.esi.newBacterioWeb.operators.classic;

import java.util.List;

import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.expr.MethodCallExpr;
import com.github.javaparser.ast.expr.StringLiteralExpr;

import edu.uclm.esi.newBacterioWeb.model.ProjectFile;
import edu.uclm.esi.newBacterioWeb.operators.Filters;

public class IKIPE extends ClassicMutationOperator {

	public List<Node> loadMutableNodes() {
		List<MethodCallExpr> expressions = this.compilationUnit.getCompilationUnit().findAll(MethodCallExpr.class);
		return Filters.filterByClassMethodNumberOfArguments(this.compilationUnit, expressions, "android.content.Intent", "putExtra", 2);
	}
	
	@Override
	public String getFamilyName() {
		return "Android intents";
	}

	@Override
	protected void generateMutants(ProjectFile originalFile, List<Node> mutableNodes, int[] projectCounter) {
		MethodCallExpr expr;
		for (int i=0; i<mutableNodes.size(); i++) {
			expr = (MethodCallExpr) mutableNodes.get(i);
			Expression arg = expr.getArgument(0);
			String argName = arg.toString();
			String newArgName = "_" + argName;
			StringLiteralExpr newArg = new StringLiteralExpr(newArgName);
			expr.setArgument(0, newArg);
			projectCounter[0]++;
			saveClassicMutant(originalFile, projectCounter[0]);
			expr.setArgument(0, arg);
		}
	}
	
	@Override
	public String getDescription() {
		return "Invalid key intent put extra";
	}
}
