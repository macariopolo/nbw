package edu.uclm.esi.newBacterioWeb.so;

import org.json.JSONObject;

public interface IProcessExecutorListener {

	void onProcessExecuted(JSONObject executionResult);

	String getDescription();

}
