package edu.uclm.esi.newBacterioWeb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LanzadoraNBW {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(LanzadoraNBW.class, args);
	}

}
