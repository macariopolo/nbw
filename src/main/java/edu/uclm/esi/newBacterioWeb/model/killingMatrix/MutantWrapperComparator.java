package edu.uclm.esi.newBacterioWeb.model.killingMatrix;

import java.util.Comparator;

import edu.uclm.esi.newBacterioWeb.model.MutantWrapper;

public class MutantWrapperComparator implements Comparator<MutantWrapper> {
	@Override
	public int compare(MutantWrapper o1, MutantWrapper o2) {
		if (o1.getDeviceName().compareTo(o2.getDeviceName())==0)
			return o1.getIndex().compareTo(o2.getIndex());
		return o1.getDeviceName().compareTo(o2.getDeviceName());
	}
}
