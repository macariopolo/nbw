package edu.uclm.esi.newBacterioWeb.model;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.json.Json;
import javax.json.stream.JsonGenerator;
import javax.json.stream.JsonGeneratorFactory;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import org.json.JSONObject;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.type.ClassOrInterfaceType;
import com.github.javaparser.serialization.JavaParserJsonDeserializer;
import com.github.javaparser.serialization.JavaParserJsonSerializer;

import edu.uclm.esi.newBacterioWeb.web.controllers.Manager;

@Entity
public class ProjectFile {
	@Id
	private String id;
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	private Project project;
	@Column
	private String fileName;
	@Column(name = "contents")
	@JsonIgnore
	@Lob
	private byte[] contents;
	@Column(columnDefinition = "LONGTEXT")
	@Lob
	@JsonIgnore
	private String compilationUnit;
	@JsonIgnore
	@OneToMany(cascade = CascadeType.REMOVE, mappedBy = "originalFile", fetch = FetchType.LAZY)
	private List<ClassicMutant> classicMutants;
	@OneToOne(cascade = CascadeType.ALL, mappedBy = "projectFile", fetch = FetchType.LAZY)
	private TestClass testClass;
	@Transient
	private static JavaParserJsonDeserializer deserializer = new JavaParserJsonDeserializer();

	public ProjectFile() {
		this.id = UUID.randomUUID().toString();
		this.classicMutants = new ArrayList<>();
	}

	public ProjectFile(Project project, String fileName, byte[] contents) {
		this();
		this.project = project;
		this.fileName = fileName;
		this.contents = contents;
	}
	
	public void analyzeJavaCode(WebSocketSession session, int index, int total) {
		StaticJavaParser.getConfiguration().setSymbolResolver(this.project.getJavaSymbolSolver());
		String code = new String(this.contents);
		CompilationUnit cu=StaticJavaParser.parse(code);
		this.compilationUnit = serialize(cu);
		JSONObject jso = new JSONObject();
		jso.put("type", "MESSAGE").put("message", index + "/" + total + ": analyzing " + this.fileName);
		try {
			session.sendMessage(new TextMessage(jso.toString()));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (fileName.contains("src/androidTest") || fileName.contains("src\\androidTest")) {
			TestClassType[] testClassType = new TestClassType[1];
			String[] packageName = new String[1];
			ClassOrInterfaceDeclaration clazz = this.isTestClass(code, packageName, testClassType);
			if (clazz!=null) {
				this.testClass = new TestClass();
				this.testClass.setName(packageName[0] + "." + clazz.getNameAsString());
				this.testClass.setChecked(true);
				this.testClass.setProjectFile(this);
				this.testClass.setTestType("androidTest");
				this.testClass.addTestMethods(clazz, testClassType[0]);
			}
		} else if (fileName.contains("src/test") || fileName.contains("src\\test")) {
			TestClassType[] testClassType = new TestClassType[1];
			String[] packageName = new String[1];
			ClassOrInterfaceDeclaration clazz = this.isTestClass(code, packageName, testClassType);
			if (clazz!=null) {
				this.testClass = new TestClass();
				this.testClass.setName(packageName[0] + "." + clazz.getNameAsString());
				this.testClass.setChecked(true);
				this.testClass.setProjectFile(this);
				this.testClass.setTestType("test");
				this.testClass.addTestMethods(clazz, testClassType[0]);
			}
		}
		Manager.get().getProjectFileDAO().save(this);
	}
	

	private ClassOrInterfaceDeclaration isTestClass(String code, String[] packageName, TestClassType[] testClassType) {
		CompilationUnit cu = StaticJavaParser.parse(code);
		List<ClassOrInterfaceDeclaration> classes = cu.findAll(ClassOrInterfaceDeclaration.class);
		if (classes.isEmpty())
			return null;
		ClassOrInterfaceDeclaration clazz = classes.get(0);
		NodeList<ClassOrInterfaceType> superclasses = clazz.getExtendedTypes();
		String superclassName;
		ClassOrInterfaceType superclass;
		for (int i=0; i<superclasses.size(); i++) {
			superclass = superclasses.get(i);
			superclassName = superclass.getNameAsString();
			if (superclassName.equals("InstrumentationTestCase"))
				testClassType[0] = TestClassType.INSTRUMENTATION_TEST_CASE;
			else if (superclassName.equals("TestCase"))
				testClassType[0] =  TestClassType.JUNIT_FRAMEWORK_TEST_CASE;
			else if (superclassName.equals("AndroidTestCase"))
				testClassType[0] =  TestClassType.ANDROID_TEST_CASE;
		}
		if (testClassType[0]==null && clazz.getAnnotationByName("RunWith").isPresent())
			testClassType[0] =  TestClassType.RUN_WITH;
		if (testClassType[0]==null) {
			List<MethodDeclaration> methods;
			methods = clazz.getMethods();
			for (MethodDeclaration method : methods) {
				if (method.isAnnotationPresent("Test")) {
					testClassType[0] = TestClassType.OTHER;
					break;
				}
			}
		}
		if (testClassType[0]!=null) {
			packageName[0] = cu.getPackageDeclaration().isPresent() ? cu.getPackageDeclaration().get().getNameAsString() : "";
			return clazz;
		}
		return null;
	}
	
	@JsonIgnore
	public List<ClassicMutant> getClassicMutants() { 
		return classicMutants;
	}
	
	public void setClassicMutants(List<ClassicMutant> classicMutants) {
		this.classicMutants = classicMutants;
	}
	
	public int getNumberOfMutants() {
		return this.classicMutants.size();
	}

	@JsonIgnore
	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	@JsonIgnore
	public byte[] getContents() {
		return contents;
	}

	public void setContents(byte[] contents) {
		this.contents = contents;
	}

	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	@JsonIgnore
	public String getCompilationUnit() {
		return compilationUnit;
	}
	
	public void setCompilationUnit(String compilationUnit) {
		this.compilationUnit = compilationUnit;
	}
	
	@JsonIgnore
	public TestClass getTestClass() {
		return testClass;
	}
	
	public void setTestClass(TestClass testClass) {
		this.testClass = testClass;
	}
	
	@JsonIgnore
	public String getCode() {
		return deserializer.deserializeObject(Json.createReader(new StringReader(this.compilationUnit))).toString();
	}
	
	private String serialize(Node node) {
        Map<String, ?> config = new HashMap<>();
        config.put(JsonGenerator.PRETTY_PRINTING, null);
        JsonGeneratorFactory generatorFactory = Json.createGeneratorFactory(config);
        JavaParserJsonSerializer serializer = new JavaParserJsonSerializer();
        StringWriter jsonWriter = new StringWriter();
        try (JsonGenerator generator = generatorFactory.createGenerator(jsonWriter)) {
            serializer.serialize(node, generator);
        }
        return jsonWriter.toString();
    }
}
