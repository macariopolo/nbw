package edu.uclm.esi.newBacterioWeb.model;

public enum TestClassType {
	RUN_WITH, JUNIT_FRAMEWORK_TEST_CASE, INSTRUMENTATION_TEST_CASE, ANDROID_TEST_CASE, OTHER
}
