package edu.uclm.esi.newBacterioWeb.model.killingMatrix;

import java.util.ArrayList;
import java.util.List;

import edu.uclm.esi.newBacterioWeb.model.TestMethod;

public class KillingMatrixRow {
	private TestMethod testMethod;
	private List<KillingMatrixCell> cells;
	private Integer killedMutants;
	
	public KillingMatrixRow() {
		this.killedMutants = 0;
		this.cells = new ArrayList<>();
	}

	public void setTestMethod(TestMethod testMethod) {
		this.testMethod = testMethod;
	}
	
	public TestMethod getTestMethod() {
		return testMethod;
	}

	public void setValue(int posMutantIndex, KillingMatrixCellValue state) {
		KillingMatrixCell cell;
		if (posMutantIndex==this.cells.size())
			this.cells.add(new KillingMatrixCell());
		cell = this.cells.get(posMutantIndex);
		cell.setState(state);
	}

	public KillingMatrixCell get(int index) {
		return this.cells.get(index);
	}

	public void setColumns(int size) {
		for (int i=0; i<size; i++)
			this.cells.add(new KillingMatrixCell());
	}

	public void increaseKilledMutants() {
		this.killedMutants++;
	}

	public Integer getKilledMutants() {
		return this.killedMutants;
	}

	public void remove(int index) {
		this.cells.remove(index);
	}

}
