package edu.uclm.esi.newBacterioWeb.model.killingMatrix;

import org.json.JSONObject;

public class KillingMatrixCell {
	private KillingMatrixCellValue state;
	private int executionOrder;
	private String fileName;

	public KillingMatrixCell() {
		this.executionOrder = 0;
		this.state = KillingMatrixCellValue.UNVISITED;
	}
	
	public void setExecutionOrder(int executionOrder) {
		this.executionOrder = executionOrder;
	}
	
	public KillingMatrixCellValue getState() {
		return this.state;
	}
	
	public void setState(KillingMatrixCellValue state) {
		this.state = state;
	}

	public int getExecutionOrder() {
		return executionOrder;
	}

	public void setFile(String fileName) {
		this.fileName = fileName;
	}
	
	public String getFileName() {
		return fileName;
	}

	public JSONObject toJSON() {
		return new JSONObject().put("state", this.state).put("executionOrder", this.executionOrder).put("fileName", this.fileName);
	}
}
