package edu.uclm.esi.newBacterioWeb.model;

import java.util.ArrayList;

import org.springframework.stereotype.Component;

@Component
public class Table {
	private ArrayList<String> rowNames;
	private ArrayList<String> colNames;
	private Object[][] values;
	
	public Table() {
		this.rowNames = new ArrayList<>();
		this.colNames = new ArrayList<>();
	}

	public void addRow(String rowName) {
		if (!this.rowNames.contains(rowName)) 
			this.rowNames.add(rowName);
	}

	public void addColumn(String columnName) {
		if (!this.colNames.contains(columnName))
			this.colNames.add(columnName);
	}

	public void set(String rowName, String columnName, Object value) {		
		int rowIndex = this.rowNames.indexOf(rowName);
		int colIndex = this.colNames.indexOf(columnName);
		this.values[rowIndex][colIndex] = value;
	}

	public ArrayList<ArrayList<String>> getTable() {
		ArrayList<ArrayList<String>> result = new ArrayList<>();
		ArrayList<String> row = new ArrayList<>();
		row.add("");
		for (int j=0; j<this.colNames.size(); j++) 
			row.add(this.colNames.get(j));
		result.add(row);
		for (int i=0; i<this.rowNames.size(); i++) {
			row = new ArrayList<>();
			result.add(row);
			row.add(this.rowNames.get(i));
			for (int j=0; j<this.colNames.size(); j++) {
				if (this.values[i][j]!=null)
					row.add(this.values[i][j].toString());
				else
					row.add("");
			}
		}
		return result;
	}

	public void ready() {
		this.values = new Object[this.rowNames.size()][this.colNames.size()];
	}
}
