package edu.uclm.esi.newBacterioWeb.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class TestMethod {
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column
	private String name;
	@Column
	private boolean checked;
	@JsonIgnore
	@ManyToOne
	private TestClass testClass;
	@Transient
	private int numberOfKilledMutants;
	
	public TestMethod() {
	}

	public TestMethod(String name, boolean checked) {
		this();
		this.name = name;
		this.checked = checked;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}
	
	public void setTestClass(TestClass testClass) {
		this.testClass = testClass;
	}
	
	public TestClass getTestClass() {
		return testClass;
	}

	public void increaseKilled() {
		this.numberOfKilledMutants++;
	}

	public int getKilledMutants() {
		return this.numberOfKilledMutants;
	}
	
}
