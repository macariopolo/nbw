package edu.uclm.esi.newBacterioWeb.model.killingMatrix;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.json.JSONArray;
import org.json.JSONObject;

import com.fasterxml.jackson.annotation.JsonIgnore;

import edu.uclm.esi.newBacterioWeb.model.MutantWrapper;
import edu.uclm.esi.newBacterioWeb.model.TaskInfo;
import edu.uclm.esi.newBacterioWeb.model.TestMethod;
import edu.uclm.esi.newBacterioWeb.model.devices.DeviceProxy;

public class KillingMatrix implements TaskInfo {
	private String id;
	private List<String> testMethodNames;
	private List<DeviceProxy> devices;
	private HashSet<String> selectedTestCases;
	private HashSet<String> ineffectiveTestCases;
	private int executionOrder;
	private float ms;
	private StringComparator testMethodNameComparator;
	private List<TestMethod> testMethods;
	
	public KillingMatrix(String id, List<TestMethod> testMethods, List<DeviceProxy> devices) {
		this.id = id;
		this.testMethodNames = new Vector<>();
		for (TestMethod testMethod : testMethods)
			this.testMethodNames.add(testMethod.getName());
		this.testMethods = testMethods;
		this.testMethods.sort(new Comparator<TestMethod>() {
			@Override
			public int compare(TestMethod o1, TestMethod o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});
		this.testMethodNames.sort(this.testMethodNameComparator);
		devices.sort(new DeviceComparator());
		this.devices = devices;
		this.selectedTestCases = new HashSet<>();
		this.ineffectiveTestCases = new HashSet<>();
		this.executionOrder = 0;
		this.ms = Float.MIN_VALUE;
	}

	public synchronized KillingMatrixCell set(String testMethodName, MutantWrapper mutantWrapper, KillingMatrixCellValue state, DeviceProxy device) {
		int posTestMethod = Collections.binarySearch(this.testMethodNames, testMethodName, this.testMethodNameComparator);
		KillingMatrixCell cell;
		if (mutantWrapper!=null)
			cell = device.setResult(posTestMethod, state);
		else {
			cell = new KillingMatrixCell();
			cell.setState(state);
		}
		if (cell.getState()==KillingMatrixCellValue.KILLED)
			this.testMethods.get(posTestMethod).increaseKilled();
		cell.setExecutionOrder(++this.executionOrder);
		return cell;
	}
	
	public synchronized KillingMatrixCell set(String testMethodName, MutantWrapper mutantWrapper, KillingMatrixCellValue killed, DeviceProxy device, String errorFileName) {
		KillingMatrixCell cell = this.set(testMethodName, mutantWrapper, killed, device);
		cell.setFile(errorFileName);
		return cell;
	}

	public void addColumn(DeviceProxy device, MutantWrapper mutantWrapper, boolean notifyToClient) {
		mutantWrapper.setTestResults(this.testMethodNames);
		device.addMutant(mutantWrapper, notifyToClient);
	}
	
	public JSONArray getSelectedTestCases() {
		JSONArray jsa = new JSONArray();
		for (String testCaseName : this.selectedTestCases)
			jsa.put(testCaseName);
		return jsa;
	}
	
	public JSONArray getIneffectiveTestCases() {
		JSONArray jsa = new JSONArray();
		for (String testCaseName : this.ineffectiveTestCases)
			jsa.put(testCaseName);
		return jsa;
	}

	@Override
	public String toString() {
		String r = "";
		r="\t";
		DeviceProxy device;
		for (int i=0; i<this.devices.size(); i++) {
			device = this.devices.get(i);
			for (int j=0; j<device.getMutants().size(); j++) {
				r+=device.getDescription() + "#" + device.getMutants().get(j).getIndex() + "\t";
			}
		}
		r+="\n";
		/*String testMethodName;
		for (int i=0; i<this.testMethodNames.size(); i++) {
			testMethodName = this.testMethodNames.get(i);
			r+= testMethodName + "\t";
			for (int j=0; j<this.mutantWrappers.size(); j++) {
				KillingMatrixCell cell = row.get(j);
				if (cell!=null)
					r+=cell.getState() + "\t";
				else
					r+="-\t";
			}
			r+=row.getKilledMutants();
			r+="\n";
		}*/
		r+="ms = " + this.getMs();
		return r;
	}

	public void markAsNonCompilable(MutantWrapper mutantWrapper, String msg, DeviceProxy deviceProxy) {
		mutantWrapper.setNonCompilable(msg);
	}
	
	@JsonIgnore
	private float getMs() {
		this.ms = 0;
		int killedMutants = 0;
		int totalMutants = 0;
		for (DeviceProxy device : this.devices) {
			totalMutants = device.getMutants().size();
			killedMutants+=device.getMutants().stream().filter(mutant -> mutant.getNumberOfKillings()>0).count();
		}
		if (totalMutants>0)
			this.ms = (float) (killedMutants) / totalMutants;
		return ms;
	}

	public JSONObject toJSON() {
		JSONObject jso = new JSONObject();
		jso.put("id", this.id);
		JSONArray devices = new JSONArray();
		for (DeviceProxy device : this.devices) {
			JSONObject jsoDevices = new JSONObject();
			jsoDevices.put("deviceName", device.getDescription());
			JSONArray jsaMutants = new JSONArray();
			for (MutantWrapper mutant : device.getMutants()) 
				jsaMutants.put(mutant.getIndex());
			jsoDevices.put("mutantIndexes", jsaMutants);
			
			devices.put(jsoDevices);
		}
		
		jso.put("devices", devices);
		jso.put("testMethods", new JSONArray(this.testMethodNames));
		return jso;
	}
	
	public Map<String, Object> getJSON() {
		return toJSON().toMap();
	}

	public TestMethod getBestTestMethod(List<TestMethod> executedTestMethods) {
		if (executedTestMethods.size()==this.testMethodNames.size())
			return null;
		int max = Integer.MIN_VALUE;
		TestMethod bestTestMethod = null;
		TestMethod testMethod;
		for (int i=0; i<this.testMethods.size(); i++) {
			testMethod = this.testMethods.get(i);
			if (!executedTestMethods.contains(testMethod) && testMethod.getKilledMutants()>max) {
				max = testMethod.getKilledMutants();
				bestTestMethod = testMethod;
			}
		}
		return bestTestMethod;
	}

	@JsonIgnore
	public JSONObject getSummary() {
		JSONObject jso = new JSONObject();
		jso.put("mutationScore", this.getMs());
		sortByKilledMutants();
		removeIneffectiveTestCases();
		TestMethod testMethod;
		while (!this.testMethods.isEmpty()) {
			testMethod=this.testMethods.get(0);
			this.selectedTestCases.add(testMethod.getName());
			removeKilledMutants(testMethod);
			this.testMethods.remove(0);
			removeIneffectiveTestCases();
		}		
		jso.put("selectedTestCases", new JSONArray(this.selectedTestCases));
		jso.put("ineffectiveTestCases", new JSONArray(this.ineffectiveTestCases));
		return jso;
	}

	private void removeKilledMutants(TestMethod testMethod) {
		/*for (int i=this.mutantWrappers.size()-1; i>=0; i--) {
			KillingMatrixCell cell = row0.get(i);
			if (cell.getState()==KillingMatrixCellValue.KILLED || cell.getState()==KillingMatrixCellValue.NON_COMPILABLE)
				this.removeColumn(i);
		}*/
	}

	private void removeColumn(int index) {
		/*this.mutantWrappers.remove(index);
		for (int i=0; i<this.rows.size(); i++) {
			KillingMatrixRow row = this.rows.get(i);
			row.remove(index);
		}*/
	}

	private void removeIneffectiveTestCases() {
		if (!this.testMethods.isEmpty()) {
			int last = this.testMethods.size()-1;
			TestMethod testMethod = this.testMethods.get(last);
			while (testMethod.getKilledMutants()==0 && !this.testMethods.isEmpty()) {
				this.testMethods.remove(last--);
				this.ineffectiveTestCases.add(testMethod.getName());
				if (last>=0)
					testMethod = this.testMethods.get(last);
			}
		}
	}

	private void sortByKilledMutants() {
		this.testMethods.sort(new Comparator<TestMethod>() {
			@Override
			public int compare(TestMethod o1, TestMethod o2) {
				return o2.getKilledMutants()-o1.getKilledMutants();
			}
		});
	}
}
