package edu.uclm.esi.newBacterioWeb.model;

public enum ExecutionMode {
	ORIGINAL, CLASSIC_MUTANTS, UNTCH_MUTANTS;
}
