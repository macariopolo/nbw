package edu.uclm.esi.newBacterioWeb.model;

import java.util.List;
import java.util.Vector;

import edu.uclm.esi.newBacterioWeb.model.killingMatrix.KillingMatrix;

public class OnlyAlive extends RunStrategy {
	private Vector<TestMethod> executedTestMethods;
	private KillingMatrix killingMatrix;
	private ExecutionOrder executionOrder;

	public OnlyAlive(TestRunnerGroup testRunnerGroup) {
		super(testRunnerGroup);
		this.killingMatrix = testRunnerGroup.getKillingMatrix();
		this.executionOrder = testRunnerGroup.getExecutionOrder();
		this.executedTestMethods = new Vector<>();
	}

	@Override
	public TestMethod getNextTestMethod() {
		if (this.lastMutantKilled)
			return null;
		
		TestClass testClass = this.testClasses.get(this.testClassesCounter);
		List<TestMethod> testMethods = testClass.getTestMethods();
		TestMethod testMethod = null;
		
		if (this.executionOrder == ExecutionOrder.SEQUENTIAL) {
			if (this.testMethodsCounter < testMethods.size())
				testMethod = testMethods.get(this.testMethodsCounter++);
			else if (this.testClassesCounter+1<this.testClasses.size()) {
				testClass = this.testClasses.get(++this.testClassesCounter);
				this.testMethodsCounter = 0;
				testMethods = testClass.getTestMethods();
				testMethod = testMethods.get(this.testMethodsCounter);
			}
		} else {
			testMethod = this.killingMatrix.getBestTestMethod(executedTestMethods);
			this.executedTestMethods.add(testMethod);	
		}
		return testMethod;
	}
	
	@Override
	public void reset() {
		super.reset();
		this.executedTestMethods.clear();
	}
}
