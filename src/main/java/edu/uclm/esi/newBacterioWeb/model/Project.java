package edu.uclm.esi.newBacterioWeb.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.springframework.web.socket.WebSocketSession;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.github.javaparser.symbolsolver.JavaSymbolSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.CombinedTypeSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.JarTypeSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.JavaParserTypeSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.ReflectionTypeSolver;

import edu.uclm.esi.newBacterioWeb.web.controllers.Manager;
import edu.uclm.esi.newBacterioWeb.web.exceptions.ProjectConfigurationException;

@Entity
public class Project {
	@Id
	private String id;
	@Column
	private String name;
	@ManyToOne
	private User creator;
	@ManyToMany
	private List<User> contributors;
	@JsonIgnore	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<ProjectFile> projectFiles;
	@JsonIgnore
	@Transient
	private List<TestClass> testClasses;
	@Transient @JsonIgnore
	private JavaSymbolSolver javaSymbolSolver;
	@OneToOne(mappedBy = "project", fetch = FetchType.LAZY)
	private ProjectConfiguration projectConfiguration;
	@Column
	private String androidManifest;
	@Transient @JsonIgnore
	private static Object cerrojo = new Object();
	
	public Project() {
		this.id = UUID.randomUUID().toString();
		this.contributors = new ArrayList<>();
		this.projectFiles = new ArrayList<>();
		this.testClasses = new ArrayList<>(); 
	}
	
	@JsonIgnore
	public String getProjectsFolder() {
		return System.getProperty("user.home") + "/nbw/projects/";
	}
	
	@JsonIgnore
	public String getProjectFolder() {
		return getProjectsFolder() + this.name + "/";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public User getCreator() {
		return creator;
	}

	public void setCreator(User creator) {
		this.creator = creator;
		this.addContributor(creator);
	}

	public List<User> getContributors() {
		return contributors;
	}
	
	public void addContributor(User contributor) {
		this.contributors.add(contributor);
	}

	public void setContributors(List<User> contributors) {
		this.contributors = contributors;
	}

	public String getId() {
		return id;
	}
	
	public void setProjectFiles(List<ProjectFile> projectFiles) {
		this.projectFiles = projectFiles;
	}

	//@JsonIgnore
	public List<ProjectFile> getSourceFiles() {
		return this.projectFiles;
	}
	
	public void loadSourceFiles() {
		List<Map<String, Object>> projectFiles = Manager.get().getProjectFileDAO().loadJavaFiles(this.id);
		for (Map<String, Object> projectFile : projectFiles) {
			String id = projectFile.get("id").toString();
			String name = projectFile.get("file_name").toString();
			ProjectFile pf = new ProjectFile();
			pf.setProject(this);
			pf.setId(id);
			pf.setFileName(name);
			this.projectFiles.add(pf);
		}
	}
	
	@JsonIgnore
	public ProjectConfiguration getProjectConfiguration() {
		return projectConfiguration;
	}
	
	public void setProjectConfiguration(ProjectConfiguration projectConfiguration) {
		this.projectConfiguration = projectConfiguration;
	}

	@JsonIgnore
	public JavaSymbolSolver getJavaSymbolSolver() {
		if (this.javaSymbolSolver!=null)
			return this.javaSymbolSolver;
		CombinedTypeSolver combinedSolver = new CombinedTypeSolver();
		
		List<String> libraries = this.projectConfiguration.getLibraries();
		for (String library : libraries) {
			JarTypeSolver jarTypeSolver;
			try {
				jarTypeSolver = new JarTypeSolver(library);
				combinedSolver.add(jarTypeSolver);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		String sdkPath = this.getSdkPath();
		if (sdkPath!=null) {
			String compileSdkVersion = this.getSdkPath() + "platforms/android-" + this.projectConfiguration.getCompileSdkVersion() + "/android.jar";
			try {
				JarTypeSolver jarTypeSolver = new JarTypeSolver(compileSdkVersion);
				combinedSolver.add(jarTypeSolver);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		ReflectionTypeSolver reflectionTypeSolver = new ReflectionTypeSolver();
		combinedSolver.add(reflectionTypeSolver);
		JavaParserTypeSolver javaParserTypeSolver = 
				new JavaParserTypeSolver(this.getProjectFolder() + this.projectConfiguration.getSrcFolder() + "main/java/");
		combinedSolver.add(javaParserTypeSolver);
		this.javaSymbolSolver = new JavaSymbolSolver(combinedSolver);
		return this.javaSymbolSolver;
	}

	private String getSdkPath() {
		Optional<RemoteHost> optRemoteHost = Manager.get().getRemoteHostDAO().findById("127.0.0.1");
		if (optRemoteHost.isPresent()) {
			RemoteHost remoteHost = optRemoteHost.get();
			return remoteHost.getSdkPath();
		}
		return null;
	}

	public void setAndroidManifest(String androidManifest) {
		this.androidManifest = androidManifest;
	}
	
	@JsonIgnore
	public String getAndroidManifest() {
		return androidManifest;
	}
	
	public void analyzeJavaFiles(WebSocketSession session) {
		List<String> ids = Manager.get().getProjectFileDAO().findJavaIdsByProject(this.id);
		Optional<ProjectFile> optProjectFile;
		ProjectFile projectFile;
		String projectFileId;
		int total = ids.size();
		for (int i=0; i<total; i++) {
			projectFileId = ids.get(i);
			optProjectFile = Manager.get().getProjectFileDAO().findById(projectFileId);
			if (optProjectFile.isPresent()) {
				projectFile = optProjectFile.get();
				projectFile.setProject(this);
				projectFile.analyzeJavaCode(session, i+1, total);
			}
		}
	}

	public List<ProjectConfigurationException> calculateConfiguration() throws Exception {
		ArrayList<ProjectConfigurationException> errorMessages = new ArrayList<>();
		if (this.androidManifest!=null) {
			this.projectConfiguration = new ProjectConfiguration();
			this.projectConfiguration.setProject(this);
			
			try {
				this.projectConfiguration.setGradlewCommand(this.guessGradleW());
			} catch (ProjectConfigurationException e) {
				errorMessages.add(e);
			}
			try {
				this.projectConfiguration.setLocalProperties(guessLocalProperties());
			} catch (ProjectConfigurationException e) {
				errorMessages.add(e);
			}
			try {
				this.projectConfiguration.setAppFolder(this.guessAppFolder());
			} catch (ProjectConfigurationException e) {
				errorMessages.add(e);
			}
			try {
				this.projectConfiguration.setBuildGradle(guessBuildGradle());
			} catch (ProjectConfigurationException e) {
				errorMessages.add(e);
			}
			try {
				this.projectConfiguration.setSrcFolder(this.guessSrcFolder());
			} catch (ProjectConfigurationException e) {
				errorMessages.add(e);
			}
			try {
				this.projectConfiguration.setManifestPath(this.guessManifest());
			} catch (ProjectConfigurationException e) {
				errorMessages.add(e);
			}
			try {
				this.projectConfiguration.setApplicationId(this.guessApplicationId());
			} catch (ProjectConfigurationException e) {
				errorMessages.add(e);
			}
			try {
				this.projectConfiguration.setTestRunner(this.guessTestRunner());
			} catch (ProjectConfigurationException e) {
				errorMessages.add(e);
			}
			
			this.projectConfiguration.setBuildAPKCommand(this.projectConfiguration.getGradlewCommand() + " assembleDebug");
			String installCommand = "platform-tools/adb -s #DEVICE# shell pm install -t -r \"#PACKAGE_TO_INSTALL#\"";
			if (this.projectConfiguration.getApplicationId()!=null)
				installCommand = installCommand.replace("#PACKAGE_TO_INSTALL#", this.projectConfiguration.getApplicationId());
			this.projectConfiguration.setInstallAPKCommand(installCommand);
			
			try {
				this.projectConfiguration.setDebugAPKPathAndName(this.guessDebugAPKPathAndName());
			} catch (ProjectConfigurationException e) {
				errorMessages.add(e);
			}
			
			try {
				this.projectConfiguration.setDebugPackage(this.guessDebugPackage());
			} catch (ProjectConfigurationException e) {
				errorMessages.add(e);
			}
			
			if (this.projectConfiguration.getGradlewCommand()!=null)
				this.projectConfiguration.setBuildTestAPKCommand(this.projectConfiguration.getGradlewCommand() + " assembleDebugAndroidTest");
			if (this.projectConfiguration.getInstallTestAPKCommand()!=null)
			this.projectConfiguration.setInstallTestAPKCommand("platform-tools/adb -s #DEVICE# shell pm install -t -r \"#PACKAGE_TO_INSTALL#\"");
			try {
				this.projectConfiguration.setTestAPKPathAndName(this.guessTestAPKPathAndName());
			} catch (ProjectConfigurationException e) {
				errorMessages.add(e);
			}
			
			try {
				this.projectConfiguration.setTestPackage(this.guessTestPackage());
			} catch (ProjectConfigurationException e) {
				errorMessages.add(e);
			}
			
			if (this.projectConfiguration.getGradlewCommand()!=null)
				this.projectConfiguration.setUnitTestCommand(this.projectConfiguration.getGradlewCommand() + " testDebugUnitTest");
			
			try {
				this.projectConfiguration.setCompileSdkVersion(this.guessCompileSdkVersion());
			} catch (ProjectConfigurationException e) {
				errorMessages.add(e);
			}
			
			Manager.get().getProjectConfigurationDAO().save(this.projectConfiguration);
		}
		return errorMessages;
	}
	

	public String guessGradleW() throws ProjectConfigurationException {
		String path = this.getProjectFolder() + "gradlew";
		File file = new File(path);
		if (!file.exists())
			throw new ProjectConfigurationException("gradlewCommand", "gradlew not found");
		path = "./gradlew";
		this.projectConfiguration.setGradlewCommand(path);
		return path;
	}
	
	public String guessLocalProperties() throws ProjectConfigurationException {
		File file = new File(this.getProjectFolder() + "local.properties");
		if (!file.exists())
			throw new ProjectConfigurationException("localProperties", "local.properties not found");
		String path = file.getAbsolutePath().substring(this.getProjectFolder().length());
		this.projectConfiguration.setLocalProperties(path);
		return path;
	}
	
	public String guessAppFolder() throws ProjectConfigurationException {
		File file = new File(this.getProjectFolder() + this.androidManifest);
		if (!file.exists())
			throw new ProjectConfigurationException("appFolder", "Cannot guess app folder");
		file = file.getParentFile().getParentFile().getParentFile();
		if (!file.exists())
			throw new ProjectConfigurationException("appFolder", "Cannot guess app folder");
		String appFolder = file.getAbsolutePath();
		if (!appFolder.endsWith(File.separator))
			appFolder = appFolder + File.separatorChar;
		appFolder = appFolder.substring(this.getProjectFolder().length());
		this.projectConfiguration.setAppFolder(appFolder);
		return appFolder;
	}

	public String guessBuildGradle() throws ProjectConfigurationException {
		File file = new File(this.getProjectFolder() + this.androidManifest);
		if (!file.exists())
			throw new ProjectConfigurationException("buildGradle", "build.gradle not found");
		file = file.getParentFile().getParentFile().getParentFile();
		file = new File(file.getAbsolutePath() + File.separatorChar + "build.gradle");
		if (!file.exists())
			throw new ProjectConfigurationException("buildGradle", "build.gradle not found");
		String path = file.getAbsolutePath().substring(this.getProjectFolder().length());
		this.projectConfiguration.setBuildGradle(path);
		return path;
	}
	
	public String guessSrcFolder() throws ProjectConfigurationException {
		File file = new File(this.getProjectFolder() + this.androidManifest);
		if (!file.exists())
			throw new ProjectConfigurationException("srcFolder", "src folder cannot be guessed");
		file = file.getParentFile().getParentFile();
		String srcFolder = file.getAbsolutePath();
		if (!srcFolder.endsWith(File.separator))
			srcFolder = srcFolder + File.separatorChar;
		srcFolder = srcFolder.substring(this.getProjectFolder().length());
		this.projectConfiguration.setSrcFolder(srcFolder);
		return srcFolder;
	}
	
	public String guessManifest() throws ProjectConfigurationException {
		File file = new File(this.getProjectFolder() + this.androidManifest);
		if (!file.exists())
			throw new ProjectConfigurationException("manifestPath", "AndroidManifest.xml not found");
		String path = file.getAbsolutePath().substring(this.getProjectFolder().length());
		this.projectConfiguration.setManifestPath(path);
		return path;
	}

	public String guessApplicationId() throws ProjectConfigurationException {
		File fAndroidManifest=new File(this.getProjectFolder() + androidManifest);
		if (!fAndroidManifest.exists())
			throw new ProjectConfigurationException("applicationId", "Cannot guess applicationId because AndroidManifest.xml has not been found");
		DocumentBuilderFactory dbf=DocumentBuilderFactory.newInstance();
		DocumentBuilder db;
		try {
			db = dbf.newDocumentBuilder();
			Document document = db.parse(fAndroidManifest);
			XPath xPath = XPathFactory.newInstance().newXPath();
			NodeList nodes = (NodeList) xPath.evaluate("/manifest", document.getDocumentElement(), XPathConstants.NODESET);
			Node atributo=nodes.item(0).getAttributes().getNamedItem("package");
			return atributo.getNodeValue();
		} catch (Exception e) {
			throw new ProjectConfigurationException("applicationId", "Error reading AndroidManifest.xml");
		}
	}
	
	public String guessTestRunner() throws ProjectConfigurationException {
		String testRunner = null;
		String path = this.getProjectFolder() + this.projectConfiguration.getBuildGradle();
		File file = new File(path);
		if (!file.exists())
			throw new ProjectConfigurationException("testRunner", "testRunner cannot be guessed");
		try (BufferedReader br = new BufferedReader(new FileReader(file))) {
			String line = br.readLine();
			while (line != null) {
				if (line.contains("testInstrumentationRunner")) {
					line = line.trim();
					line = line.substring("testInstrumentationRunner".length()).trim();
					line = line.substring(1, line.length() - 1).trim();
					testRunner = this.projectConfiguration.getApplicationId() + ".test/" + line;
					break;
				}
				line = br.readLine();
			}
		} catch (Exception e) {
			throw new ProjectConfigurationException("testRunner", "Error reading build.gradle file");
		}
		return testRunner;
	}
	
	public String guessDebugPackage() throws ProjectConfigurationException {
		return "/data/local/tmp/" + this.projectConfiguration.getApplicationId();
	}

	public String guessInstallAPKCommand() throws ProjectConfigurationException {
		String installCommand = "platform-tools/adb -s #DEVICE# shell pm install -t -r \"#PACKAGE_TO_INSTALL#\"";
		installCommand = installCommand.replace("#PACKAGE_TO_INSTALL#", "/data/local/tmp/" + this.projectConfiguration.getApplicationId());
		return installCommand;
	}
	
	public String guessDebugAPKPathAndName() throws ProjectConfigurationException {
		String path = this.getProjectFolder() + this.projectConfiguration.getAppFolder();
		File app = new File(path);
		if (!app.exists())
			throw new ProjectConfigurationException("debugAPKPathAndName", "app folder not found");
		String apkPath = guess(app.getAbsolutePath(), 
				"build" + File.separatorChar + "outputs" + File.separatorChar + "apk" + 
				File.separatorChar + "debug" + File.separatorChar + "app-debug.apk");
		if (apkPath == null)
			throw new ProjectConfigurationException("debugAPKPathAndName", apkPath + " not found");
		apkPath = apkPath.substring(this.getProjectFolder().length());
		this.projectConfiguration.setDebugAPKPathAndName(apkPath);
		return apkPath;
	}
	
	public String guessTestPackage() throws ProjectConfigurationException {
		return "/data/local/tmp/" + this.projectConfiguration.getApplicationId() + ".test";
	}
	
	public String guessInstallTestAPKCommand() throws ProjectConfigurationException {
		String installTestCommand = "platform-tools/adb -s #DEVICE# shell pm install -t -r \"#PACKAGE_TO_INSTALL#\"";
		installTestCommand = installTestCommand.replace("#PACKAGE_TO_INSTALL#", "/data/local/tmp/" + this.projectConfiguration.getApplicationId() + ".test");
		return installTestCommand;
	}
	
	public String guessTestAPKPathAndName() throws ProjectConfigurationException {
		String path = this.getProjectFolder() + this.projectConfiguration.getAppFolder();
		File app = new File(path);
		if (!app.exists())
			throw new ProjectConfigurationException("debugTestAPK", "app folder not found");
		String testAPKPath = guess(app.getAbsolutePath(), 
				"build" + File.separatorChar + "outputs" + File.separatorChar + "apk" + 
				File.separatorChar + "androidTest" + File.separatorChar + "debug" + 
				File.separatorChar +  "app-debug-androidTest.apk");
		if (testAPKPath == null)
			throw new ProjectConfigurationException("testAPKPathAndName", testAPKPath + " not found");
		testAPKPath = testAPKPath.substring(this.getProjectFolder().length());
		this.projectConfiguration.setTestAPKPathAndName(testAPKPath);
		return testAPKPath;
	}
	
	public String guessCompileSdkVersion() throws ProjectConfigurationException {
		String compileSDKVersion = null;
		String path = this.getProjectFolder() + this.projectConfiguration.getBuildGradle();
		File file = new File(path);
		if (!file.exists())
			throw new ProjectConfigurationException("compileSdkVersion", "compileSdkVersion cannot be guessed");
		try (BufferedReader br = new BufferedReader(new FileReader(file))) {
			String line = br.readLine();
			while (line != null) {
				if (line.contains("compileSdkVersion")) {
					line = line.trim();
					line = line.substring("compileSdkVersion".length()).trim();
					compileSDKVersion = line;
					break;
				}
				line = br.readLine();
			}
		} catch (Exception e) {
			throw new ProjectConfigurationException("testRunner", "Error reading build.gradle file");
		}
		return compileSDKVersion;		
	}
	
	private String guess(String folder, String subfolder) {
		String path = folder + File.separatorChar + subfolder;
		if (new File(path).exists())
			return path;
		return null;
	}
	
	public void setProjectConfiguration(Map<String, String> configuration) {
		Class<ProjectConfiguration> c = ProjectConfiguration.class;
		Iterator<String> keys = configuration.keySet().iterator();
		String key;
		Field field;
		while (keys.hasNext()) {
			key = keys.next();
			try {
				field = c.getDeclaredField(key);
				field.setAccessible(true);
				field.set(this.projectConfiguration, configuration.get(key));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public List<TestClass> getTestClasses() {
		return this.testClasses;
	}
	
	public void loadTestClasses() {
		this.testClasses = Manager.get().getTestClassDAO().getTestClasses(this.id);
	}
}
