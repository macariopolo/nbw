package edu.uclm.esi.newBacterioWeb.model;

public enum ExecutionOrder {
	SEQUENTIAL, BEST_TEST_CASE_FIRST
}
