package edu.uclm.esi.newBacterioWeb.model;

public enum TaskState {
	PREPARING, RUNNING, FINISHED;
}
