package edu.uclm.esi.newBacterioWeb.model;

import java.io.File;
import java.util.List;
import java.util.Vector;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

import org.json.JSONObject;
import org.springframework.web.socket.WebSocketSession;

import edu.uclm.esi.newBacterioWeb.model.devices.DeviceProxy;
import edu.uclm.esi.newBacterioWeb.so.IProcessExecutorListener;
import edu.uclm.esi.newBacterioWeb.web.ws.WSTestsExecution;

@Entity
public class RemoteHost implements IProcessExecutorListener {
	@Id
	private String ip;
	private Integer port;
	private String sdkPath;
	private String avdPath;
	private boolean active;
	@Transient
	private List<DeviceProxy> devices;
	@Transient
	private TestRunnerGroup testRunnerGroup;
	@Transient
	private WebSocketSession session;
	@Transient
	private Project project;
	@Transient
	private String command;
	
	public RemoteHost() {
		this.devices = new Vector<>();
	}
	
	public RemoteHost(String ip, int port) {
		this();
		this.ip = ip;
		this.port = port;
	}
	
	public void setActive(boolean active) {
		this.active = active;
	}
	
	public boolean isActive() {
		return active;
	}
	
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	
	public Integer getPort() {
		return port;
	}
	
	public void setPort(Integer port) {
		this.port = port;
	}
	
	public String getSdkPath() {
		return sdkPath;
	}

	public void setSdkPath(String sdkPath) {
		if (!sdkPath.endsWith(File.separator))
			sdkPath = sdkPath + File.separatorChar;
		this.sdkPath = sdkPath;
	}

	public String getAvdPath() {
		return avdPath;
	}

	public void setAvdPath(String avdPath) {
		if (!avdPath.endsWith(File.separator))
			avdPath = avdPath + File.separatorChar;
		this.avdPath = avdPath;
	}

	public void downloadProject(Project project, TestRunnerGroup testRunnerGroup, WebSocketSession session, String command) {
		this.project = project;
		this.testRunnerGroup = testRunnerGroup;
		this.session = session;
		this.setCommand(command);
		WSTestsExecution.send(session, "type", "MESSAGE", "message", "Downloading original project on " + this.ip);
		String url = "http://" + this.getIp() + ":" + this.getPort() + "/downloadProject?projectId=" + project.getId(); 
		Runnable runnable = HttpRequests.getRunnableGet(this, url, "projectDownloaded");
		new Thread(runnable).start();
	}
	
	@Override
	public void onProcessExecuted(JSONObject executionResult) {
		if (executionResult.getInt("returnCode")!=0) {
			this.testRunnerGroup.onProcessExecuted(executionResult);
			return;
		} 
		String type = executionResult.getString("type");
		switch (type) {
		case "projectDownloaded" :
			if (this.command.equals("runTests"))
				this.compile();
			else
				this.testRunnerGroup.onProcessExecuted(executionResult);
			break;
		case "originalAPKCompiled" :
			if (this.command.equals("runTests"))
				this.compileTests();
			else
				this.testRunnerGroup.onProcessExecuted(executionResult);
			break;
		case "testAPKCompiled" :
			if (this.command.equals("runTests")) {
				for (int i=0; i<this.devices.size(); i++) {
					DeviceProxy device = this.devices.get(i);
					device.setTestRunnerGroup(this.testRunnerGroup);
					device.setProject(this.project);
					device.setSession(this.session);
					device.push(false);
				}
			} else 
				this.testRunnerGroup.onProcessExecuted(executionResult);
			break;
		}
	}
	
	public void compile() {
		WSTestsExecution.send(session, "type", "MESSAGE", "message", "Compiling original project on " + this.ip + ":" + this.port);
		String url = "http://" + this.getIp() + ":" + this.getPort() + "/compile?projectId=" + project.getId();
		Runnable runnable = HttpRequests.getRunnableGet(this, url, "originalAPKCompiled");
		new Thread(runnable).start();
	}
	
	public void compileTests() {
		WSTestsExecution.send(session, "type", "MESSAGE", "message", "Compiling tests project on " + this.ip + ":" + this.port);		
		String url = "http://" + this.getIp() + ":" + this.getPort() + "/compileTest?projectId=" + project.getId();
		Runnable runnable = HttpRequests.getRunnableGet(this, url, "testAPKCompiled");
		new Thread(runnable).start();
	}

	@Override
	public String getDescription() {
		return this.ip + ":" + this.port;
	}

	public void addDevice(DeviceProxy device) {
		this.devices.add(device);
	}

	public void setProject(Project project) {
		this.project = project;
		for (DeviceProxy device : this.devices)
			device.setProject(project);
	}

	public void setTestRunnerGroup(TestRunnerGroup testRunnerGroup) {
		this.testRunnerGroup = testRunnerGroup;
		for (DeviceProxy device : this.devices)
			device.setTestRunnerGroup(testRunnerGroup);
	}

	public void setSession(WebSocketSession session) {
		this.session = session;
		for (DeviceProxy device : this.devices)
			device.setSession(session);
	}

	public void setCommand(String command) {
		this.command = command;
		for (DeviceProxy device : this.devices)
			device.setCommand(command);
	}

}
