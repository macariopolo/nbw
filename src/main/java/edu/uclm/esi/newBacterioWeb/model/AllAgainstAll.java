package edu.uclm.esi.newBacterioWeb.model;

import java.util.List;

public class AllAgainstAll extends RunStrategy {
	
	public AllAgainstAll(TestRunnerGroup testRunnerGroup) {
		super(testRunnerGroup);
	}

	@Override
	public TestMethod getNextTestMethod() {
		TestClass testClass = this.testClasses.get(this.testClassesCounter);
		List<TestMethod> testMethods = testClass.getTestMethods();

		TestMethod testMethod = null;
		if (this.testMethodsCounter < testMethods.size())
			testMethod = testMethods.get(this.testMethodsCounter++);
		else if (this.testClassesCounter+1<this.testClasses.size()) {
			testClass = this.testClasses.get(++this.testClassesCounter);
			this.testMethodsCounter = 0;
			testMethods = testClass.getTestMethods();
			testMethod = testMethods.get(this.testMethodsCounter++);
		}
		return testMethod;
	}

}
