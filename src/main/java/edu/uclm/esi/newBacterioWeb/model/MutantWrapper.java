package edu.uclm.esi.newBacterioWeb.model;

import java.util.List;

import org.json.JSONObject;

import edu.uclm.esi.newBacterioWeb.model.devices.DeviceProxy;
import edu.uclm.esi.newBacterioWeb.model.killingMatrix.KillingMatrixCell;
import edu.uclm.esi.newBacterioWeb.model.killingMatrix.KillingMatrixCellValue;

public abstract class MutantWrapper {
	
	protected String id;
	protected Integer index;
	protected String classUnderTest;
	protected Integer killings;
	protected String deviceName;
	protected String exception;
	private KillingMatrixCell[] testResults;
	private int numberOfKillings;

	public MutantWrapper(String id, int index, String classUnderTest, String deviceDescription) {
		this.id = id;
		this.index = index;
		this.classUnderTest = classUnderTest;
		this.deviceName = deviceDescription;
		this.killings = 0;
	}

	public MutantWrapper(Object[] o) {
		this.id = o[0].toString();
		this.index = (int) o[1];
		this.classUnderTest = o[2].toString();
		this.killings = 0;
	}

	public MutantWrapper(JSONObject jsoMutantWrapper) {
		this.id = jsoMutantWrapper.getString("id");
		this.index = jsoMutantWrapper.optInt("index");
		this.classUnderTest = jsoMutantWrapper.optString("classUnderTest");
		this.killings = jsoMutantWrapper.optInt("killings");
		this.deviceName = jsoMutantWrapper.optString("device");
	}

	public String getId() {
		return id;
	}
	
	public Integer getIndex() {
		return index;
	}
	
	public String getClassUnderTest() {
		return classUnderTest;
	}
	
	public String getDeviceName() {
		return deviceName;
	}

	public void increaseKillings() {
		this.killings++;
	}
	
	public Integer getKillings() {
		return killings;
	}

	public JSONObject toJSON() {
		JSONObject jso = new JSONObject().
			put("id", this.id).
			put("index", this.index).
			put("classUnderTest", this.classUnderTest).
			put("killings", this.killings);
		if (this.deviceName!=null)
			jso.put("device", this.deviceName);
		return jso;
	}

	public void setDevice(DeviceProxy device) {
		this.deviceName = device.getDescription();
	}

	public void setNonCompilable(String msg) {
		this.exception = msg;
	}
	
	public boolean isNonCompilable() {
		return this.exception!=null;
	}

	public void setTestResults(List<String> testMethods) {
		this.testResults = new KillingMatrixCell[testMethods.size()];
		for (int i=0; i<this.testResults.length; i++)
			this.testResults[i] = new KillingMatrixCell();
	}

	public KillingMatrixCell setTestResult(int testMethodIndex, KillingMatrixCellValue state) {
		this.testResults[testMethodIndex].setState(state);
		if (state == KillingMatrixCellValue.KILLED)
			this.numberOfKillings++;
		return this.testResults[testMethodIndex];
	}
	
	public int getNumberOfKillings() {
		return numberOfKillings;
	}
}
