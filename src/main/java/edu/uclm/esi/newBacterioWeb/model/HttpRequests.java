package edu.uclm.esi.newBacterioWeb.model;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONArray;
import org.json.JSONObject;

import edu.uclm.esi.newBacterioWeb.so.IProcessExecutorListener;
import edu.uclm.esi.newBacterioWeb.web.controllers.StringResponseHandler;

public class HttpRequests {
	public static Runnable getRunnableGet(IProcessExecutorListener listener, String url, String type) {
		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				HttpClientBuilder builder = HttpClientBuilder.create();
				CloseableHttpClient client = builder.build();
				HttpGet request = new HttpGet(url);
				try {
					try {
						StringResponseHandler responseHandler = new StringResponseHandler();
						JSONObject executionResult = new JSONObject(client.execute(request, responseHandler));
						executionResult.put("device", listener.getDescription());
						listener.onProcessExecuted(executionResult);
					} finally {
						client.close();
					}
				} catch (Exception e) {
					listener.onProcessExecuted(errorMessage(type, e));
				}
			}
		};
		return runnable;
	}
	
	public static Runnable getRunnablePost(IProcessExecutorListener listener, String url, String type, JSONObject payload) {
		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				HttpClientBuilder builder = HttpClientBuilder.create();
				CloseableHttpClient client = builder.build();
				HttpPost request = new HttpPost(url);
				try {
					StringEntity requestEntity = new StringEntity(payload.toString(), ContentType.APPLICATION_JSON);
					request.setEntity(requestEntity);
					try {
						StringResponseHandler responseHandler = new StringResponseHandler();
						JSONObject executionResult = new JSONObject(client.execute(request, responseHandler));
						executionResult.put("device", listener.getDescription());
						listener.onProcessExecuted(executionResult);
					} finally {
						client.close();
					}
				} catch (Exception e) {
					listener.onProcessExecuted(errorMessage(type, e));
				}
			}
		};
		return runnable;
	}
	
	private static JSONObject errorMessage(String type, Exception e) {
		return new JSONObject().put("returnCode", -1).
				put("type", type).
				put("outputLines", new JSONArray()).
				put("errorLines", new JSONArray().put(e.getMessage()));
	}
}
