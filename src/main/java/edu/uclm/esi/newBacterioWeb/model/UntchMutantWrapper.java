package edu.uclm.esi.newBacterioWeb.model;

import org.json.JSONObject;

public class UntchMutantWrapper extends MutantWrapper {

	public UntchMutantWrapper(Object[] o) {
		super(o[0].toString(), (int) o[1], o[2].toString(), null);
	}

	public UntchMutantWrapper(JSONObject jsoMutantWrapper) {
		super(jsoMutantWrapper);
	}
	
}
