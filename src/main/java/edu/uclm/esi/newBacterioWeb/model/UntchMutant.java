package edu.uclm.esi.newBacterioWeb.model;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class UntchMutant implements IMutant {
	@Id
	private String id;
	@ManyToOne
	@JsonIgnore
	private ProjectFile originalFile;
	@Column(columnDefinition = "LONGTEXT")
	@Lob
	@JsonIgnore
	private String code;
	private int mutantIndexFrom;
	private int mutantIndexTo;
	
	public UntchMutant() {
		this.id = UUID.randomUUID().toString();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public ProjectFile getOriginalFile() {
		return originalFile;
	}

	public void setOriginalFile(ProjectFile originalFile) {
		this.originalFile = originalFile;
	}

	@JsonIgnore
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setIndexFrom(int mutantIndexFrom) {
		this.mutantIndexFrom = mutantIndexFrom;
	}

	public int getMutantIndexFrom() {
		return mutantIndexFrom;
	}
	
	public void setIndexTo(int mutantIndexTo) {
		this.mutantIndexTo = mutantIndexTo;
	}

	public int getMutantIndexTo() {
		return mutantIndexTo;
	}
}
