package edu.uclm.esi.newBacterioWeb.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.json.JSONObject;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.PackageDeclaration;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.printer.PrettyPrinterConfiguration;

import edu.uclm.esi.newBacterioWeb.web.controllers.Manager;
import edu.uclm.esi.newBacterioWeb.web.ws.WSServerMDroidImporter;

public class MDroidImporter {

	private WebSocketSession session;
	private Project project;
	private BufferedReader linesReader;
	private String originalLogFileName;

	public MDroidImporter(WebSocketSession session, Project project, String fileName, byte[] logFileContents) throws FileNotFoundException, IOException {
		this.session = session;
		this.project = project;
		this.originalLogFileName = fileName.substring(0, fileName.indexOf("LOG_FILE"));;
		String logFileName = Manager.get().getIoPath() + this.originalLogFileName + ".txt";
		try(FileOutputStream fos = new FileOutputStream(logFileName)) {
			fos.write(logFileContents);
		}
		this.linesReader = new BufferedReader(new FileReader(logFileName));
	}

	public void askForNewMutant() throws IOException {
		String line = this.linesReader.readLine();
		if (line==null)
			return;

		int mutantLength = "Mutant ".length();
		int posMutant = line.indexOf("Mutant ");
		String srcPath = project.getProjectFolder();
		int srPathLength = srcPath.length();
		 
		if (posMutant!=-1) {
			line = line.substring(posMutant + mutantLength).trim();
			int posDosPuntos = line.indexOf(':');
			int mutantIndex = Integer.parseInt(line.substring(0, posDosPuntos));
			int posPyC = line.indexOf(';', posDosPuntos);
			int posInLine = line.indexOf(" in line ", posPyC);
			String fileName = line.substring(posDosPuntos+1, posPyC).trim();
			fileName = fileName.substring(srPathLength);
			String operator = line.substring(posPyC+1, posInLine).trim();
			
			JSONObject jso = new JSONObject().put("type", "GIVE_ME_OTHER").
					put("logFileName", this.originalLogFileName).
					put("mutantFileName", fileName).
					put("operator", operator);
			
			session.sendMessage(new TextMessage(jso.toString()));
			Integer nom = Manager.get().getClassicMutantDAO().findMaxIndexByProjectId(this.project.getId());
			if (nom==null)
				nom = 1;
			try {
				//saveMutant(logFileName, mutantIndex, nom, fileName, operator);
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}
	}

	private void saveMutant(String logFileName, int oldMutantIndex, int newMutantIndex, String fileName, String operator) throws FileNotFoundException, IOException {
		String folder = logFileName + oldMutantIndex + "/";
		fileName = fileName.substring(fileName.indexOf("main"));
		WSServerMDroidImporter.send(session, "type", "MESSAGE", "message", "Looking for a new mutant");
		File mutantFile = new File(folder + fileName);
		StaticJavaParser.getConfiguration().setSymbolResolver(project.getJavaSymbolSolver());
		try(FileInputStream fis = new FileInputStream(mutantFile)) {
			byte[] bytes = new byte[fis.available()];
			fis.read(bytes);
			String code = new String(bytes);
			CompilationUnit cu=StaticJavaParser.parse(code);
			Optional<PackageDeclaration> optPackageDeclaration = cu.getPackageDeclaration();
			if (optPackageDeclaration.isPresent()) {
				PackageDeclaration packageDeclaration = optPackageDeclaration.get();
				fileName = packageDeclaration.getNameAsString();
			}
			fileName=fileName + "/";
			List<ClassOrInterfaceDeclaration> classes = cu.findAll(ClassOrInterfaceDeclaration.class);
			if (!classes.isEmpty()) {
				ClassOrInterfaceDeclaration clazz = classes.get(0);
				fileName=fileName + clazz.getNameAsString();
				fileName = fileName.replace('.', '/') + ".java";
				fileName = project.getProjectConfiguration().getSrcFolder() + "main/java/" + fileName;
				WSServerMDroidImporter.send(session, "type", "MESSAGE", "message", "Processing a mutant of " + fileName);
				ProjectFile originalFile = Manager.get().getProjectFileDAO().findByFileName(project.getId(), fileName);
				if (originalFile!=null) {
					ClassicMutant mutant = new ClassicMutant();
					mutant.setCode(cu.toString(new PrettyPrinterConfiguration()));
					mutant.setIndex(newMutantIndex+1);
					mutant.setOperator(operator);
					mutant.setOriginalFile(originalFile);
					Manager.get().getClassicMutantDAO().save(mutant);
				}
			}
		}
	}
}
