package edu.uclm.esi.newBacterioWeb.model;

import java.util.List;

import edu.uclm.esi.newBacterioWeb.model.killingMatrix.KillingMatrixCellValue;

public abstract class RunStrategy {
	
	protected boolean lastMutantKilled;
	protected int testClassesCounter;
	protected int testMethodsCounter;	
	protected List<TestClass> testClasses;

	public RunStrategy(TestRunnerGroup testRunnerGroup) {
		this.testClasses = testRunnerGroup.getTestClasses();
	}
	
	public abstract TestMethod getNextTestMethod();

	public void setKilled(KillingMatrixCellValue killed) {
		this.lastMutantKilled = killed == KillingMatrixCellValue.KILLED;
	}

	public void reset() {
		this.lastMutantKilled = false;
		this.testClassesCounter = 0;
		this.testMethodsCounter = 0;
	}
}
