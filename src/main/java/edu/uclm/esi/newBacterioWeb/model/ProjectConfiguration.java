package edu.uclm.esi.newBacterioWeb.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class ProjectConfiguration {
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column
	private String gradlewCommand;
	@Column
	private String localProperties;
	@Column
	private String appFolder;
	@Column
	private String buildGradle;
	@Column
	private String srcFolder;
	@Column
	private String manifestPath;
	@Column
	private String applicationId;
	@Column
	private String testRunner;
	@Column
	private String buildAPKCommand;
	@Column
	private String installAPKCommand;
	@Column
	private String debugAPKPathAndName;
	@Column
	private String debugPackage;
	@Column
	private String buildTestAPKCommand;
	@Column
	private String installTestAPKCommand;
	@Column
	private String testAPKPathAndName;
	@Column
	private String testPackage;
	@Column
	private String unitTestCommand;
	@Column
	private String compileSdkVersion;
	@ElementCollection(fetch = FetchType.EAGER)
	private List<String> libraries;
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn
	@JsonIgnore
	private Project project; 
	
	public ProjectConfiguration() {
		this.libraries = new ArrayList<>();
	}
	
	public void addLibrary(String library) {
		this.libraries.add(library);
	}
	
	public void removeLibrary(String library) {
		this.libraries.remove(library);
	}
	
	public void setLibraries(List<String> libraries) {
		this.libraries = libraries;
	}
	
	public List<String> getLibraries() {
		return libraries;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getGradlewCommand() {
		return gradlewCommand;
	}

	public void setGradlewCommand(String gradlewCommand) {
		this.gradlewCommand = gradlewCommand;
	}
	
	public String getLocalProperties() {
		return localProperties;
	}

	public void setLocalProperties(String localProperties) {
		this.localProperties = localProperties;
	}

	public String getAppFolder() {
		return appFolder;
	}

	public void setAppFolder(String appFolder) {
		this.appFolder = appFolder;
	}

	public String getBuildGradle() {
		return buildGradle;
	}

	public void setBuildGradle(String buildGradle) {
		this.buildGradle = buildGradle;
	}

	public String getSrcFolder() {
		return srcFolder;
	}

	public void setSrcFolder(String srcFolder) {
		this.srcFolder = srcFolder;
	}

	public String getManifestPath() {
		return manifestPath;
	}

	public void setManifestPath(String manifestPath) {
		this.manifestPath = manifestPath;
	}

	public String getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public String getTestRunner() {
		return testRunner;
	}

	public void setTestRunner(String testRunner) {
		this.testRunner = testRunner;
	}

	public String getBuildAPKCommand() {
		return buildAPKCommand;
	}

	public void setBuildAPKCommand(String buildAPKCommand) {
		this.buildAPKCommand = buildAPKCommand;
	}

	public String getInstallAPKCommand() {
		return installAPKCommand;
	}

	public void setInstallAPKCommand(String installAPKCommand) {
		this.installAPKCommand = installAPKCommand;
	}

	public String getDebugAPKPathAndName() {
		return debugAPKPathAndName;
	}

	public void setDebugAPKPathAndName(String debugAPKPathAndName) {
		this.debugAPKPathAndName = debugAPKPathAndName;
	}

	public String getDebugPackage() {
		return debugPackage;
	}

	public void setDebugPackage(String debugPackage) {
		this.debugPackage = debugPackage;
	}

	public String getBuildTestAPKCommand() {
		return buildTestAPKCommand;
	}

	public void setBuildTestAPKCommand(String buildTestAPKCommand) {
		this.buildTestAPKCommand = buildTestAPKCommand;
	}

	public String getInstallTestAPKCommand() {
		return installTestAPKCommand;
	}

	public void setInstallTestAPKCommand(String installTestAPKCommand) {
		this.installTestAPKCommand = installTestAPKCommand;
	}

	public String getTestAPKPathAndName() {
		return testAPKPathAndName;
	}
	
	public void setTestAPKPathAndName(String testAPKPathAndName) {
		this.testAPKPathAndName = testAPKPathAndName;
	}
	
	public String getTestPackage() {
		return testPackage;
	}

	public void setTestPackage(String testPackage) {
		this.testPackage = testPackage;
	}

	public String getUnitTestCommand() {
		return unitTestCommand;
	}

	public void setUnitTestCommand(String unitTestCommand) {
		this.unitTestCommand = unitTestCommand;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public void setCompileSdkVersion(String compileSdkVersion) {
		this.compileSdkVersion = compileSdkVersion;
	}
	
	public String getCompileSdkVersion() {
		return compileSdkVersion;
	}
}
