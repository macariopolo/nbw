package edu.uclm.esi.newBacterioWeb.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;

@Entity
public class TestClass {
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column
	private String name;
	@OneToOne
	@JoinColumn
	@JsonIgnore
	private ProjectFile projectFile;
	@Column
	private boolean checked;
	@Column
	private String testType;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "testClass", fetch = FetchType.EAGER)
	private List<TestMethod> testMethods;
	
	public TestClass() {
		this.testMethods = new ArrayList<>();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public ProjectFile getProjectFile() {
		return projectFile;
	}

	public void setProjectFile(ProjectFile projectFile) {
		this.projectFile = projectFile;
	}

	public String getTestType() {
		return testType;
	}

	public void setTestType(String testType) {
		this.testType = testType;
	}

	public List<TestMethod> getTestMethods() {
		return testMethods;
	}

	public void setTestMethods(List<TestMethod> testMethods) {
		this.testMethods = testMethods;
		for (TestMethod testMethod : testMethods)
			testMethod.setTestClass(this);
	}
	
	public boolean isChecked() {
		return checked;
	}
	
	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public void addTestMethods(ClassOrInterfaceDeclaration clazz, TestClassType testClassType) {
		List<MethodDeclaration> methods;
		String methodName;
		if (testClassType == TestClassType.JUNIT_FRAMEWORK_TEST_CASE || testClassType == TestClassType.INSTRUMENTATION_TEST_CASE || testClassType == TestClassType.ANDROID_TEST_CASE) {
			methods = clazz.getMethods();
			for (MethodDeclaration method : methods) {
				methodName = method.getNameAsString();
				if (methodName.startsWith("test")) {
					TestMethod testMethod = new TestMethod(clazz.getNameAsString() + "#" + methodName, true);
					testMethod.setTestClass(this);
					testMethods.add(testMethod);
				}
			}
		} else if (testClassType == TestClassType.RUN_WITH || testClassType == TestClassType.OTHER) {
			methods = clazz.getMethods();
			for (MethodDeclaration method : methods) {
				if (method.isAnnotationPresent("Test")) {
					methodName = method.getNameAsString();
					TestMethod testMethod = new TestMethod(methodName, true);
					testMethod.setTestClass(this);
					testMethods.add(testMethod);
				}
			}
		}
	}	
	
}
