package edu.uclm.esi.newBacterioWeb.model;

public interface Task {

	String getId();
	User getUser();
	TaskInfo getInfo();
	TaskState getState();
	void stop();
}
