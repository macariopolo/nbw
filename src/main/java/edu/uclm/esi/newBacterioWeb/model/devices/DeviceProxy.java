package edu.uclm.esi.newBacterioWeb.model.devices;

import java.util.List;
import java.util.Vector;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.web.socket.WebSocketSession;

import edu.uclm.esi.newBacterioWeb.model.ClassicMutantWrapper;
import edu.uclm.esi.newBacterioWeb.model.ExecutionLine;
import edu.uclm.esi.newBacterioWeb.model.ExecutionOrder;
import edu.uclm.esi.newBacterioWeb.model.HttpRequests;
import edu.uclm.esi.newBacterioWeb.model.MutantWrapper;
import edu.uclm.esi.newBacterioWeb.model.Project;
import edu.uclm.esi.newBacterioWeb.model.RemoteHost;
import edu.uclm.esi.newBacterioWeb.model.RunStrategy;
import edu.uclm.esi.newBacterioWeb.model.TestClass;
import edu.uclm.esi.newBacterioWeb.model.TestMethod;
import edu.uclm.esi.newBacterioWeb.model.TestRunnerGroup;
import edu.uclm.esi.newBacterioWeb.model.killingMatrix.KillingMatrix;
import edu.uclm.esi.newBacterioWeb.model.killingMatrix.KillingMatrixCell;
import edu.uclm.esi.newBacterioWeb.model.killingMatrix.KillingMatrixCellValue;
import edu.uclm.esi.newBacterioWeb.so.IProcessExecutorListener;
import edu.uclm.esi.newBacterioWeb.web.ws.WSTestsExecution;

public class DeviceProxy implements IProcessExecutorListener {
	private String ip;
	private int port;
	private String name;
	private boolean busy;
	private Project project;
	private TestRunnerGroup testRunnerGroup;
	private boolean readyForRunning;
	private int mutantWrapperCounter;
	private List<MutantWrapper> mutantWrappers;
	private MutantWrapper currentMutantWrapper;
	private WebSocketSession session;
	private RunStrategy runStrategy;
	private String command;
	
	public DeviceProxy(String ip, int port, String name) {
		this.ip = ip;
		this.port = port;
		this.name = name;
		this.mutantWrappers = new Vector<>();
	}
	
	public RemoteHost getRemoteHost() {
		return new RemoteHost(this.ip, this.port);
	}

	public void askForAMutant() {
		this.runStrategy.reset();
		this.currentMutantWrapper = this.testRunnerGroup.getNextMutantWrapper();
		if (this.currentMutantWrapper!=null) {
			this.testRunnerGroup.getKillingMatrix().addColumn(this, this.currentMutantWrapper, true);
			this.currentMutantWrapper.setDevice(this);
			String url = "http://" + this.getIp() + ":" + this.getPort() + "/downloadAndInstallNextMutant";
			JSONObject payload = new JSONObject();
			payload.put("projectId", project.getId()).
				put("mutantWrapper", this.currentMutantWrapper.toJSON()).
				put("deviceName", this.name);
			WSTestsExecution.send(session, "type", "PROGRESS", "state", "DOWNLOADING", "deviceName", this.getDescription(), "mutantIndex", this.currentMutantWrapper.getIndex());
			Runnable runnable = HttpRequests.getRunnablePost(this, url, "mutantInstalled", payload);
			new Thread(runnable).start();
		} else {
			WSTestsExecution.send(session, "type", "PROGRESS", "state", "FINISHED", "deviceName", this.getDescription());
			this.testRunnerGroup.deviceHasFinished(this);
		}
	}
	
	public void downloadAndInstallNextMutant() {
		this.runStrategy.reset();
		if (this.mutantWrapperCounter<this.mutantWrappers.size()) 
			this.currentMutantWrapper = this.mutantWrappers.get(mutantWrapperCounter++);
		else
			this.currentMutantWrapper = null;
		
		if (this.currentMutantWrapper!=null) {
			String url = "http://" + this.getIp() + ":" + this.getPort() + "/downloadAndInstallNextMutant";
			JSONObject payload = new JSONObject();
			payload.put("projectId", project.getId()).
				put("mutantWrapper", this.currentMutantWrapper.toJSON()).
				put("deviceName", this.name);
			WSTestsExecution.send(session, "type", "PROGRESS", "state", "DOWNLOADING", "deviceName", this.getDescription(), "mutantIndex", this.currentMutantWrapper.getIndex());
			Runnable runnable = HttpRequests.getRunnablePost(this, url, "mutantInstalled", payload);
			new Thread(runnable).start();
		} else {
			WSTestsExecution.send(session, "type", "PROGRESS", "state", "FINISHED", "deviceName", this.getDescription());
			this.testRunnerGroup.deviceHasFinished(this);
		}
	}

	public void runNextTest() {
		TestMethod testMethod = this.runStrategy.getNextTestMethod();
		if (testMethod!=null)
			runTest(testMethod);
		else if (!this.testRunnerGroup.getDistributionAlgorithm().equals("MUTANTS_ON_DEMAND"))
			this.downloadAndInstallNextMutant();
		else
			this.askForAMutant();
	}

	private void runTest(TestMethod testMethod) {
		TestClass testClass = testMethod.getTestClass();
		String url = "http://" + this.getIp() + ":" + this.getPort() + "/runTestMethod";
		JSONObject payload = new JSONObject().
				put("type", "runTestMethod").
				put("projectId", project.getId()).
				put("deviceName", this.name).
				put("testClassName", testClass.getName()).
				put("testMethodName", testMethod.getName());
		if (this.currentMutantWrapper!=null)
			payload.put("mutantWrapper", this.currentMutantWrapper.toJSON());
		Runnable runnable = HttpRequests.getRunnablePost(this, url, "testResult", payload);
		new Thread(runnable).start();
		WSTestsExecution.send(session, "type", "PROGRESS", "state", "RUNNING", "deviceName", this.getDescription(), 
				"testMethod", testClass.getName() + "#" + testMethod.getName(), 
				"mutantIndex", this.currentMutantWrapper!=null ? this.currentMutantWrapper.getIndex() : "original");
	}
	
	@Override
	public void onProcessExecuted(JSONObject executionResult) {
		String type = executionResult.getString("type");
		switch (type) {
		case "APKPushed" :
			if ((executionResult.getInt("returnCode")!=0) || !this.command.equals("runTests"))
				this.testRunnerGroup.onProcessExecuted(executionResult);
			else
				this.push(true);
			break;
		case "testAPKPushed" :
			if ((executionResult.getInt("returnCode")!=0) || !this.command.equals("runTests"))
				this.testRunnerGroup.onProcessExecuted(executionResult);
			else			
				this.install(false);
			break;
		case "APKInstalled" :
			if ((executionResult.getInt("returnCode")!=0) || !this.command.equals("runTests"))
				this.testRunnerGroup.onProcessExecuted(executionResult);
			else
				this.install(true);
			break;
		case "testAPKInstalled" :
			this.readyForRunning = true;
			WSTestsExecution.send(session, "type", "PROGRESS", "state", "READY", "deviceName", this.getDescription());
			this.testRunnerGroup.onProcessExecuted(executionResult);
			break;
		case "testResult" :
			this.setResult(executionResult, this.testRunnerGroup.getExecutionOrder());
			runNextTest();
			break;
		case "mutantInstalled" :
			if (executionResult.getInt("returnCode")!=0) {
				this.setResult(executionResult, this.testRunnerGroup.getExecutionOrder());
				if (!this.testRunnerGroup.getDistributionAlgorithm().equals("MUTANTS_ON_DEMAND"))
					this.downloadAndInstallNextMutant();
				else
					this.askForAMutant();
			} else {
				runNextTest();
			}
			break;
		case "testsStopped" :
			this.readyForRunning = false;
			break;
		}
	}
	
	public void setResult(JSONObject executionResult, ExecutionOrder executionOrder) {
		int mutantIndex = 0;
		MutantWrapper mutantWrapper = null;
		if (executionResult.getString("type").equals("mutantInstalled")) {
			mutantWrapper = new ClassicMutantWrapper(executionResult.optJSONObject("mutantWrapper"));
			this.testRunnerGroup.getKillingMatrix().markAsNonCompilable(mutantWrapper, "Non compilable", this);
			WSTestsExecution.send(session, "type", "NON_COMPILABLE", "mutantIndex", mutantWrapper.getIndex());
		} else {
			String testMethodName = executionResult.optString("testMethodName");
			if (testMethodName.length()==0)
				return;
			if (executionResult.optJSONObject("mutantWrapper")!=null)
				mutantWrapper = new ClassicMutantWrapper(executionResult.optJSONObject("mutantWrapper"));
			String classUnderTest = "Original system (no mutant)";
			if (mutantWrapper!=null) {
				mutantIndex = mutantWrapper.getIndex();
				classUnderTest = mutantWrapper.getClassUnderTest();
			}

			JSONArray files = executionResult.getJSONArray("files");
			ExecutionLine executionLine = buildExecutionLine(testRunnerGroup.getId(), testMethodName, project.getName(),
					this.getClass().getSimpleName(), ExecutionOrder.SEQUENTIAL, mutantIndex, classUnderTest, KillingMatrixCellValue.ALIVE, "androidTest", this.name);
			KillingMatrixCellValue killed = setVerdict(this.testRunnerGroup.getKillingMatrix(), testMethodName, mutantWrapper, executionResult, files);
			executionLine.finish(killed);
			this.runStrategy.setKilled(killed);	
		}	
	}
	
	public KillingMatrixCell setResult(int testMethodIndex, KillingMatrixCellValue state) {
		return this.currentMutantWrapper.setTestResult(testMethodIndex, state);
	}

	private ExecutionLine buildExecutionLine(String executionId, String testMethod, String projectName, String executionAgorithm,
			ExecutionOrder executionOrder, int mutantIndex, String classUnderTest, KillingMatrixCellValue killed, String testType, String deviceName) {
		ExecutionLine line = new ExecutionLine();
		line.setExecutionId(executionId);
		line.setTestCase(testMethod);
		line.setProjectName(projectName);
		line.setAlgorithm(executionAgorithm);
		line.setExecutionOrder(executionOrder);
		line.setMutantIndex(mutantIndex);
		line.setClassUnderTest(classUnderTest);
		line.setKilled(killed);
		line.setTestType(testType);
		line.setDevice(deviceName);
		return line;
	}

	private KillingMatrixCellValue setVerdict(KillingMatrix killingMatrix, String methodName, MutantWrapper mutantWrapper, JSONObject executionResult, JSONArray files) {
		JSONArray outputLines = executionResult.getJSONArray("outputLines");
		KillingMatrixCellValue killed = KillingMatrixCellValue.ALIVE;
		
		int posResult=findLine("INSTRUMENTATION_STATUS_CODE: -1", outputLines);
		if (posResult!=-1)
			killed = KillingMatrixCellValue.KILLED;
		else {
			 posResult=findLine("INSTRUMENTATION_STATUS_CODE: -2", outputLines);
			 if (posResult!=-1)
				killed = KillingMatrixCellValue.KILLED;
		}
		if (posResult==-1) {
			posResult=findLine("INSTRUMENTATION_STATUS_CODE: -4", outputLines);
			if (posResult!=-1)
				killed = KillingMatrixCellValue.IGNORED;
		}
		
		KillingMatrixCell cell;
		if (killed==KillingMatrixCellValue.ALIVE)
			cell = killingMatrix.set(methodName, mutantWrapper, killed, this);
		else
			cell = killingMatrix.set(methodName, mutantWrapper, killed, this, files.getString(0));
		int mutantIndex = mutantWrapper==null ? 0 : mutantWrapper.getIndex(); 
		WebSocketSession session = testRunnerGroup.getSession();
		WSTestsExecution.send(session, "type", "EXECUTION_RESULT", "methodName", methodName, "mutantIndex", mutantIndex, "cell", cell.toJSON());
		return killed;
	}
	
	private int findLine(String pattern, JSONArray lines) {
		for (int i=0; i<lines.length(); i++)
			if (lines.getString(i).contains(pattern))
				return i;
		return -1;
	}
	
	public void push(boolean testAPK) {
		WSTestsExecution.send(session, "type", "PROGRESS", "state", testAPK ? "PUSHING TEST APK" : "PUSHING APK", "deviceName", this.getDescription());
		String url = "http://" + this.getIp() + ":" + this.getPort() + "/push";
		JSONObject payload = new JSONObject();
		payload.put("projectId", project.getId()).
			put("testAPK", testAPK).
			put("deviceName", this.name);
		Runnable runnable = HttpRequests.getRunnablePost(this, url, testAPK ? "testAPKPushed" : "APKPushed", payload);
		new Thread(runnable).start();
	}
	
	public void install(boolean testAPK) {
		WSTestsExecution.send(session, "type", "PROGRESS", "state", testAPK ? "INSTALLING TEST APK" : "INSTALLING APK", "deviceName", this.getDescription());
		String url = "http://" + this.getIp() + ":" + this.getPort() + "/install";
		JSONObject payload = new JSONObject();
		payload.put("projectId", project.getId()).
			put("testAPK", testAPK).
			put("deviceName", this.name);
		Runnable runnable = HttpRequests.getRunnablePost(this, url, testAPK ? "testAPKInstalled" : "APKInstalled", payload);
		new Thread(runnable).start();
	}
	
	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isBusy() {
		return this.busy;
	}

	public String getDescription() {
		return this.ip + ":" + this.port + "/" + this.name;
	}

	public void setBusy(boolean busy) {
		this.busy = busy;
	}

	public void pushCurrentMutantFile(Project project, int index, Object object) {
		// TODO Auto-generated method stub
		
	}

	public JSONObject toJSON() {
		return new JSONObject().put("name", this.name).put("ip", this.ip).put("port", this.port);
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public void setTestRunnerGroup(TestRunnerGroup testRunnerGroup) {
		this.testRunnerGroup = testRunnerGroup;
	}

	public void stop() {
		WSTestsExecution.send(session, "type", "MESSAGE", "message", "Stopping execution on " + this.getDescription());
		String url = "http://" + this.getIp() + ":" + this.getPort() + "/stopTests";
		JSONObject payload = new JSONObject();
		payload.put("projectId", project.getId()).
			put("deviceName", this.name).
			put("packageName", this.project.getProjectConfiguration().getDebugPackage());
		Runnable runnable = HttpRequests.getRunnablePost(this, url, "testsStopped", payload);
		new Thread(runnable).start();
	}
	
	public void setReadyForRunning(boolean readyForRunning) {
		this.readyForRunning = readyForRunning;
	}

	public boolean isReadyForRunning() {
		return this.readyForRunning;
	}

	public void addMutant(MutantWrapper mutantWrapper, boolean notifyToClient) {
		this.mutantWrappers.add(mutantWrapper);
		mutantWrapper.setDevice(this);
		if (notifyToClient)
			WSTestsExecution.send(session, "type", "MUTANT_ADDED", "deviceName", this.getDescription(), "mutantIndex", mutantWrapper.getIndex());
	}

	public void setSession(WebSocketSession session) {
		this.session = session;
	}

	public TestRunnerGroup getTestRunnerGroup() {
		return this.testRunnerGroup;
	}

	public void setRunStrategy(RunStrategy runStrategy) {
		this.runStrategy = runStrategy;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public List<MutantWrapper> getMutants() {
		return this.mutantWrappers;
	}

	public void removeMutants() {
		this.mutantWrappers.clear();
	}
}
