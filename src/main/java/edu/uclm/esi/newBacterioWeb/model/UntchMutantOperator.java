package edu.uclm.esi.newBacterioWeb.model;

import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class UntchMutantOperator {
	@Id
	private String id;
	private String projectId;
	private String originalFileId;
	private String mutantId;
	private int mutantIndex;
	private String operator;
	
	public UntchMutantOperator() {
		this.id = UUID.randomUUID().toString();
	}

	public UntchMutantOperator(String projectId, String originalFileId, String mutantId, int mutantIndex, String operator) {
		this();
		this.projectId = projectId;
		this.originalFileId = originalFileId;
		this.mutantId = mutantId;
		this.mutantIndex = mutantIndex;
		this.operator = operator;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getProjectId() {
		return projectId;
	}
	
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}
	
	public String getOriginalFileId() {
		return originalFileId;
	}
	
	public void setOriginalFileId(String originalFileId) {
		this.originalFileId = originalFileId;
	}
	
	public String getMutantId() {
		return mutantId;
	}
	
	public void setMutantId(String mutantId) {
		this.mutantId = mutantId;
	}
	
	public int getMutantIndex() {
		return mutantIndex;
	}

	public void setMutantIndex(int mutantIndex) {
		this.mutantIndex = mutantIndex;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}
}
