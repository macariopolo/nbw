package edu.uclm.esi.newBacterioWeb.model;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.Vector;
import java.util.stream.Collectors;

import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.web.socket.WebSocketSession;

import com.fasterxml.jackson.annotation.JsonIgnore;

import edu.uclm.esi.newBacterioWeb.model.devices.DeviceProxy;
import edu.uclm.esi.newBacterioWeb.model.killingMatrix.KillingMatrix;
import edu.uclm.esi.newBacterioWeb.so.IProcessExecutorListener;
import edu.uclm.esi.newBacterioWeb.web.controllers.Manager;
import edu.uclm.esi.newBacterioWeb.web.exceptions.ProcessException;
import edu.uclm.esi.newBacterioWeb.web.ws.WSTestsExecution;

public class TestRunnerGroup implements Runnable, Task, IProcessExecutorListener {
	private String id;
	@JsonIgnore
	private Project project;
	@JsonIgnore
	private WebSocketSession session;
	@JsonIgnore
	private List<DeviceProxy> devices;
	@JsonIgnore
	private List<RemoteHost> remoteHosts;
	@JsonIgnore
	private List<DeviceProxy> finishedDevices;
	private ExecutionMode executionMode;
	@JsonIgnore
	private User user;
	@JsonIgnore
	private List<TestClass> testClasses;
	@JsonIgnore
	private KillingMatrix killingMatrix;
	private TaskState state;
	@JsonIgnore
	private int mutantSampling;
	@JsonIgnore
	private ExecutionAlgorithm executionAgorithm;
	@JsonIgnore
	private ExecutionOrder executionOrder;
	@JsonIgnore
	private String selectedMutants;
	@JsonIgnore
	private List<MutantWrapper> mutantWrappers;
	@JsonIgnore
	private String command;
	@JsonIgnore
	private String distributionAlgorithm;
	
	public TestRunnerGroup(User user, Project project, WebSocketSession session, JSONArray remoteDevices, ExecutionMode executionMode, String command) throws Exception {
		this.id = UUID.randomUUID().toString();
		this.executionAgorithm = ExecutionAlgorithm.ALL_AGAINST_ALL;
		this.user = user;
		this.project = project;
		this.session = session;
		this.devices = new Vector<>();
		this.remoteHosts = new Vector<>();
		if (remoteDevices.length()==0) 
			throw new Exception("Select at least one device");
		loadDevicesAndHosts(remoteDevices);
		
		this.command = command;
		if (command.equals("runTests")) {
			//Manager.get().reserveDevices();
			this.executionMode = executionMode;
			this.mutantSampling = 100;
			WSTestsExecution.send(session, "type", "MESSAGE", "message", "Recovering test classes");
			this.testClasses = Manager.get().getTestClassDAO().getTestClasses(project.getId());
			
			WSTestsExecution.send(session, "type", "MESSAGE", "message", "Recovering test methods");
			List<TestMethod> allTestMethods = new ArrayList<>();
			for (TestClass testClass : this.testClasses) {
				List<TestMethod> testMethods = new ArrayList<>();
				testMethods = Manager.get().getTestMethodDAO().getTestMethods(testClass.getId());
				testMethods = testMethods.
					stream().
					filter(testMethod -> testMethod.isChecked()).
					collect(Collectors.toList());
				testClass.setTestMethods(testMethods);
				allTestMethods.addAll(testMethods);
			}
			this.killingMatrix = new KillingMatrix(this.id, allTestMethods, this.devices);
			this.state = TaskState.PREPARING;
			this.mutantWrappers = new Vector<>();
			this.finishedDevices = new Vector<>();
		}
	}
	
	@Override
	public void run() {
		try {
			if (this.command.equals("runTests")) {
				if (!skipOriginal) {
					for (RemoteHost remoteHost : this.remoteHosts) 
						remoteHost.downloadProject(project, this, this.session, this.command);
				} else {
					for (RemoteHost remoteHost : this.remoteHosts) {
						remoteHost.setProject(project);
						remoteHost.setTestRunnerGroup(this);
						remoteHost.setSession(this.session);
						remoteHost.setCommand(this.command);
					}
					for (DeviceProxy device : this.devices)
						device.setReadyForRunning(true);
					JSONObject executionResult = new JSONObject().put("type", "testAPKInstalled").put("returnCode", 0);
					this.onProcessExecuted(executionResult);
				}
			} else if (this.command.equals("downloadProject")) {
				for (RemoteHost remoteHost : this.remoteHosts) 
					remoteHost.downloadProject(project, this, this.session, this.command);
			} else if (this.command.equals("compile") || this.command.equals("compileTests")) {
				for (RemoteHost remoteHost : this.remoteHosts) {
					remoteHost.setProject(project);
					remoteHost.setTestRunnerGroup(this);
					remoteHost.setSession(this.session);
					remoteHost.setCommand(this.command);
					if (this.command.equals("compile"))
						remoteHost.compile();
					else
						remoteHost.compileTests();
				}
			} else if (this.command.equals("pushAPK") || this.command.equals("pushTests")) {
				for (DeviceProxy device : this.devices) {
					device.setTestRunnerGroup(this);
					device.setProject(this.project);
					device.setSession(this.session);
					device.setCommand(this.command);
					if (this.command.equals("pushAPK"))
						device.push(false);
					else
						device.push(true);
				}
			} else if (this.command.equals("installAPK") || this.command.equals("installTests")) {
				for (DeviceProxy device : this.devices) {
					device.setTestRunnerGroup(this);
					device.setProject(this.project);
					device.setSession(this.session);
					device.setCommand(this.command);
					if (this.command.equals("installAPK"))
						device.install(false);
					else
						device.install(true);
				}
			} 
		} catch (Exception e) {
			WSTestsExecution.send(session, "type", "ERROR", "message", e.getMessage());
		}
	}

	@Override
	public void onProcessExecuted(JSONObject executionResult) {
		if (executionResult.getInt("returnCode")!=0) {
			for (DeviceProxy device : this.devices)
				device.setReadyForRunning(false);
			Exception e = new ProcessException(executionResult);
			WSTestsExecution.send(this.session, "type", "ERROR", "message", e.getMessage());
			return;
		} 
		String type = executionResult.getString("type");
		switch (type) {
		case "projectDownloaded" :
			WSTestsExecution.send(session, "type", "MESSAGE", "message", "Project downloaded at " + executionResult.optString("device"));
			break;
		case "originalAPKCompiled" :
			WSTestsExecution.send(session, "type", "MESSAGE", "message", "Project compiled at " + executionResult.optString("device"));
			break;
		case "testAPKCompiled" :
			WSTestsExecution.send(session, "type", "MESSAGE", "message", "Tests compiled at " + executionResult.optString("device"));
			break;			
		case "testAPKInstalled" :
			if (this.command.equals("runTests")) {
				String deviceName = executionResult.optString("device");
				if (deviceName.length()>0)
					WSTestsExecution.send(session, "type", "MESSAGE", "message", "Project installed at " + deviceName);
				if (allDevicesAreReady()) {
					this.state = TaskState.RUNNING;
					try {
						executeTests();
					} catch (Exception e) {
						this.state = TaskState.FINISHED;
						WSTestsExecution.send(session, "type", "ERROR", "message", e.getMessage());
					}
				}
			} else {
				WSTestsExecution.send(session, "type", "MESSAGE", "message", "Tests installed to " + executionResult.optString("device"));
			}
			break;
		case "APKPushed" :
			WSTestsExecution.send(session, "type", "MESSAGE", "message", "APK pushed to " + executionResult.optString("device"));
			break;
		case "testAPKPushed" :
			WSTestsExecution.send(session, "type", "MESSAGE", "message", "Tests pushed to " + executionResult.optString("device"));
			break;
		case "APKInstalled" :
			WSTestsExecution.send(session, "type", "MESSAGE", "message", "APK installed to " + executionResult.optString("device"));
			break;
		}
	}

	private boolean allDevicesAreReady() {
		return this.devices.stream().filter(device-> device.isReadyForRunning()).count()==this.devices.size();
	}

	private void executeTests() throws Exception {
		if (executionMode == ExecutionMode.ORIGINAL) {
			while (this.devices.size()>1) {
				this.deviceHasFinished(this.devices.get(1));
				this.devices.remove(1);
			}
			DeviceProxy device = this.devices.get(0);
			device.setRunStrategy(new AllAgainstAll(this));
			this.killingMatrix.addColumn(device, new ClassicMutantWrapper("0", 0, "ORIGINAL SYSTEM (no mutant)", device.getDescription()), true);
			JSONObject jsoKillinkMatrix = this.killingMatrix.toJSON();
			WSTestsExecution.send(session, "type", "KILLING_MATRIX", "killingMatrix", jsoKillinkMatrix.toString());
			WSTestsExecution.send(session, "type", "MESSAGE", "message", "Executing tests on original system");
			device.runNextTest();
		} else if (executionMode == ExecutionMode.CLASSIC_MUTANTS || executionMode == ExecutionMode.UNTCH_MUTANTS) {
			WSTestsExecution.send(session, "type", "MESSAGE", "message", "Preparing execution");
			List<Object[]> oMutantWrappers;
			if (executionMode == ExecutionMode.CLASSIC_MUTANTS) {
				oMutantWrappers = Manager.get().getClassicMutantDAO().loadIds(project.getId());
				for (int i=0; i<oMutantWrappers.size(); i++) {
					MutantWrapper mutantWRapper = new ClassicMutantWrapper(oMutantWrappers.get(i));
					this.mutantWrappers.add(mutantWRapper);
				}
			} else {
				oMutantWrappers = Manager.get().getUntchMutantOperatorDAO().loadIds(project.getId());
				for (int i=0; i<oMutantWrappers.size(); i++) {
					MutantWrapper mutantWRapper = new UntchMutantWrapper(oMutantWrappers.get(i));
					this.mutantWrappers.add(mutantWRapper);
				}
			}
			WSTestsExecution.send(session, "type", "MESSAGE", "message", "Filtering mutants");
			if (this.mutantWrappers.isEmpty())
				throw new Exception("No mutants selected");
			filterMutantWrappers();
			if (this.mutantWrappers.isEmpty())
				throw new Exception("No mutants selected");
			WSTestsExecution.send(session, "type", "type", "MESSAGE", "MESSAGE", "message", "Distributing mutants");
			int exceso = this.devices.size() - this.mutantWrappers.size();
			for (int i=exceso; i>0; i--)
				this.devices.remove(0);
			if (!distributionAlgorithm.equals("MUTANTS_ON_DEMAND"))
				distributeMutants();
			JSONObject jsoKillinkMatrix = this.killingMatrix.toJSON();
			WSTestsExecution.send(session, "type", "KILLING_MATRIX", "killingMatrix", jsoKillinkMatrix.toString());		
			
			WSTestsExecution.send(session, "type", "MESSAGE", "message", "Executing");
			if (this.executionMode == ExecutionMode.CLASSIC_MUTANTS) {
				for (DeviceProxy device : this.devices) {
					if (this.executionAgorithm == ExecutionAlgorithm.ALL_AGAINST_ALL)
						device.setRunStrategy(new AllAgainstAll(this));
					else 
						device.setRunStrategy(new OnlyAlive(this));
					if (!distributionAlgorithm.equals("MUTANTS_ON_DEMAND"))
						device.downloadAndInstallNextMutant();
					else
						device.askForAMutant();
				}
			} else if (this.executionMode==ExecutionMode.UNTCH_MUTANTS) {
				this.downloadSchema();
				//JSONObject jsoTaskResponse = this.project.compileAndInstallSchema(this.deviceProxies);
				//if (jsoTaskResponse.getInt("returnCode")!=0) {
				//	WSTestsExecution.send(session, "type", "ERROR", "message", jsoTaskResponse);
				//	return;
				//}
			}
		}
	}
	
	private boolean skipOriginal;

	private void distributeMutants() {
		int cont = 0;
		DeviceProxy device;
		MutantWrapper mutantWrapper;
		
		for (int i=0; i<mutantWrappers.size(); i++) {
			mutantWrapper = mutantWrappers.get(i);
			device = this.devices.get(cont++);
			cont = cont % this.devices.size();
			this.killingMatrix.addColumn(device, mutantWrapper, false);
		}
	}

	private void filterMutantWrappers() {
		if (selectedMutants!=null && selectedMutants.length()>0) {
			List<String> selectedMutants;
			if (this.selectedMutants.indexOf(',')!=-1)
				selectedMutants = Arrays.asList(this.selectedMutants.split(","));
			else 
				selectedMutants = Arrays.asList(this.selectedMutants.split("\t"));
			MutantWrapper mutantWrapper;
			for (int i = mutantWrappers.size()-1; i>=0; i--) {
				mutantWrapper = this.mutantWrappers.get(i);
				if (!selectedMutants.contains("" + mutantWrapper.getIndex()))
					this.mutantWrappers.remove(i);
			}
		} 
		
		if (selectedMutants==null || selectedMutants.isEmpty()) {
			double threshold = (double) this.mutantSampling / 100.0;
			SecureRandom dado = new SecureRandom();
			for (int i=mutantWrappers.size()-1; i>=0; i--)
				if (dado.nextDouble()>threshold)
					mutantWrappers.remove(i);
		}
	}

	@JsonIgnore
	public Project getProject() {
		return this.project;
	}

	@JsonIgnore
	public WebSocketSession getSession() {
		return session;
	}
	
	@JsonIgnore
	public KillingMatrix getKillingMatrix() {
		return killingMatrix;
	}
	
	@Override
	public String getId() {
		return id;
	}
	
	@Override
	public User getUser() {
		return this.user;
	}
	
	@Override 
	public TaskInfo getInfo() {
		return this.killingMatrix;
	}
	
	@Override 
	public TaskState getState() {
		return this.state;
	}
	
	@JsonIgnore
	public List<TestClass> getTestClasses() {
		return testClasses;
	}

	public void setMutantSampling(int mutantSampling) {
		this.mutantSampling = mutantSampling;
	}

	public void setExecutionAlgorithm(ExecutionAlgorithm executionAgorithm) {
		this.executionAgorithm = executionAgorithm;
	}
	
	public ExecutionAlgorithm getExecutionAgorithm() {
		return executionAgorithm;
	}

	public void setExecutionOrder(ExecutionOrder executionOrder) {
		this.executionOrder = executionOrder;
	}
	
	public ExecutionOrder getExecutionOrder() {
		return executionOrder;
	}

	public void setSelectedMutants(String selectedMutants) {
		this.selectedMutants = selectedMutants;		
	}

	private void loadDevicesAndHosts(JSONArray remoteDevices) {
		for (int i=0; i<remoteDevices.length(); i++) {
			JSONObject jsoDeviceProxy = remoteDevices.getJSONObject(i);
			DeviceProxy deviceProxy = Manager.get().addRemoteDevice(jsoDeviceProxy.getString("ip"), jsoDeviceProxy.getInt("port"), jsoDeviceProxy.getString("name")); 
			this.devices.add(deviceProxy);
			RemoteHost host = deviceProxy.getRemoteHost();
			boolean found = false;
			for (int j=0; j<this.remoteHosts.size(); j++) {
				RemoteHost existingHost = this.remoteHosts.get(j);
				if (existingHost.getDescription().equals(host.getDescription())) {
					found = true;
					existingHost.addDevice(deviceProxy);
					break;
				}
			}
			if (!found) {
				this.remoteHosts.add(host);
				host.addDevice(deviceProxy);
			}				
		}
	}

	public synchronized void deviceHasFinished(DeviceProxy device) {
		this.finishedDevices.add(device);
		device.setReadyForRunning(false);
		if (this.finishedDevices.size()==this.devices.size()) {
			this.state = TaskState.FINISHED;
			JSONObject summary = this.killingMatrix.getSummary();
			WSTestsExecution.send(session, "type", "SUMMARY", "summary", summary);
			WSTestsExecution.send(session, "type", "MESSAGE", "message", "Test execution finished");
			if (this.executionMode==ExecutionMode.UNTCH_MUTANTS)
				undoSchema();
		}
	}

	private void undoSchema() {
		String path = project.getProjectFolder();
		path = path + project.getProjectConfiguration().getSrcFolder();
		path = path + "main/java/mutantSchema";
		File file = new File(path);
		try {
			FileUtils.forceDelete(file);
		} catch (IOException e) {
			WSTestsExecution.send(session, "type", "ERROR", "message", "Unable to restore the original project (i.e., unable to remove the Mutant Schema)");
		}
		
		List<String> originalFileIds = Manager.get().getUntchMutantDAO().loadOriginalFileIds(this.project.getId());
		String originalFileId;
		for (int i=0; i<originalFileIds.size(); i++) {
			originalFileId = originalFileIds.get(i);
			Optional<ProjectFile> optOriginalFile = Manager.get().getProjectFileDAO().findById(originalFileId);
			if (optOriginalFile.isPresent()) {
				ProjectFile originalFile = optOriginalFile.get();
				String fileName = this.project.getProjectFolder() + originalFile.getFileName();
				try(FileOutputStream fos = new FileOutputStream(fileName)) {
					fos.write(originalFile.getContents());
				} catch (Exception e) {
					WSTestsExecution.send(session, "type", "ERROR", "message", "Error restoring the original file " + originalFile.getFileName());
				}
			}
		}	
	}

	private void downloadSchema() throws Exception {
		Optional<TemplateFile> optMutantDriver = Manager.get().getTemplateFileDAO().findById("MutantDriver.java");
		if (optMutantDriver.isPresent()) {
			TemplateFile mutantDriver = optMutantDriver.get();
			String path = project.getProjectFolder();
			path = path + project.getProjectConfiguration().getSrcFolder();
			path = path + "main/java/" + mutantDriver.getPackageName();
			File file = new File(path);
			file.mkdirs();
			path = path + mutantDriver.getFileName();
			try(FileOutputStream fos = new FileOutputStream(path)) {
				fos.write(mutantDriver.getCode().getBytes());
			}
		}
		
		List<String> mutantIds = Manager.get().getUntchMutantDAO().loadIds(this.project.getId());
		String mutantId;
		for (int i=0; i<mutantIds.size(); i++) {
			mutantId = mutantIds.get(i);
			Optional<UntchMutant> optMutant = Manager.get().getUntchMutantDAO().findById(mutantId);
			if (optMutant.isPresent()) {
				UntchMutant mutant = optMutant.get();
				mutant.replaceOriginal(this.project.getProjectFolder());
			}
		}		
	}

	@Override
	public void stop() {
		for (DeviceProxy device : this.devices) {
			device.stop();
			this.deviceHasFinished(device);
		}
	}

	@Override
	public String getDescription() {
		return "TestRunnerGroup #" + this.id;
	}

	public void setSkipOriginal(boolean skipOriginal) {
		this.skipOriginal = skipOriginal;
	}

	public synchronized MutantWrapper getNextMutantWrapper() {
		if (this.mutantWrappers.isEmpty())
			return null;
		return this.mutantWrappers.remove(0);
	}
	
	public String getDistributionAlgorithm() {
		return distributionAlgorithm;
	}
	
	public void setDistributionAlgorithm(String distributionAlgorithm) {
		this.distributionAlgorithm = distributionAlgorithm;
	}
}
