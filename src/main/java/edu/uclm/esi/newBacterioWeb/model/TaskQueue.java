package edu.uclm.esi.newBacterioWeb.model;

import java.util.List;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.context.annotation.Bean;


public class TaskQueue {
	
	private ConcurrentHashMap<String, Task> tasksById;
	private ConcurrentHashMap<String, Vector<Task>> tasksByUser;
	
	public TaskQueue() {
		this.tasksById = new ConcurrentHashMap<>();
		this.tasksByUser = new ConcurrentHashMap<>();
	}
	
	private static class TaskQueueHolder {
		static TaskQueue singleton=new TaskQueue();
	}
	
	@Bean
	public static TaskQueue get() {
		return TaskQueueHolder.singleton;
	}

	public void put(User user, Task task) {
		this.tasksById.put(task.getId(), task);
		Vector<Task> userTasks = this.tasksByUser.get(user.getUserName());
		if (userTasks==null) {
			userTasks = new Vector<>();
			this.tasksByUser.put(user.getUserName(), userTasks);
		}
		userTasks.add(task);
	}
	
	public List<Task> getTasks(User user) {
		Vector<Task> tasks = this.tasksByUser.get(user.getUserName());
		if (tasks==null)
			return new Vector<>();
		return tasks;
	}

	public Task findTask(User user, String idTask) throws Exception {
		Task task = this.tasksById.get(idTask);
		if (!task.getUser().getUserName().equals(user.getUserName()))
			throw new Exception("Task not found for user " + user.getUserName());
		return task;
	}
}
