package edu.uclm.esi.newBacterioWeb.model.killingMatrix;

import java.util.Comparator;

import edu.uclm.esi.newBacterioWeb.model.devices.DeviceProxy;

public class DeviceComparator implements Comparator<DeviceProxy> {
	@Override
	public int compare(DeviceProxy o1, DeviceProxy o2) {
		return o1.getDescription().compareTo(o2.getDescription());
	}
}
