package edu.uclm.esi.newBacterioWeb.model;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public interface IMutant {

	public String getCode();
	public ProjectFile getOriginalFile();
	
	public default String replaceOriginal(String projectPath) throws Exception {
		String originalCode;
		String fileName = projectPath + this.getOriginalFile().getFileName();
		try(FileInputStream fis = new FileInputStream(fileName)) {
			byte[] bytes = new byte[fis.available()];
			fis.read(bytes);
			originalCode = new String(bytes);
		}
		try(FileOutputStream fos = new FileOutputStream(fileName)) {
			fos.write(this.getCode().getBytes());
		}
		return originalCode;
	}

	public default void restore(String projectPath, String originalCode) throws IOException {
		String fileName = projectPath + this.getOriginalFile().getFileName();
		try(FileOutputStream fos = new FileOutputStream(fileName)) {
			fos.write(originalCode.getBytes());
		}
	}
}
