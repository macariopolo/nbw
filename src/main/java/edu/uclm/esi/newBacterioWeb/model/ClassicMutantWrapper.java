package edu.uclm.esi.newBacterioWeb.model;

import org.json.JSONObject;

public class ClassicMutantWrapper extends MutantWrapper {

	public ClassicMutantWrapper(String id, int index, String classUnderTest, String deviceName) {
		super(id, index, classUnderTest, deviceName);
	}

	public ClassicMutantWrapper(Object[] o) {
		super(o);
	}

	public ClassicMutantWrapper(JSONObject jsoMutantWrapper) {
		super(jsoMutantWrapper);
	}


}
