package edu.uclm.esi.newBacterioWeb.model.killingMatrix;

public enum KillingMatrixCellValue {
	UNVISITED, ALIVE, KILLED, NON_COMPILABLE, IGNORED;
}
