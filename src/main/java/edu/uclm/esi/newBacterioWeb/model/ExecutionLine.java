package edu.uclm.esi.newBacterioWeb.model;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

import edu.uclm.esi.newBacterioWeb.model.killingMatrix.KillingMatrixCellValue;
import edu.uclm.esi.newBacterioWeb.web.controllers.Manager;

@Entity
public class ExecutionLine {
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String executionId;
	private String projectName;
	private String algorithm;
	private String executionOrder;
	private String testCase;
	private int mutantIndex;
	@Column(columnDefinition = "LONGTEXT")
	private String classUnderTest;
	private String killed;
	private String testType;
	private String device;
	private String date;
	private String time;
	private long duration;
	@Transient
	private long startTime;
	@Transient
	private static SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy"), 
		timeFormatter = new SimpleDateFormat("hh:mm:ss a");

	public ExecutionLine() {
		Date date = new Date();
		this.startTime = date.getTime();
		this.date = dateFormatter.format(date);
		this.time = timeFormatter.format(date);
	}

	public String getExecutionId() {
		return executionId;
	}

	public void setExecutionId(String executionId) {
		this.executionId = executionId;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getAlgorithm() {
		return algorithm;
	}

	public void setAlgorithm(String algorithm) {
		this.algorithm = algorithm;
	}

	public String getExecutionOrder() {
		return executionOrder;
	}
	
	public void setExecutionOrder(ExecutionOrder executionOrder) {
		this.executionOrder = executionOrder==ExecutionOrder.SEQUENTIAL ? "Sequential" : "Best test case first";
	}

	public String getTestCase() {
		return testCase;
	}

	public void setTestCase(String testCase) {
		this.testCase = testCase;
	}

	public int getMutantIndex() {
		return mutantIndex;
	}

	public void setMutantIndex(int mutantIndex) {
		this.mutantIndex = mutantIndex;
	}

	public String getClassUnderTest() {
		return classUnderTest;
	}

	public void setClassUnderTest(String classUnderTest) {
		this.classUnderTest = classUnderTest;
	}

	public String getKilled() {
		return killed;
	}

	public void setKilled(KillingMatrixCellValue killed) {
		this.killed = killed.name();
	}

	public String getTestType() {
		return testType;
	}

	public void setTestType(String testType) {
		this.testType = testType;
	}

	public String getDevice() {
		return device;
	}

	public void setDevice(String device) {
		this.device = device;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}
	
	public long getDuration() {
		return duration;
	}
	
	public void setDuration(long duration) {
		this.duration = duration;
	}

	public void finish(KillingMatrixCellValue killed) {
		this.killed = killed.name();
		Date date = new Date();
		this.duration = date.getTime() - this.startTime;		
		Manager.get().getExecutionLineDAO().save(this);
	}	
}
