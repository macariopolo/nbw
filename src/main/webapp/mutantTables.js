var self;

function ViewModel() {
	self = this;
	self.projectName = ko.observable(sessionStorage.projectName);
	self.mutantTables = ko.observableArray([]);
	
	self.message = ko.observable("");
	
	loadMutantTables = function() {
		self.message("Recovering mutant tables");
		var data = {
			url : "loadMutantTables",
			type : "get",
			dataType : 'json',
			success : function(response) {
				self.mutantTables(response);
				self.message("Mutant tables recovered");
			},
			error : function(response) {
				self.message("");
				alert(response.message);
			}
		};
		$.ajax(data);
	}
	
	self.goBack = function() {
		window.history.back();
	}
	
	self.goToMutantTables = function() {
		window.location.href = "mutantTables.html";
	} 
	
	loadMutantTables();
}

var vm = new ViewModel();
ko.applyBindings(vm);