var self;

function ViewModel() {
	self = this;
	self.message = ko.observable("");
	self.error = ko.observable("");
	self.projectName = ko.observable(sessionStorage.projectName);
	self.emulators = ko.observableArray([]);
	self.remoteDevices = ko.observableArray([]);
	self.runningDevices = ko.observableArray([]);
	
	self.testClasses = ko.observableArray([]);
	self.tasks = ko.observableArray([]);
	
	self.classicSampling = ko.observable(10);
	self.selectedMutants = ko.observable("");
	self.executionAlgorithm = ko.observable("onlyAlive");
	self.executionOrder = ko.observable("bestTestCaseFirst");
	self.distributionAlgorithm = ko.observable("MUTANTS_ON_DEMAND");
	
	self.skipOriginal = ko.observable(false);
	
	self.selectedTestCases = ko.observableArray([]);
	self.ineffectiveTestCases = ko.observableArray([]);
	self.mutationScore = ko.observable();
	
	self.killingMatrix = ko.observable(new KillingMatrix("", [], []));
		
	var allSelected = true;
	self.revertSelection = function() {
		allSelected = !allSelected;
		for (var i=0; i<self.testClasses().length; i++) {
			self.testClasses()[i].check(allSelected);
		}
	}
	
	self.openRemoteConsoles = function() {
		window.open("remoteConsoles.html");
	}
	
	self.goBack = function() {
		window.history.back();
	}
	
	self.saveSelection = function() {
		var testClasses = [];
		var tc;
		for (var i=0; i<self.testClasses().length; i++) {
			tc = { 
				id : self.testClasses()[i].id,
				checked : self.testClasses()[i].checked(),
				testMethods : self.testClasses()[i].getMethods()
			};
			testClasses.push(tc);
		}
		var data = {
			data : JSON.stringify(testClasses),
			url : "saveTestClasses",
			type : "post",
			contentType: 'application/json',
			success : function(response) {
				self.message("Test cases saved");
			},
			error : function(response) {
				self.error(response.responseText);
			}
		};
		$.ajax(data);		
	}
	
	loadTestClasses = function() {
		var data = {
			url : "loadTestClasses",
			type : "get",
			contentType: 'application/json',
			dataType : 'json',
			success : function(response) {
				self.testClasses([]);
				for (var i=0; i<response.length; i++) {
					var ri = response[i];
					var testClass = new TestClass(ko, ri.id, ri.name, ri.testType, ri.checked, ri.testMethods);
					self.testClasses.push(testClass);
				}
			},
			error : function(response) {
				self.error(response.responseText);
			}
		};
		$.ajax(data);
	}
	
	self.reloadTasks = function() {
		var data = {
			url : "getTasks",
			type : "get",
			contentType: 'application/json',
			dataType : 'json',
			success : function(response) {
				self.tasks([]);
				for (var i=0; i<response.length; i++) {
					var task = new Task(response[i].id, response[i].state);
					self.tasks.push(task);
				}
			},
			error : function(response) {
				self.error(response.responseText);
			}
		};
		$.ajax(data);
	}
	
	self.loadMutantByIndex = function(mutantIndex) {
		sessionStorage.mutantIndex = mutantIndex;
		window.open("mutantCode.html");
	}
	
	self.ws = new WebSocket("ws://" + window.location.host + "/wsTestsExecution");
	self.runTests = function(executionType) {
		self.message("Executing test cases");
		self.error("");
		self.selectedTestCases([]);
		self.ineffectiveTestCases([]);
		self.mutationScore();	
		var remoteDevices = [];
		self.runningDevices([]);
		for (var i=0; i<self.remoteDevices().length; i++) {
			if (self.remoteDevices()[i].checked) {
				var rd = self.remoteDevices()[i];
				remoteDevices.push(rd);
				self.runningDevices.push(new RunningRemoteDevice(rd.ip, rd.port, rd.name));
			}
		}
		if (executionType=="ORIGINAL") {
			self.executionAlgorithm("allAgainstAll");
			self.executionOrder("sequential");
		}
		var msg = {
			type : "runTests",
			executionMode : executionType,
			remoteDevices : remoteDevices,
			executionAlgorithm : self.executionAlgorithm(),
			executionOrder : self.executionOrder(),
			mutantSampling : self.classicSampling(),
			selectedMutants : self.selectedMutants(),
			distributionAlgorithm : self.distributionAlgorithm(),
			skipOriginal : self.skipOriginal()
		};
		self.killingMatrix(new KillingMatrix("", [], []));
		self.ws.send(JSON.stringify(msg));
	}
	
	self.ws.onopen = function() {
		self.message("Connected to WebSocket");
	}

	function findRunningDevice(deviceName) {
		for (var i=0; i<self.runningDevices().length; i++)
			if (self.runningDevices()[i].name() == deviceName)
				return self.runningDevices()[i];
	}
	
	self.ws.onmessage = function(event) {
		var jso = JSON.parse(event.data);
		var type = jso.type;
		if (type=="ERROR") {
			self.error(jso.message);
			return;
		}
		self.error("");
		if (type=="PROGRESS") {
			var state = jso.state;
			var deviceName = jso.deviceName;
			var mutantIndex = jso.mutantIndex;
			var testMethod = jso.testMethod;
			var runningDevice;
			if (state == "DOWNLOADING") { 
				runningDevice = findRunningDevice(deviceName);
				runningDevice.setState("Downloading mutant " + mutantIndex);
			} else if (state == "READY") {
				runningDevice = findRunningDevice(deviceName);
				runningDevice.setState("ready for running, waiting for all devices");
			} else if (state == "RUNNING") {
				runningDevice = findRunningDevice(deviceName);
				runningDevice.setState("Running " + testMethod + " against " + mutantIndex);
			} else if (state == "PUSHING APK") {
				runningDevice = findRunningDevice(deviceName);
				runningDevice.setState("Pushing APK");
			} else if (state == "PUSHING TEST APK") {
				runningDevice = findRunningDevice(deviceName);
				runningDevice.setState("Pushing test APK");
			} else if (state == "INSTALLING APK") {
				runningDevice = findRunningDevice(deviceName);
				runningDevice.setState("Installing APK");
			} else if (state == "INSTALLING TEST APK") {
				runningDevice = findRunningDevice(deviceName);
				runningDevice.setState("Installing test APK");
			} else if (state == "FINISHED") {
				runningDevice = findRunningDevice(deviceName);
				runningDevice.setState("Finished");
			} else {
				runningDevice = findRunningDevice(deviceName);
				runningDevice.setState(state);
			}
		} else if (type=="KILLING_MATRIX") {
			var km = JSON.parse(jso.killingMatrix);
			self.killingMatrix().id(km.id);
			var devices = km.devices;
			for (var i=0; i<devices.length; i++) {
				var device = devices[i];
				var deviceMutants = new DeviceMutants(device.deviceName);
				self.killingMatrix().devices.push(deviceMutants);				
				var mutantIndexes = device.mutantIndexes;
				for (var j=0; j<mutantIndexes.length; j++)
					deviceMutants.mutants.push(mutantIndexes[j]);
			}
			self.killingMatrix().testMethods(km.testMethods);
		} else if (type=="MUTANT_ADDED") {
			var deviceName = jso.deviceName;
			var mutantIndex = jso.mutantIndex;
			self.killingMatrix().addMutant(deviceName, mutantIndex);			
		} else if (type=="EXECUTION_RESULT") {
			var celda = document.getElementById(jso.methodName + "_" + jso.mutantIndex);
			var state = jso.cell.state;
			var executionOrder = jso.cell.executionOrder;
			if (state == "KILLED" || state=="IGNORED") {
				var anchor = document.createElement("a");
				celda.appendChild(anchor);
				anchor.innerHTML = executionOrder;
				anchor.setAttribute("href", "javascript:viewError('" + jso.cell.fileName + "')");
				if (state == "KILLED")
					celda.setAttribute("style", "background-color:red");
				else
					celda.setAttribute("style", "background-color:yellow");
			} else if (state == "ALIVE") {
				celda.innerHTML = executionOrder;
				celda.setAttribute("style", "background-color:green");
			} 
		} else if (type=="NON_COMPILABLE") {
			var celda = document.getElementById("mutant" + jso.mutantIndex);
			celda.setAttribute("style", "background-color:red");
		} else if (type=="SUMMARY") {
			self.selectedTestCases(jso.summary.selectedTestCases);
			self.ineffectiveTestCases(jso.summary.ineffectiveTestCases);
			self.mutationScore(jso.summary.mutationScore);
		} else if (type=="MESSAGE") {
			self.message(jso.message);
		}
	}
	
	self.ws.onclose = function() {
		self.message("Test execution finished");
	}
	
	self.emulatorDescription = function(emulator) {
		return emulator.ip + ":" + emulator.port + "/" + emulator.name;
	} 
	
	loadEmulators = function() {
		var data = {
			url : "getRemoteHosts",
			type : "get",
			contentType: 'application/json',
			success : function(response) {
				self.emulators([]);
				for (var i=0; i<response.length; i++) {
					var remoteHost = new RemoteHost(response[i].ip, response[i].port);
					remoteHost.loadEmulators(self.emulators);
				}
			},
			error : function(response) {
				alert(response.responseText);
			}
		};
		$.ajax(data);		
	}
	
	loadRemoteDevices = function() {
		var data = {
			url : "getRemoteHosts",
			type : "get",
			contentType: 'application/json',
			dataType : 'json',
			success : function(response) {
				self.remoteDevices([]);
				for (var i=0; i<response.length; i++) {
					var remoteHost = new RemoteHost(response[i].ip, response[i].port);
					remoteHost.loadDevices(self.remoteDevices);
				}
			},
			error : function(response) {
				self.error(response.responseText);
			}
		};
		$.ajax(data);		
	}
	
	launchEmulators = function() {
		var selectedEmulators = [];
		for (var i=0; i<self.emulators().length; i++)
			if (self.emulators()[i].checked)
				selectedEmulators.push(self.emulators()[i]);
		var info = {
			emulators : selectedEmulators
		};
			
		var data = {
			data : JSON.stringify(selectedEmulators),
			url : "launchEmulators",
			type : "post",
			contentType: 'application/json',
			success : function(response) {
				self.message("Emulators launched");
			},
			error : function(response) {
				self.error(response.responseText);
			}
		};
		$.ajax(data);	
	}
	
	selectAllRemoteDevices = function() {
		
	}
	
	selectAllEmulators = function() {
		
	}
	
	loadEmulators();
	loadRemoteDevices();
	loadTestClasses();
}

function viewError(fileName) {
	sessionStorage.errorFileName = fileName;
	window.open("viewError.html");
}

var vm = new ViewModel();
ko.applyBindings(vm);