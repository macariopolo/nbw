var self;

function ViewModel() {
	self = this;
	
	var currentFile = sessionStorage.projectFileId;
	var projectId = sessionStorage.projectId;
	var mutantIndex = sessionStorage.mutantIndex;
	
	self.originalCode = ko.observableArray([]);
	self.mutants = ko.observableArray([]);
	self.selectedMutant = ko.observable();
	self.mutantCode = ko.observableArray([]);
	
	self.loadFile = function() {
		var data;
		if (projectId && mutantIndex) {
			data = {
				url : "loadFileFromMutantIndex?mutantIndex=" + mutantIndex + "&projectId=" + projectId,
				type : "get",
				contentType: 'application/json',
				success : function(response) {
					sessionStorage.projectFileId = response;
					currentFile = sessionStorage.projectFileId;
					sessionStorage.removeItem("mutantIndex");
					mutantIndex = undefined;
					self.loadFile();
					self.loadMutants();
				},
				error : function(response) {
					alert(response.message);
				}
			};
			$.ajax(data);
		} else {
			data = {
				url : "loadFile?id=" + currentFile,
				type : "get",
				contentType: 'application/json',
				dataType : 'json',
				success : function(response) {
					self.originalCode(response);
				},
				error : function(response) {
					alert(response.message);
				}
			};
			$.ajax(data);
		}
	}
	
	self.loadMutant = function() {
		var data = {
			url : "loadMutant?mutantId=" + self.selectedMutant(),
			type : "get",
			contentType: 'application/json',
			dataType : 'json',
			success : function(response) {
				self.mutantCode([]);
				compare(response);
			},
			error : function(response) {
				alert(response.message);
			}
		};
		$.ajax(data);
	}
	
	self.removeMutant = function() {
		var data = {
			url : "removeMutant?mutantId=" + self.selectedMutant(),
			type : "delete",
			contentType: 'application/json',
			success : function(response) {
				self.mutantCode([]);
				self.loadMutants();
			},
			error : function(response) {
				alert(response.message);
			}
		};
		$.ajax(data);
	}
	
	self.loadMutants = function() {
		var data = {
			url : "loadMutantsOf?originalFileId=" + currentFile,
			type : "get",
			contentType: 'application/json',
			dataType : 'json',
			success : function(response) {
				self.mutants(response);
				self.selectedMutant(response[0]);
				self.loadMutant();
			},
			error : function(response) {
				alert(response.message);
			}
		};
		$.ajax(data);
	}
	
	function compare(lines) {
		var min = self.originalCode().length;
		if (lines.length<min)
			min = lines.length;
		for (var i=0; i<min; i++) {
			if (self.originalCode()[i]!=lines[i])
				self.mutantCode.push("<mark style='background-color: red'>" + lines[i] + "</mark>");
			else
				self.mutantCode.push(lines[i]);
		}
	}
	
	self.loadFile();
	self.loadMutants();
}

var vm = new ViewModel();
ko.applyBindings(vm);