var self;

function ViewModel() {
	self = this;
	
	self.lines = ko.observableArray([]);
	
	var errorFileName = sessionStorage.errorFileName;
	sessionStorage.removeItem(errorFileName);

	var data = {
		url : "viewError?fileName=" + errorFileName,
		type : "get",
		contentType: 'application/json',
		success : function(response) {
			self.lines(response);
		},
		error : function(response) {
			self.lines.push("Error: " + response.responseText);
		}
	};
	$.ajax(data);	
}

var vm = new ViewModel();
ko.applyBindings(vm);