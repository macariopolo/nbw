var self;

function ViewModel() {
	self = this;
	self.userName = ko.observable(sessionStorage.userName);
	self.project = ko.observable(new Project(ko, "", "", sessionStorage.userName));
	
	self.message = ko.observable("");
		
	self.patterns = ko.observableArray([]);
	self.selectedPattern = ko.observable("");

	self.goBack = function() {
		window.history.back();
	}
		
	self.open = function(project) {
		let data = {
			url : "openProject?id=" + sessionStorage.projectId,
			type : "get",
			contentType: "application/json",
			dataType : "json",
			success : function(response) {
				self.project(new Project(ko, response.id, response.name, response.creator.userName, response.sourceFiles));
			},
			error : function(response) {
				alert(response.message);
			}
		};
		$.ajax(data);
	}	
	
	self.deleteSelected = function() {
		var data = {
			url : "deleteMutants",
			type : "post",
			data : JSON.stringify(self.project().selectedFiles()),
			contentType: 'application/json',
			dataType : 'json',
			success : function(response) {
				self.message("Mutants deleted");
			},
			error : function(response) {
				alert(response.responseText);
			}
		};
		$.ajax(data);	
	}
	
	self.goToMutantTables = function() {
		window.location.href = "mutantTables.html";
	}
	
	self.goToMutantsGeneration = function() {
		sessionStorage.selectedFiles = JSON.stringify(self.project().selectedFiles());
		window.location.href = "mutantsGeneration.html";
	}
	
	self.goToRun = function() {
		window.location.href = "run.html";
	}
	
	self.goToConfiguration = function() {
		window.location.href = "projectConfiguration.html";
	}
	
	self.open();
}

var vm = new ViewModel();
ko.applyBindings(vm);