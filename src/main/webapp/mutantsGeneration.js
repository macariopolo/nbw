var self;

//Porque hay que crear las funciones asignandolas a una var, para que los observables
function ViewModel() {
	self = this;
	self.projectName = ko.observable(sessionStorage.projectName);
	self.selectedFiles = ko.observableArray(JSON.parse(sessionStorage.selectedFiles));
	self.operatorFamilies = ko.observableArray([]);
	self.message = ko.observable("");
	self.classicMutantsMessage = ko.observable("");
	self.untchMutantsMessage = ko.observable("");
	
	self.generateClassicMutants = ko.observable(true);
	self.generateUntchMutants = ko.observable(true);
	
	
	self.loadOperators = function() {
		var data = {
			url : "loadOperators",
			type : "get",
			dataType : 'json',
			success : function(response) {
				var families = Object.keys(response);
				var operators = Object.values(response);
				self.operatorFamilies([]);
				for (var i=0; i<families.length; i++) 
					self.operatorFamilies.push(new OperatorFamily(families[i], operators[i]));
				self.message("");
			},
			error : function(response) {
				self.message("");
				alert(response.message);
			}
		};
		$.ajax(data);
	}
	
	self.getSelectedOperators = function() {
		var r = [];
		var family;
		for (var i=0; i<self.operatorFamilies().length; i++) {
			family = self.operatorFamilies()[i];
			for (var j=0; j<family.operators.length; j++)
				if (family.operators[j].checked())
					r.push(family.operators[j].name);
		}
		return r;
	}
	
	
	
	self.generateMutants = function() {
		self.message("Generating mutants");
		var ws = new WebSocket("ws://" + window.location.host + "/wsMutantsGeneration");
		ws.onopen = function() {
			var files = [];
			for (var i=0; i<self.selectedFiles().length; i++)
				files.push(self.selectedFiles()[i].id);
			var msg = {
				type : "GENERATE_MUTANTS",
				operators : self.getSelectedOperators(),
				files : files,
				generateClassicMutants : self.generateClassicMutants(),
				generateUntchMutants : self.generateUntchMutants()
			};
			ws.send(JSON.stringify(msg));
		}
		ws.onmessage = function(event) {
			var jso = JSON.parse(event.data);
			var type = jso.type;
			if (type=="CLASSIC_MUTANT") {
				self.classicMutantsMessage("Classic mutants generated: " + jso.index);
			} else if (type=="UNTCH_MUTANT") {
				self.untchMutantsMessage("Untch mutants generated: " + jso.amount);
			}
		}
		ws.onclose = function() {
			self.message("Mutant generation finished");
		}
	}
	
	self.goToProject = function() {
		window.location.href = "project.html";
	}
	
	self.goToMutantTables = function() {
		window.location.href = "mutantTables.html";
	} 
	
	self.loadOperators();
}

var vm = new ViewModel();
ko.applyBindings(vm);