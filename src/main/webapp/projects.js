var self;

function ViewModel() {
	self = this;
	self.userName = ko.observable(sessionStorage.userName);
	self.projects = ko.observableArray([]);
	self.project = ko.observable(new Project(ko, "", "", sessionStorage.userName));
	
	self.message = ko.observable("");
	
	self.loadProjects = function() {	
		var data = {
			url : "loadProjects",
			type : "get",
			contentType: 'application/json',
			dataType : 'json',
			success : function(response) {
				self.projects([]);
				var p;
				for (var i=0; i<response.length; i++) {
					p = response[i];
					var project = new Project(ko, p.id, p.name, p.creator.userName);
					self.projects.push(project);
				}
			},
			error : function(response) {
				alert(response.message);
			}
		};
		$.ajax(data);	
	}
	
	self.createProject = function() {
		var info = {
			userName : self.userName(),
			name : self.project().name
		};
			
		var data = {
			data : JSON.stringify(info),
			url : "createProject",
			type : "post",
			contentType: 'application/json',
			dataType : 'json',
			success : function(response) {
				self.loadProjects();
			},
			error : function(response) {
				alert(response.message);
			}
		};
		$.ajax(data);
	}
	
	self.open = function(project) {
		sessionStorage.projectId = project.id;
		sessionStorage.projectName = project.name;
		window.location.href = "project.html";
	}
	
	self.remove = function(project) {
		var r = confirm("Delete project?");
		if (r) {
			var data = {
				url : "deleteProject?id=" + project.id,
				type : "get",
				contentType: 'application/json',
				dataType : 'json',
				success : function(response) {
					self.loadProjects();
				},
				error : function(response) {
					alert(response.message);
				}
			};
			$.ajax(data);
		}
	}	
	
	
	self.loadProjects();
}

var vm = new ViewModel();
ko.applyBindings(vm);