var self;

function ViewModel() {
	self = this;
	
	self.message = ko.observable("");
	
	self.remoteHost = ko.observable(new RemoteHost());
	
	self.remoteHosts = ko.observableArray([]);

	self.addRemoteHost = function() {
		var data = {
			data : JSON.stringify(self.remoteHost()),
			url : "addHost",
			type : "put",
			contentType: 'application/json',
			success : function(response) {	
				self.remoteHost(new RemoteHost());
				self.remoteHosts([]);
				for (var i=0; i<response.length; i++)
					self.remoteHosts.push(new RemoteHost(response[i].ip, response[i].port, response[i].sdkPath, response[i].avdPath));
			},
			error : function(response) {
				alert(response.message);
			}
		};
		$.ajax(data);	
	}
	
	self.remove = function(rh) {
		var data = {
			data : JSON.stringify(rh),
			url : "deleteHost",
			type : "delete",
			contentType: 'application/json',
			success : function(response) {
				self.remoteHost(new RemoteHost());
				self.remoteHosts([]);
				for (var i=0; i<response.length; i++)
					self.remoteHosts.push(new RemoteHost(response[i].ip, response[i].port, response[i].sdkPath, response[i].avdPath));
			},
			error : function(response) {
				alert(response.message);
			}
		};
		$.ajax(data);			
	}
	
	self.goBack = function() {
		window.history.back();
	}
	
	var data = {
		url : "getRemoteHosts",
		type : "get",
		success : function(response) {	
			self.remoteHosts([]);
			for (var i=0; i<response.length; i++)
				self.remoteHosts.push(new RemoteHost(response[i].ip, response[i].port, response[i].sdkPath, response[i].avdPath));
		},
		error : function(response) {
			alert(response.message);
		}
	};
	$.ajax(data);	
}

var vm = new ViewModel();
ko.applyBindings(vm);