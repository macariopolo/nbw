var self;

function ViewModel() {
	self = this;
	
	var mutantIndex = sessionStorage.mutantIndex;
	//sessionStorage.removeItem("id");
	
	self.mutantCode = ko.observableArray([]);
	
	self.loadMutantByIndex = function() {	
		var data = {
			url : "loadMutantByIndex?mutantIndex=" + mutantIndex,
			type : "get",
			contentType: 'application/json',
			dataType : 'json',
			success : function(response) {
				self.mutantCode(response);
			},
			error : function(response) {
				alert(response.message);
			}
		};
		$.ajax(data);	
	}
	
	self.loadMutantByIndex();
}

var vm = new ViewModel();
ko.applyBindings(vm);