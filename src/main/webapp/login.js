var self;

function ViewModel() {
	self = this;
	self.userName = ko.observable("macario.polo");
	self.email = ko.observable("macario.polo@uclm.es");
	self.pwd = ko.observable("pepe");
	
	self.login = function() {
		var info = {
			userName : self.userName(),
			pwd : self.pwd()
		};
			
		var data = {
			data : JSON.stringify(info),
			url : "login",
			type : "post",
			contentType: 'application/json',
			dataType : 'json',
			success : function(response) {
				sessionStorage.userName = self.userName();
				window.location.href="projects.html";
			},
			error : function(response) {
				alert(response.message);
			}
		};
		$.ajax(data);		
	}
	
	self.requestToken = function() {
		alert("Token requested");
	}
	
}

var vm = new ViewModel();
ko.applyBindings(vm);