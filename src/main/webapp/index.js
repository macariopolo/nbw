function ViewModel() {
	var self = this;
	
	self.goToLogin = function() {
		window.location.href="login.html";
	}
	
	self.goToRegister = function() {
		window.location.href="register.html";
	}
	
}

var vm = new ViewModel();
ko.applyBindings(vm);