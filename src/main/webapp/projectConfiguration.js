var self;

function ViewModel() {
	self = this;
	self.projectName = ko.observable(sessionStorage.projectName);
	self.message = ko.observable("");
	self.projectConfiguration = ko.observable(new ProjectConfiguration());
	self.lastLibrary = ko.observable("");
	
	function loadProjectConfiguration() {
		var data = {
			url : "loadProjectConfiguration",
			type : "get",
			dataType : 'json',
			success : function(response) {
				self.message("Configuration loaded");
				self.projectConfiguration().gradlewCommand(response.gradlewCommand);
				self.projectConfiguration().localProperties(response.localProperties);
				self.projectConfiguration().appFolder(response.appFolder);
				self.projectConfiguration().buildGradle(response.buildGradle);
				self.projectConfiguration().srcFolder(response.srcFolder);
				self.projectConfiguration().manifestPath(response.manifestPath);
				self.projectConfiguration().applicationId(response.applicationId);
				self.projectConfiguration().testRunner(response.testRunner);
				self.projectConfiguration().buildAPKCommand(response.buildAPKCommand);
				self.projectConfiguration().installAPKCommand(response.installAPKCommand);
				self.projectConfiguration().debugAPKPathAndName(response.debugAPKPathAndName);
				self.projectConfiguration().debugPackage(response.debugPackage);
				self.projectConfiguration().buildTestAPKCommand(response.buildTestAPKCommand);
				self.projectConfiguration().installTestAPKCommand(response.installTestAPKCommand);
				self.projectConfiguration().testAPKPathAndName(response.testAPKPathAndName);
				self.projectConfiguration().testPackage(response.testPackage);
				self.projectConfiguration().unitTestCommand(response.unitTestCommand);
				self.projectConfiguration().compileSdkVersion(response.compileSdkVersion);
				self.projectConfiguration().libraries(response.libraries);
			},
			error : function(response) {
				alert(response.message);
			}
		};
		$.ajax(data);	
	}

	self.save = function() {
		var info = {
			configuration : ko.toJSON(self.projectConfiguration)
		};
		var data = {
			data :  ko.toJSON(self.projectConfiguration),
			url : "saveConfiguration",
			type : "post",
			contentType: 'application/json',
			dataType : 'json',
			success : function(response) {
				self.message("Configuration saved");
			},
			error : function(response) {
				alert(response.message);
			}
		};
		$.ajax(data);		
	}
	
	self.analyzeSourceFiles = function() {
		var ws = new WebSocket("ws://" + window.location.host + "/wsServerUpload");
		ws.onopen = function() {
			var msg = {
				type : "ANALYZE_SOURCE_FILES",
				projectId : sessionStorage.projectId
			};
			ws.send(JSON.stringify(msg));
		}
		ws.onmessage = function(event) {
			var jso = JSON.parse(event.data);
			var type = jso.type;
			if (type=="MESSAGE") {
				self.message(jso.message);
			} else if (type=="ERROR") {
				self.error(jso.message);
				this.close();
			} else if (type=="FILES_ANALYZED") {
				self.message("Source files analyzed");
				this.close();
			}
		}
	}
	
	self.guessGradleW = function() {
		var data = {
			url : "guessGradleW",
			type : "get",
			success : function(response) {
				self.projectConfiguration().gradlewCommand(response);
			},
			error : function(response) {
				alert(response.message);
			}
		};
		$.ajax(data);	
	}
	
	self.guessLocalProperties = function() {
		var data = {
			url : "guessLocalProperties",
			type : "get",
			success : function(response) {
				self.projectConfiguration().localProperties(response);
			},
			error : function(response) {
				alert(response.message);
			}
		};
		$.ajax(data);	
	}
	
	self.guessAppFolder = function() {
		var data = {
			url : "guessAppFolder",
			type : "get",
			success : function(response) {
				self.projectConfiguration().appFolder(response);
			},
			error : function(response) {
				alert(response.message);
			}
		};
		$.ajax(data);	
	}
	
	self.guessBuildGradle = function() {
		var data = {
			url : "guessBuildGradle",
			type : "get",
			success : function(response) {
				self.projectConfiguration().buildGradle(response);
			},
			error : function(response) {
				alert(response.message);
			}
		};
		$.ajax(data);	
	}
	
	self.guessSrcFolder = function() {
		var data = {
			url : "guessSrcFolder",
			type : "get",
			success : function(response) {
				self.projectConfiguration().srcFolder(response);
			},
			error : function(response) {
				alert(response.message);
			}
		};
		$.ajax(data);	
	}
	
	self.guessManifest = function() {
		var data = {
			url : "guessManifest",
			type : "get",
			success : function(response) {
				self.projectConfiguration().manifestPath(response);
			},
			error : function(response) {
				alert(response.message);
			}
		};
		$.ajax(data);	
	}
	
	self.guessApplicationId = function() {
		var data = {
			url : "guessApplicationId",
			type : "get",
			success : function(response) {
				self.projectConfiguration().applicationId(response);
			},
			error : function(response) {
				alert(response.message);
			}
		};
		$.ajax(data);	
	}
	
	self.guessTestRunner = function() {
		var data = {
			url : "guessTestRunner",
			type : "get",
			success : function(response) {
				self.projectConfiguration().testRunner(response);
			},
			error : function(response) {
				alert(response.message);
			}
		};
		$.ajax(data);	
	}
	
	self.guessDebugPackage = function() {
		var data = {
			url : "guessDebugPackage",
			type : "get",
			success : function(response) {
				self.projectConfiguration().debugPackage(response);
			},
			error : function(response) {
				alert(response.message);
			}
		};
		$.ajax(data);	
	}
	
	self.guessInstallAPKCommand = function() {
		var data = {
			url : "guessInstallAPKCommand",
			type : "get",
			success : function(response) {
				self.projectConfiguration().installAPKCommand(response);
			},
			error : function(response) {
				alert(response.message);
			}
		};
		$.ajax(data);	
	}
	
	self.guessDebugAPKPathAndName = function() {
		var data = {
			url : "guessDebugAPKPathAndName",
			type : "get",
			success : function(response) {
				self.projectConfiguration().debugAPKPathAndName(response);
			},
			error : function(response) {
				alert(response.message);
			}
		};
		$.ajax(data);	
	}
	
	self.guessTestPackage = function() {
		var data = {
			url : "guessTestPackage",
			type : "get",
			success : function(response) {
				self.projectConfiguration().testPackage(response);
			},
			error : function(response) {
				alert(response.message);
			}
		};
		$.ajax(data);	
	}
	
	self.guessInstallTestAPKCommand = function() {
		var data = {
			url : "guessInstallTestAPKCommand",
			type : "get",
			success : function(response) {
				self.projectConfiguration().installTestAPKCommand(response);
			},
			error : function(response) {
				alert(response.message);
			}
		};
		$.ajax(data);	
	}
	
	self.guessTestAPKPathAndName = function() {
		var data = {
			url : "guessTestAPKPathAndName",
			type : "get",
			success : function(response) {
				self.projectConfiguration().testAPKPathAndName(response);
			},
			error : function(response) {
				alert(response.message);
			}
		};
		$.ajax(data);	
	}
	
	self.guessCompileSDKVersion = function() {
		var data = {
			url : "guessCompileSDKVersion",
			type : "get",
			success : function(response) {
				self.projectConfiguration().compileSdkVersion(response);
			},
			error : function(response) {
				alert(response.message);
			}
		};
		$.ajax(data);	
	}

	self.goBack = function() {
		window.history.back();
	}

	
	self.addLibrary = function() {
		for (var i=0; i<self.projectConfiguration().libraries().length; i++) {
			if (self.projectConfiguration().libraries()[i]==self.lastLibrary())
				return;
		}
		self.projectConfiguration().libraries.push(self.lastLibrary());
	}
	
	loadProjectConfiguration();
}

var vm = new ViewModel();
ko.applyBindings(vm);