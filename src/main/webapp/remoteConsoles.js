var self;

function ViewModel() {
	self = this;
	self.message = ko.observable("");
	self.messages = ko.observableArray([]);
	self.error = ko.observable("");
	self.projectName = ko.observable(sessionStorage.projectName);
	self.remoteDevices = ko.observableArray([]);
	self.runningDevices = ko.observableArray([]);
		
	self.close = function() {
		window.close();
	}
	
	self.ws = new WebSocket("ws://" + window.location.host + "/wsTestsExecution");
	self.doCommand = function(command) {
		self.message("Sendind command " + command);
		self.error("");
		var remoteDevices = [];
		self.runningDevices([]);
		for (var i=0; i<self.remoteDevices().length; i++) {
			if (self.remoteDevices()[i].checked) {
				var rd = self.remoteDevices()[i];
				remoteDevices.push(rd);
				self.runningDevices.push(new RunningRemoteDevice(rd.ip, rd.port, rd.name));
			}
		}
		var msg = {
			type : command,
			remoteDevices : remoteDevices
		};
		self.ws.send(JSON.stringify(msg));
	}
	
	self.ws.onopen = function() {
		self.message("Connected to WebSocket");
	}

	function findRunningDevice(deviceName) {
		for (var i=0; i<self.runningDevices().length; i++)
			if (self.runningDevices()[i].name() == deviceName)
				return self.runningDevices()[i];
	}
	
	self.ws.onmessage = function(event) {
		var jso = JSON.parse(event.data);
		var type = jso.type;
		if (type=="ERROR") {
			self.error(jso.message);
			self.messages.push(jso.message);
			return;
		}
		self.error("");
		if (type=="MESSAGE") {
			self.message(jso.message);
			self.messages.push(jso.message);
		}
	}
	
	self.ws.onclose = function() {
		self.message("Command executed");
	}
	
	loadRemoteDevices = function() {
		var data = {
			url : "getRemoteHosts",
			type : "get",
			contentType: 'application/json',
			dataType : 'json',
			success : function(response) {
				self.remoteDevices([]);
				for (var i=0; i<response.length; i++) {
					var remoteHost = new RemoteHost(response[i].ip, response[i].port);
					remoteHost.loadDevices(self.remoteDevices);
				}
			},
			error : function(response) {
				self.error(response.responseText);
			}
		};
		$.ajax(data);		
	}
	
	self.downloadProject = function() {
		self.doCommand("downloadProject");
	}
	
	self.compile = function() {
		self.doCommand("compile");
	}
	
	self.compileTests = function() {
		self.doCommand("compileTests");
	}
	
	self.pushAPK = function() {
		self.doCommand("pushAPK");
	}
	
	self.pushTests = function() {
		self.doCommand("pushTests");
	}
	
	self.installAPK = function() {
		self.doCommand("installAPK");
	}
	
	self.installTests = function() {
		self.doCommand("installTests");
	}
	
	self.clearConsoles = function() {
		self.messages([]);
	}
	
	loadRemoteDevices();
}

var vm = new ViewModel();
ko.applyBindings(vm);