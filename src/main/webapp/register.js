var self;

function ViewModel() {
	self = this;
	self.userName = ko.observable("macario.polo");
	self.email = ko.observable("macario.polo@uclm.es");
	self.pwd1 = ko.observable("pepe");
	self.pwd2 = ko.observable("pepe");
	
	self.register = function() {
		var info = {
			userName : self.userName(),
			email : self.email(),
			pwd1 : self.pwd1(),
			pwd2 : self.pwd2()
		};
		var data = {
			data : JSON.stringify(info),
			url : "register",
			type : "post",
			contentType: 'application/json',
			success : function() {
				window.location.href="login.html";
			},
			error : function(response) {
				alert(response.responseText);
			}
		};
		$.ajax(data);		
	}
	
}

var vm = new ViewModel();
ko.applyBindings(vm);