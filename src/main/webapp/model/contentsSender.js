class ContentsSender {
	constructor(event) {
		this.event = event;
		this.counter = 0;
	}
	
	sendFiles() {
		var contentsSender = this;
		var ws = new WebSocket("ws://" + window.location.host + "/wsServerUpload");
		ws.onopen = function() {
			contentsSender.sendFile(ws);
		}
		ws.onmessage = function(event) {
			var jso = JSON.parse(event.data);
			var type = jso.type;
			if (type=="GIVE_ME_OTHER") {
				self.message(jso.fileName);
				if (jso.projectId)
					sessionStorage.projectId = jso.projectId;
				contentsSender.counter++;
				if (contentsSender.counter<contentsSender.event.target.files.length)
					contentsSender.sendFile(ws);
				else {
					this.close();
					contentsSender.createConfiguration();
				}	
			} else if (type=="ERROR") {
				self.error(jso.message);
				this.close();
			}
		}
	}
	
	createConfiguration() {
		var ws = new WebSocket("ws://" + window.location.host + "/wsServerUpload");
		ws.onopen = function() {
			var msg = {
				type : "CREATE_CONFIGURATION",
				projectId : sessionStorage.projectId
			};
			ws.send(JSON.stringify(msg));
		}
		ws.onmessage = function(event) {
			var jso = JSON.parse(event.data);
			var type = jso.type;
			if (type=="MESSAGE") {
				self.message(jso.message);
			} else if (type=="ALL_SAVED") {
				self.message("Zip file saved");
				this.close();
				self.loadProjects();
			} 			
		}
	}
	
	sendFile(ws) {
		var file = this.event.target.files[this.counter];
		if (file.size>0 && file.size<20000000) {
			var reader = new FileReader();
			reader.onload = function(e) {
				var blob = new Blob([file.webkitRelativePath, '#@00@#', reader.result]);
				ws.send(blob);
			}
			reader.readAsArrayBuffer(file);
		} else {
			var msg = {
				type : "GIVE_ME_OTHER",
				fileName : file.webkitRelativePath
			};
			ws.send(JSON.stringify(msg));
		}
	}
}