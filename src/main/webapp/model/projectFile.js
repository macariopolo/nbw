class ProjectFile {
	constructor(ko, file) {
		this.checked = ko.observable(false);
		this.id = file.id;
		this.fileName = file.fileName;
		this.numberOfMutants = ko.observable(0);
		this.loadNumberOfMutants();
	}
	
	loadNumberOfMutants() {
		let self = this;
		let data = {
			url : "getNumberOfMutants?fileId=" + this.id,
			type : "get",
			success : function(response) {
				self.numberOfMutants(response);
			}
		};
		$.ajax(data);
	}
	
	viewCode() {
		sessionStorage.projectFileId = this.id;
		window.open("code.html");
	}
}