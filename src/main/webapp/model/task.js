class Task {
	constructor(id, state) {
		this.id = id;
		this.state = state;
	}

	viewTask() {
		var data = {
			url : "viewTask?id=" + this.id,
			type : "get",
			contentType: 'application/json',
			dataType : 'json',
			success : function(response) {
				console.log(response);
			},
			error : function(response) {
				alert(response.responseText);
			}
		};
		$.ajax(data);		
	}
	
	stopTask() {
		var data = {
			url : "stopTask?id=" + this.id,
			type : "get",
			contentType: 'application/json',
			dataType : 'json',
			success : function(response) {
				console.log(response);
			},
			error : function(response) {
				alert(response.responseText);
			}
		};
		$.ajax(data);			
	}
}