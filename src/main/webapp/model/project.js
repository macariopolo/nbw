class Project {
	constructor(ko, id, name, creator, projectFiles) {
		this.id = id;
		this.name = name;
		this.creator = creator;
		this.projectFiles = [];
		if (projectFiles) { 
			let set = new Set();
			let key;
			let pf;
			for (var i=0; i<projectFiles.length; i++) {
				pf = new ProjectFile(ko, projectFiles[i]);
				this.projectFiles.push(pf);
				key = projectFiles[i].fileName;
				key = key.substring(0, key.lastIndexOf("/"));
				set.add(key);
			}
			for (let item of set)
				self.patterns.push(item);
			self.patterns.sort();
		}
	}
	
	selectedFiles() {
		var r = [];
		for (var i=0; i<this.projectFiles.length; i++)
			if (this.projectFiles[i].checked())
				r.push(this.projectFiles[i]);
		return r;
		
	}
	
	selectAll() {
		var check = this.projectFiles[0].checked();
		for (var i=0; i<this.projectFiles.length; i++) {
			this.projectFiles[i].checked(!check);
		}
	}
	
	selectByFilter() {
		var pf;
		for (var i=0; i<this.projectFiles.length; i++) {
			pf = this.projectFiles[i];
			if (pf.fileName.startsWith(self.selectedPattern()))
				pf.checked(!pf.checked());
		}
	}
	
	addContents(project, event) {
		var contentsSender = new ContentsSender(event);
		contentsSender.sendFiles();
	}
	
	uploadZip() {
		var yo = this;
		self.message("Uploading zip. Please, wait");
		$.ajax({
			url: 'uploadZip',
			type: 'POST',
			data: new FormData($('form')[0]),
			cache: false,
			contentType: false,
			processData: false,
			success : function(response) {
				yo.saveFiles(response);
			},
			error : function() {
				alert("Error uploading the zip file");
			},
			xhr: function () {
				var myXhr = $.ajaxSettings.xhr();
				if (myXhr.upload) {
					myXhr.upload.addEventListener('#progress', function (e) {
						if (e.lengthComputable) {
							$('#progress').attr({
								value: e.loaded,
								max: e.total,
							});
						}
					}, false);
				}
				return myXhr;
			}				
		});
	}
	
	saveFiles(fileName) {
		var ws = new WebSocket("ws://" + window.location.host + "/wsZipUploader");
		ws.onopen = function() {
			var msg = {
				type : "SAVE_UNZIPPED_FILES",
				fileName : fileName
			};
			ws.send(JSON.stringify(msg));
		}
		ws.onmessage = function(event) {
			var jso = JSON.parse(event.data);
			var type = jso.type;
			if (type=="MESSAGE") {
				self.message(jso.message);
			} else if (type=="FILE_NAME") {
				self.message(jso.fileName);
			} else if (type=="ALL_SAVED") {
				self.message("Zip file saved");
				this.close();
				self.loadProjects();
			} else if (type=="ERROR") {
				self.error(jso.message);
				this.close();
			}
		}
	}
	
	importMutants(project, event) {
		var importer = new Importer(event);
		importer.importMutants();
	}
	
	importFromMDroid(project, event) {
		var importer = new MDroidImporter(event);
		importer.importMutants();
	}
}
