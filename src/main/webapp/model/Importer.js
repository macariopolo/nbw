class Importer {
	constructor(event) {
		this.event = event;
		this.counter = 0;
	}
	
	importMutants() {
		var importer = this;
		var ws = new WebSocket("ws://" + window.location.host + "/wsMutantsImporter");
		ws.onopen = function() {
			importer.importMutant(ws);
		}
		ws.onmessage = function(event) {
			var jso = JSON.parse(event.data);
			var type = jso.type;
			if (type=="GIVE_ME_OTHER") {
				self.message(jso.fileName);
				importer.counter++;
				if (importer.counter<importer.event.target.files.length)
					importer.importMutant(ws);
				else {
					this.close();
					self.message("Mutants imported");
				}	
			}
		}
	}
	
	importMutant(ws) {
		var file = this.event.target.files[this.counter];
		var reader = new FileReader();
		reader.onload = function(e) {
			var blob = new Blob([reader.result]);
			ws.send(blob);
		}
		reader.readAsArrayBuffer(file);
	}
}