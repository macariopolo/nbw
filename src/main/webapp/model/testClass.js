class TestClass {
	constructor(ko, id, name, runner, checked, testMethods) {
		this.id = id;
		this.name = name;
		this.runner = runner;
		this.checked = ko.observable(checked);
		this.testMethods = [];
		for (var i=0; i<testMethods.length; i++) {
			var tm = testMethods[i];
			var testMethod = new TestMethod(ko, tm.id, tm.name, tm.checked);
			this.testMethods.push(testMethod);
		}
	}
	
	check(checked) {
		this.checked(checked);
		for (var i=0; i<this.testMethods.length; i++) {
			this.testMethods[i].check(this.checked());
		}
	}
	
	selectTestClass() {
		for (var i=0; i<this.testMethods.length; i++) {
			this.testMethods[i].check(this.checked());
		}
	}
	
	getMethods() {
		var r = [];
		var tm;
		for (var i=0; i<this.testMethods.length; i++) {
			tm = this.testMethods[i];
			r.push({id : tm.id, name : tm.name, checked : tm.checked()});
		}
		return r;
	}
}

class TestMethod {
	constructor(ko, id, name, checked) {
		this.id = id;
		this.name = name;
		this.checked = ko.observable(checked);
	}
	
	check(checked) {
		this.checked(checked);
	}
}