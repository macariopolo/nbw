class RemoteDevice {
	constructor(ip, port, name, busy) {
		this.ip = ip;
		this.port = port;
		this.name = name;
		this.checked = false;
		this.busy = busy;
	}	
	
	description() {
		return this.ip + ":" + this.port + "/" + this.name; 
	} 
}
