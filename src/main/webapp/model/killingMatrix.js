class KillingMatrix {
	constructor(id, devices, testMethods) {
		this.id = ko.observable(id);
		this.devices = ko.observableArray([]);
		this.testMethods = ko.observableArray(testMethods);
	}
	
	addMutant(deviceName, mutantIndex) {
		for (var i=0; i<this.devices().length; i++) {
			if (this.devices()[i].deviceName == deviceName) {
				var device = this.devices()[i];
				device.mutants.push(mutantIndex);
				break;
			}
		}
	}
}

class DeviceMutants {
	constructor(deviceName) {
		this.deviceName = deviceName;
		this.mutants = ko.observableArray([]);
	}
	
	description() {
		return this.deviceName;
	}  

	viewMutant(mutantIndex) {
		sessionStorage.mutantIndex = mutantIndex;
		window.open("code.html");
	}
	
	killDevice() {
		var data = {
			url : "killDevice",
			type : "post",
			data : JSON.stringify(this.deviceName),
			contentType: 'application/json',
			dataType : 'json',
			success : function(response) {
				console.log(response);
			},
			error : function(response) {
				alert(response.responseText);
			}
		};
		$.ajax(data);
	}
}