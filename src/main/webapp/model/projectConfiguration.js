class ProjectConfiguration {
	constructor() {
		this.gradlewCommand = ko.observable("");
		this.localProperties = ko.observable("");
		this.appFolder = ko.observable("");
		this.buildGradle = ko.observable("");
		this.srcFolder = ko.observable("");
		this.manifestPath = ko.observable("");
		this.applicationId = ko.observable("");
		this.testRunner = ko.observable("");
		
		this.buildAPKCommand = ko.observable("./gradlew assembleDebug");
		this.installAPKCommand = ko.observable("platform-tools/adb -s #DEVICE# shell pm install -t -r \"#PACKAGE_TO_INSTALL#\"");
		this.debugAPKPathAndName = ko.observable("");
		this.debugPackage = ko.observable("...");
		
		this.buildTestAPKCommand = ko.observable("./gradlew assembleDebugAndroidTest");
		this.installTestAPKCommand = ko.observable("platform-tools/adb -s #DEVICE# shell pm install -t -r \"#PACKAGE_TO_INSTALL#\"");
		this.testAPKPathAndName = ko.observable("");
		this.testPackage = ko.observable("...");
				
		this.unitTestCommand = ko.observable("testDebugUnitTest");
		
		this.compileSdkVersion = ko.observable("28");

		this.libraries = ko.observableArray([]);
	}
	
	removeLibrary(library) {
		for (var i=0; i<this.libraries().length; i++) {
			if (this.libraries()[i]==library) {
				this.libraries.splice(i, 1);
				break;
			}
		}
	}
}
