class RemoteHost {
	constructor(ip, port, sdkPath, avdPath) {
		this.ip = ip;
		this.port = 8710;
		this.sdkPath = sdkPath;
		this.avdPath = avdPath;
		this.emulators = ko.observableArray([]);
		this.devices = ko.observableArray([]);
	}
	
	loadEmulators(allEmulators) {
		var self = this;
		var url = "ws://" + this.ip + ":" + this.port + "/wsEmulators";
		var ws = new WebSocket(url);
		
		ws.onopen = function(event) {
			var msg = {
				type : "LOAD_EMULATORS"
			};
			ws.send(JSON.stringify(msg));
		}
		
		ws.onmessage = function(event) {
			var emulators = JSON.parse(event.data);
			for (var i=0; i<emulators.length; i++) {
				var emulator = new Emulator(self.ip, self.port, emulators[i]);
				emulator.checked = true;
				allEmulators.push(emulator);
				self.emulators.push(emulator);
			}
		}
		
		ws.onerror = function(event) {
			console.log("Error con " + this.ip);
		}
	}
	
	loadDevices(allRemoteDevices) {
		var self = this;
		var url = "ws://" + this.ip + ":" + this.port + "/wsDevices";
		var ws = new WebSocket(url);
		
		ws.onopen = function(event) {
			var msg = {
				type : "LOAD_DEVICES"
			};
			ws.send(JSON.stringify(msg));
		}
		
		ws.onmessage = function(event) {
			var devices = JSON.parse(event.data);
			for (var i=0; i<devices.length; i++) {
				var device = new RemoteDevice(self.ip, self.port, devices[i], false);
				device.checked = true;
				allRemoteDevices.push(device);
				self.devices.push(device);
			}
		}
		
		ws.onerror = function(event) {
			console.log("Error con " + this.ip);
		}
	}
}
