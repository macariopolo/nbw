class MDroidImporter {
	constructor(event) {
		this.event = event;	
		this.files = event.target.files;
		this.numberOfFiles = event.target.files.length;
		this.counter = 0;
	}
	
	importMutants() {
		var importer = this;
		var logFile = null;
		for (var i=0; i<this.numberOfFiles; i++) {
			var file = this.files[i];
			if (file.name.endsWith("log")) {
				if (logFile==null)
					logFile = file;
				else if (file.name.length<logFile.name.length)
					logFile=file;
			}
		}
		var reader = new FileReader();
		reader.onload = function(e) {
			var msg = {
				type : "LOG",
				logFileName : logFile.name,
				webkitRelativePath : logFile.webkitRelativePath,
				logFile : reader.result
			};
			importer.sendLog(msg);
		}
		reader.readAsText(logFile);
	}
	
	sendLog(msg) {
		var importer = this;
		var ws = new WebSocket("ws://" + window.location.host + "/wsMDroidImporter?projectId=" + sessionStorage.projectId);
		ws.onopen = function() {
			ws.send(JSON.stringify(msg));
		}
		ws.onmessage = function(event) {
			var jso = JSON.parse(event.data);
			var type = jso.type;
			if (type=="GIVE_ME") {
				var folder = jso.folder;
				var fileName = jso.fileName;
				var operatorName =jso.operatorName;
				var posSrc = fileName.indexOf("src/");
				fileName = fileName.substring(posSrc+4);
				importer.sendFile(ws, folder, fileName, operatorName);
			} else if (type=="FINISHED")
				self.message("Mutants imported");
		}
	}
	
	sendFile(ws, folder, fileName, operatorName) {
		self.message("Sending " + fileName);
		var importer = this;
		for (var i=0; i<this.files.length; i++) {
			if (this.files[i].webkitRelativePath == (folder + "/" + fileName)) {
				var msg = {
					type : "JAVA",
					fileName : fileName,
					operatorName : operatorName.substring(0, operatorName.indexOf(" "))
				};
				var reader = new FileReader();
				reader.onload = function(e) {
					msg.contents = reader.result;
					ws.send(JSON.stringify(msg));
				}
				reader.readAsText(this.files[i]);
				break;
			}
		}
	}
}