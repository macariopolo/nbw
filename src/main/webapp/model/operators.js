class OperatorFamily {
	constructor(name, operators) {
		this.name = name;
		this.operators = [];
		for (var i=0; i<operators.length; i++) {
			this.operators.push(new Operator(operators[i].name));
		}
	}
	
	select() {
		var check = this.operators[0].checked();
		for (var i=0; i<this.operators.length; i++) {
			this.operators[i].checked(!check);
		}
	}  
}

class Operator {
	constructor(name) {
		this.name = name;
		this.checked = ko.observable(false);
	}
}