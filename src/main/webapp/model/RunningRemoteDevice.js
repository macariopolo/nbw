class RunningRemoteDevice {
	constructor(ip, port, name) {
		this.description = ip + ":" + port + "/" + name;
		this.state = ko.observable("");
	}
	
	setState(state) {
		this.state(state);
	} 
	
	name() {
		return this.description;
	}
}
